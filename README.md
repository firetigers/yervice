# OnDemand Services Online Flatform

Codename: ODS  
Name: Yervice  
Domain: www.yervice.com  

### How To Run

```sh
// Install deps  
npm install  

// Run everything. Watching `server`, `style`  
npm run dev  

// Run server only  
npm run server  

// Run as production mode  
npm start  
```

### Environment Variables

```
NODE_ENV = production | development (default)  
APP_ROOT_EMAIL = <required>  
APP_CODE_INSIDE = <required>  
APP_LOG_LEVEL = all (default) < trace < debug < info < warn < error < fatal < mark < off 
APP_IP = 127.0.0.1  
APP_PORT = 3000  
APP_SSL_CERT = ../certificates/local/server.crt  
APP_SSL_KEY = ../certificates/local/server.crt  
APP_EX_TRUST_PROXY = true | false (default)
APP_DB_NAME = ods  
APP_DB_USER = <empty>  
APP_DB_PASS = <empty>  
APP_DB_HOST = localhost  
APP_DB_PORT = 27017  
APP_DB_RELPSET = <empty>  
APP_SESSION_SALT = <required> (32 chars)  
APP_JWT_SECRET_ACCESS = <required> (32 chars)  
APP_JWT_SECRET_REFRESH = <required> (32 chars)  
APP_JWT_SECRET_INSIDER = <required> (32 chars)  

```