const person = {
  isHuman: false,
  printIntroduction: function () {
    console.log(`My name is ${this.namex}. Am I human? ${this.isHuman}`);
  }
};

const me = Object.create(undefined);

me.name = "Matthew"; // "name" is a property set on "me", but not on "person"
me.isHuman = true; // inherited properties can be overwritten

me.printIntroduction();

console.log(me.__proto__)