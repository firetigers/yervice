'use strict'

const Promise = require('bluebird')

let prom = Promise.resolve().return(123)

prom.then((data) => {
  console.log(data)
})