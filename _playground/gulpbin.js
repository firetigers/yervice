const cp_exec = require('child_process').exec

function exec(cmd, cb) {
  let cp = cp_exec(cmd, cb)
  let dataCb = (data) => console.log(data.trim())
  cp.stdout.on('data', dataCb)
  cp.stderr.on('data', dataCb)
}


exec('echo hello $PATH')