'use strict'

const Benchmark = require('benchmark')

module.exports = (
  tests,
  opt = {
    async: false
  }) => {
  let suite = new Benchmark.Suite
  for (const test in tests) {
    if (tests.hasOwnProperty(test)) {
      suite = suite.add(test, tests[test])
    }
  }
  suite
    .on('cycle', function (event) {
      console.log(String(event.target))
    })
    .on('complete', function () {
      console.log('Fastest is ' + this.filter('fastest').map('name'))
    })
    .run(opt)
}