const Promise = require('bluebird')

async function test() {
  console.log('A')
  let a = Promise.delay(3000).then(() => {
    throw new Error('x-error')
  })
  try {
    await a
    console.log('B')
    let b = await Promise.delay(3000).return(a + 'Y')
    console.log('C')
    console.log('=', b + 'Z')
  } catch (err) {
    console.log(err.message)
  }
}

test()

