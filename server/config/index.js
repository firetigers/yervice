'use strict'

/**
 * Configuration File
 * 
 * WARNING: `process.env` must only be used inside this file
 */

const path = require('path')
const fs = require('fs')

let config = {}

// Status flags
config.production = process.env.NODE_ENV === 'production'
config.prod = config.production
config.development = !config.production
config.dev = config.development
config.demo = Boolean(process.env.APP_DEMO)

// Enable/disable access control
// Must be true on production
config.accessControl = config.prod
// Root email
config.rootEmail = process.env.APP_ROOT_EMAIL
// Code for inside login
config.codeInside = process.env.APP_CODE_INSIDE

// Set log level
config.logLevel = process.env.APP_LOG_LEVEL || 'all'

// Supported browser verions
config.browsers = JSON.stringify({
  chrome: 40, // 40,
  firefox: 46, // 36,
  opera: 37, // 27,
  safari: 6, // 6,
  edge: 14 // 14,
})

/**
 * PATHS
 */
config.paths = {}
// Project root directory
config.paths.root = path.resolve(__dirname, '../..')
// Server
config.paths.server = config.paths.root + '/server'
// Server
config.paths.routes = config.paths.server + '/routes'
// Website
config.paths.www = config.paths.root + '/www'

/**
 * SERVER
 */
config.server = {}
// Host name / IP address.
config.server.host = process.env.APP_IP || '127.0.0.1'
// Host port to use.
config.server.port = process.env.APP_PORT ? parseInt(process.env.APP_PORT, 10) : (parseInt(process.env.PORT, 10) || 3000)
// Host name for https.
config.server.httpsHost = config.server.host
// Host port for https.
config.server.httpsPort = config.server.port == 80 ? 443 : config.server.port + 1
// Options object to https server.
config.server.httpsOptions = {
  //   cert: fs.readFileSync(
  //     path.resolve(config.paths.root, process.env.APP_SSL_CERT || '../certificates/local/server.crt')
  //   ),
  //   key: fs.readFileSync(
  //     path.resolve(config.paths.root, process.env.APP_SSL_KEY || '../certificates/local/server.key')
  //   )
}
// Express settings
config.server.express = {
  'view engine': 'ejs',
  'trust proxy': Boolean(process.env.APP_EX_TRUST_PROXY)
}

/**
 * DATABASE (MongoDB)
 */
config.database = {}
// Database name.
config.database.name = process.env.APP_DB_NAME || 'ods'
// Database user.
config.database.user = process.env.APP_DB_USER || ''
// Databse password.
config.database.password = process.env.APP_DB_PASS || ''
// Server hostname. For docker, this is the database container name.
config.database.host = process.env.APP_DB_HOST || 'localhost'
// Server port.
config.database.port = process.env.APP_DB_PORT || 27017
// Comma separated `<host>:<ip>` of replica set.
// This will override above `host:port` settings.
config.database.replicaSet = process.env.APP_DB_RELPSET ? _.split(process.env.APP_DB_RELPSET, ',') : []
// Salt for database string encryption
config.database.saltrounds = 10
// Options
config.database.options = {
  useNewUrlParser: true,
  reconnectTries: Number.MAX_VALUE, // 20 * 60, // 1 Hour
  reconnectInterval: 3000 // Interval * Tries
}

/**
 * SESSION
 */
config.session = {}
// Set session id.
config.session.id = 'ODSSID'
// String salt for session security.
config.session.salt = process.env.APP_SESSION_SALT || 'yidROTh7po6otru39yUbr56R1cutec9x'
// Session expiration in hours. 14 Days.
config.session.expiration = 24 * 14
// Use for express session.
config.session.cookieSecure = true

/**
 * JWT (JSON Web Token)
 */
config.jwt = {}
// Secret for access token
config.jwt.secretAccess = process.env.APP_JWT_SECRET_ACCESS || '7u2IWrAvOyAnOSpudE2u8isP8sTLKama'
// Secret for refresh token
config.jwt.secretRefresh = process.env.APP_JWT_SECRET_REFRESH || 'bebOpiBRahOsajlwachudRLCrusput9T'
// Secret for insider token
config.jwt.secretInsider = process.env.APP_JWT_SECRET_INSIDER || 'MaFotR2GldubRupHEZa4i6idLfA3A8re'
// Expiration for access token in hours
config.jwt.expirationAccess = config.prod ? 1 : 8
// Expiration for refresh token in hours
config.jwt.expirationRefresh = 336 // 14 days
// Expiration for insider token in hours
config.jwt.expirationInsider = config.jwt.expirationAccess

/**
 * CACHE SETTINGS (node-cache)
 */

config.cache = {}
// Buffer time to stay a route open
config.cache.routecode = { stdTTL: 30 } // seconds

/**
 * EXPORT
 */
module.exports = global.Config = Object.freeze(config)

// Globals
require('./globals')