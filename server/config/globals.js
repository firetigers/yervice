'use strict'

// Path to server folder
global.__server = global.Config.paths.server

// Use bluebird as default promise
global.Promise = require('bluebird')

// Codes
global.Code = require('../utils/codes')

// Logger
global.Logger = require('../utils/logger')

// Models
global.Model = require('../models')
