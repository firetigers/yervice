'use strict'

/**
 * Configuration for Logger
 */
const config = global.Config
const production = process.env.NODE_ENV == 'production'

// Log Levels: all < trace < debug < info < warn < error < fatal < mark < off

module.exports = {
  pm2: true,
  appenders: {
    console: {
      type: 'console'
    },
    file: {
      type: 'file',
      filename: config.paths.root + '/logs/server.log',
      maxLogSize: 10485760,
      backups: 3,
      compress: true
    }
  },
  categories: {
    // For production, get log level from config
    default: { appenders: ['console'], level: (production ? config.logLevel : 'all') },
    // Specific log level for other outputs
    file: { appenders: ['file'], level: 'info' }
  }
}