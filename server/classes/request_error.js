'use strict'

// Custom error for requests
class RequestError extends Error {
  constructor(code, status, message) {
    if (message instanceof Error) message = message.toString();
    super(typeof message === 'string' ? message : '')
    this.code = code
    this.status = status
    this.error = true
    this.payload = message
  }
}

// Rename error
RequestError.prototype.name = 'RequestError'

// Export
module.exports = RequestError