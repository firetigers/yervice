'use strict'

const { param, query, body, validationResult } = require('express-validator/check')

function validate(req, res, next) {
  const errs = validationResult(req)
  if (!errs.isEmpty()) return res.jsonError('EAIP', errs.array())
  next()
}

module.exports = { validate, param, query, body }

