'use strict'

/**
 * Access Control Middleware
 * 
 * Makes sure that the user has the right access to routes
 * 
 * This middleware should be placed after `authAccess`
 */

const enabled = global.Config.accessControl

// Default middleware
let access = module.exports = (level) => {
  return (req, res, next) => {
    if (!enabled) return next()
    if (res.locals.user.access_level < level)
      return res.jsonError('EAUA')
    next()
  }
}

// Default access control
access.forAndAbove = access

// Above specified level
access.above = (level) => {
  return (req, res, next) => {
    if (!enabled) return next()
    if (res.locals.user.access_level > level)
      return next()
    res.jsonError('EAUA')
  }
}

// Only for specified level
access.only = (level) => {
  return (req, res, next) => {
    if (!enabled) return next()
    if (res.locals.user.access_level === level)
      return next()
    res.jsonError('EAUA')
  }
}