'use strict'

/**
 * Refresh Token Authentication
 * 
 * Makes sure that the user has the right refresh token
 * 
 * Does a much stricter checking using `FingerprintJs2`
 * and client's ip addresses (proxy chain)
 */

const _take = require('lodash/take')
const passport = require('passport')

module.exports = () => {
  return (req, res, next) => {
    passport.authenticate(
      'jwt_refresh',
      { session: false },
      (err, data) => {
        if (err) return res.jsonError(err)
        if (
          !data.fp ||
          data.fp !== req.body.fingerprint ||
          data.ips !== JSON.stringify(_take(req.ips, 5))
        ) return res.jsonError('EAUA')
        res.locals.user = {
          id: data.user._id,
          login_key: data.user.login_key
        }
        res.locals.expiring = data.expiring
        next()
      }
    )(req, res, next)
  }
}