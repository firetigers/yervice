'use strict'

/**
 * Access Token Authentication
 * 
 * Makes sure that the user has the right token
 */

const passport = require('passport')
const _defaults = require('lodash/defaults')

function authenticate(req, res, opt, next) {
  passport.authenticate(
    opt.strategy,
    { session: false },
    (err, user) => {
      if (err) return res.jsonError(err)
      if (!user && opt.throw(req)) return res.jsonError('EAUA')
      res.locals.user = user
      next()
    }
  )(req, res, next)
}


module.exports = (opt = {}) => {
  _defaults(opt, {
    strategy: 'jwt_access',
    throw: () => true
  })
  return (req, res, next) => {
    authenticate(req, res, opt, next)
  }
}