'use strict'

const _defaults = require('lodash/defaults')

function createMiddleware(opt) {
  return (req, res, next) => {
    Model.Client.findOne(
      {
        user: res.locals.user.id
      },
      '+_id')
      .then(client => {
        if (!client && opt.throw(req)) throw Code.error('EANCL')
        res.locals.client = client
        next()
        return null
      })
      .catch(err => res.jsonError(err))
  }
}

module.exports = (opt = {}) => {
  _defaults(opt, {
    throw: () => true
  })
  return createMiddleware(opt)
}