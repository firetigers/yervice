'use strict'

const _defaults = require('lodash/defaults')

function createMiddleware(opt) {
  return (req, res, next) => {
    Model.Provider.findOne(
      {
        user: res.locals.user.id
      },
      '+_id')
      .then(provider => {
        if (!provider && opt.throw(req)) throw Code.error('EANSP')
        res.locals.provider = provider
        next()
        return null
      })
      .catch(err => res.jsonError(err))
  }
}

module.exports = (opt = {}) => {
  _defaults(opt, {
    throw: () => true
  })
  return createMiddleware(opt)
}