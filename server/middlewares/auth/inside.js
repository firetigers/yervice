'use strict'

/**
 * Insider Login Authentication
 */

const config = global.Config
const enabled = global.Config.accessControl

module.exports = () => (req, res, next) => {
  if (!enabled) return next()
  if (config.codeInside === req.query.code)
    return next()
  res.endError(404)
}