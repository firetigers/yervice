'use strict'

const _defaults = require('lodash/defaults')

module.exports = (model, opt = {}) => {
  _defaults(opt, {
    send: true,
    key: 'result'
  })
  return (req, res, next) => {
    model.findByIdAndDelete(req.params.id || req.body.id)
      .exec()
      .then(doc => {
        if (opt.send) return res.jsonSuccess(doc)
        else res.locals[opt.key] = doc
        next()
        return null
      })
      .catch(err => res.jsonError(err))
  }
}