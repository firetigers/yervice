'use strict'

const { hydrateObject } = require('../../utils/helpers')
const _defaults = require('lodash/defaults')

/**
 * Create a middleware that handles database find query
 */

/**
 * 
 * @param {object} model - Model instance
 * @param {object|function} query - Query object or function that return query object
 * @param {object} opt - Options
 */
module.exports = (model, cond = {}, opt = {}) => {
  _defaults(opt, {
    send: true,
    key: 'result'
  })
  let _isFunc = typeof cond === 'function'
  return (req, res, next) => {
    model.find(
      _isFunc ? cond(req, res) : hydrateObject(cond, req, res),
      opt.select,
      opt.options)
      .then(docs => {
        if (opt.populate) {
          return model.populate(docs, opt.populate)
        }
        return docs
      })
      .then(docs => opt.map ? docs.map(opt.map) : docs)
      .then(docs => {
        if (opt.send) return res.jsonSuccess(docs)
        else res.locals[opt.key] = docs
        next()
        return null
      })
      .catch(err => res.jsonError(err))
  }
}