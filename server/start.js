'use strict'

const config = require('./config')
const _isEmpty = require('lodash/isEmpty')
const _join = require('lodash/join')
const _endsWith = require('lodash/endsWith')
const express = require('express')
const mongoose = require('mongoose')
const next = require('next')
const glob = require('glob')
const fs = require('fs')
const helmet = require('helmet')
const bodyparser = require('body-parser')
const passport = require('passport')
const auths = require('./auths')
const extenders = require('./extenders')
const fixRoot = require('./actions/fixRoot')

const app = next({
  dev: config.dev,
  dir: config.paths.www
})
const handle = app.getRequestHandler()

/**
 * Establish database connection
 */
function connectDatabase(server) {
  let host = config.database.host + (config.database.port ? ':' + config.database.port : ''),
    credentials = config.database.user ? config.database.user + ':' + config.database.password + '@' : '',
    url = 'mongodb://' + credentials
  // Use replica set if specified
  if (!_isEmpty(config.database.replicaSet)) url += _join(config.database.replicaSet, ',')
  // Append host
  else url += host
  // Append database name
  url += '/' + config.database.name
  // Assign default promise to mongoose
  mongoose.Promise = global.Promise
  // Initiate connection
  Logger.console.debug(`Database: connecting to ${host}`)
  return mongoose
    .connect(
      url,
      config.database.options
    )
    .then(() => { Logger.console.debug(`Database: connected`) })
}

/**
 * Configure express server
 */
function configServer(server) {
  // Apply express settings
  let expressSettings = config.server.express
  for (let key in expressSettings) server.set(key, expressSettings[key])
  // Enable security features by Helmet.
  if (config.prod) server.use(helmet())
  // Add body parsers. Only `json and form-urlencoded`, NOT multipart.
  server.use(bodyparser.json())
  server.use(
    bodyparser.urlencoded({
      extended: true
    })
  )
  // Configure passport authentications
  auths(server)
  // Initialize passport.
  server.use(passport.initialize())
  // Load extenders
  extenders(server)
  return server
}

/**
 * Gather all routes
 */
function loadRoutes(server) {
  // Health check, used for production servers
  server.get('/health', (req, res) => { res.status(200).send() })

  // Compile all routers
  let routeFolders = [],
    routePaths = config.paths.routes
  glob.sync('**/*', { cwd: routePaths }).forEach(route => {
    let _isFolder = !_endsWith(route, '.js')
    route = '/' + route.replace(/\.[^/.]+$/, '')
    if (!_endsWith(route, 'index')) {
      server.use(route, require(routePaths + route))
      if (_isFolder) routeFolders.push(route)
    }
  })
  routeFolders.reverse().forEach(route => {
    let _pathDeindex = routePaths + route + '/zindex.js'
    if (fs.existsSync(_pathDeindex)) {
      server.use(route, require(_pathDeindex))
    }
  })

  // Fallback routes
  server.get('*', (req, res) => { return handle(req, res) })
  Logger.console.debug(`Express: routes mounted`)
  return server
}

/**
 * Handle port listening
 */
function startServer(server) {
  return new Promise((resolve, reject) => {
    let host = config.server.host,
      port = config.server.port
    server.listen(port, (err) => {
      if (err) reject(err)
      resolve({ host, port })
    })
  })
}

/**
 * Check for root user and fix any issue
 */
function checkRoot() {
  return fixRoot()
}

/**
 * Result handlers
 */
function success(info) {
  Logger.console.info(`Server: started on ${info.host}:${info.port}`)
}

function error(err) {
  Logger.console.error(err)
}

/**
 * Initiate start sequence
 */
app.prepare()
  .then(connectDatabase)
  .then(checkRoot)
  .then(() => express())
  .then(configServer)
  .then(loadRoutes)
  .then(startServer)
  .then(success)
  .catch(error)