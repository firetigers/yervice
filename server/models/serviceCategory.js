'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Types = Schema.Types

const updated = require('./fields/updated')

const schema = new Schema({
  ...updated,
  
  // TODO: Create index `service:category` to restrict 
  // to only one connection or document
  service: {
    required: true,
    type: Types.ObjectId,
    ref: 'Service'
  },
  category: {
    required: true,
    type: Types.ObjectId,
    ref: 'Category'
  }
});

module.exports = Object.assign(
  mongoose.model('ServiceCategory', schema),
  {}
)