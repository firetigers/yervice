'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Types = Schema.Types

const schema = new Schema({
  email: {
    required: true,
    type: String,
    unique: true
  },
  user: {
    required: true,
    type: Types.ObjectId,
    ref: 'User',
    unique: true
  },
  kind: {
    required: true,
    type: Number,
    min: 1,
    max: 4
  },
  token: {
    required: true,
    type: String
  },
  date_created: {
    required: true,
    type: Date,
    default: Date.now
  }
});

module.exports = Object.assign(
  mongoose.model('Confirmation', schema),
  {
    KIND: {
      ACTIVATION: 1,
      DEACTIVATION: 2,
      PROMOTION: 3,
      DEMOTION: 4
    }
  }
)