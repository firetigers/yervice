'use strict'

const mongoose = require('mongoose')
const Types = mongoose.Schema.Types

module.exports = {
  date_updated: {
    require: true,
    default: Date.now,
    type: Date
  },
  updated_by: {
    require: true,
    ref: 'User',
    type: Types.ObjectId
  }
}