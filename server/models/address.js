'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Types = Schema.Types
const updated = require('./fields/updated')

/*

A reasonable format for storing addresses would be as follows:

- Address Lines 1-4
- Locality  [Business Bay]  [Cagayan De Oro]
- Province  [-]             [Misamis Oriental]
- Region    [Dubai]         [Northern Mindanao]
- Postcode (or zipcode)
- Country [United Arab Emirates]

Address lines 1-4 can hold components such as:

- Building
- Sub-Building
- Premise number (house number)
- Premise Range
- Thoroughfare
- Sub-Thoroughfare
- Double-Dependent Locality
- Sub-Locality

*/

const schema = new Schema({
  ...updated,

  user: {
    required: true,
    ref: 'User',
    type: Types.ObjectId
  },

  line1: {
    require: true,
    type: String,
    maxlength: 60
  },
  line2: {
    type: String,
    maxlength: 60
  },

  locality: {
    require: true,
    type: String,
    maxlength: 40
  },
  region: {
    require: true,
    type: String,
    maxlength: 40
  },
  country: {
    require: true,
    type: String,
    maxlength: 40
  }

});

module.exports = Object.assign(
  mongoose.model('Address', schema),
  {}
)