'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Types = Schema.Types

const updated = require('./fields/updated')

const schema = new Schema({
  ...updated,
  
  name: {
    required: true,
    unique: true,
    type: String
  },
  parent: {
    ref: 'Category',
    default: null,
    type: Types.ObjectId
  },
  active: {
    required: true,
    default: true,
    type: Boolean
  }
});

module.exports = Object.assign(
  mongoose.model('Category', schema),
  {}
)