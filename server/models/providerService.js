'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Types = Schema.Types

const schema = new Schema(
  {
    // TODO: Create index `provider:service` to 
    // restrict to only one document
    provider: {
      required: true,
      type: Types.ObjectId,
      ref: 'Provider'
    },
    service: {
      required: true,
      type: Types.ObjectId,
      ref: 'Service'
    },
    price: {
      required: true,
      type: Number
    }
  }
);

module.exports = Object.assign(
  mongoose.model('ProviderService', schema),
  {}
)
