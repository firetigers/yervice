'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Types = Schema.Types
const updated = require('./fields/updated')
const _ = require('lodash')
const shortid = require('shortid')

const STATUS = {
  /**
   * Default status
   */
  HOLD: 'HOLD',
  /**
   * Order has been places and searching for service providers.
   * 
   * Once search is complete (all service providers accepted the job),
   * client will get notified and will be able to see all service providers.
   * 
   * If client don't want the service provider, he can kick him out and
   * the system will search for new one.
   * 
   * Both parties can cancel job order anytime without penalty.
   */
  SEARCHING: 'SEARCHING',
  /**
   * Client accepted all found service providers and now waiting for the time of job.
   * If a party (SP or Client) canceled the job/order, a penalty must apply.
   * 
   * System creates a comence cron job at the time specified by client.
   */
  WAITING: 'WAITING',
  /**
   * Service providers arrived to clients location and are now working.
   * 
   * System create a time's-up cron job.
   * And after time's-up, system create a to-complete cron job,
   * for client to mark complete or reject
   */
  WORKING: 'WORKING',
  /**
   * Client marked the job completed.
   * 
   * Client must mark the job complete within the buffer time (within 1 hour after),
   * otherwise, the system will auto mark the job as completed.
   * 
   * If client wish to reject the result of job, he must also do it before
   * the butter time.
   */
  COMPLETED: 'COMPLETED',
  /**
   * Job canceled
   */
  CANCELED: 'CANCELED',
  /**
   * Job rejected by client, maybe not happy with the result of job.
   */
  REJECTED: 'REJECTED'
}

/**
 * Order schema
 */
const schema = new Schema({
  ...updated,

  // Replace mongoose id format
  _id: {
    type: String,
    default: shortid.generate
  },

  service: {
    required: true,
    type: Types.ObjectId,
    ref: 'Service'
  },

  client: {
    required: true,
    type: Types.ObjectId,
    ref: 'Client'
  },

  status: {
    require: true,
    type: String,
    default: STATUS.HOLD,
    enum: _.values(STATUS)
  },

  location: {
    require: true,
    type: String
  },

  // Date and time
  datetime: {
    require: true,
    type: Date
  },

  // All attribute values
  payload: {
    require: true,
    type: Object
  }

});

module.exports = Object.assign(
  mongoose.model('Order', schema),
  {
    STATUS
  }
)