'use strict'

module.exports = {
  Order: require('./order'),
  Address: require('./address'),
  ProviderService: require('./providerService'),
  ServiceCategory: require('./serviceCategory'),
  Service: require('./service'),
  Category: require('./category'),
  Client: require('./client'),
  Provider: require('./provider'),
  Confirmation: require('./confirmation'),
  Account: require('./account'),
  User: require('./user')
}