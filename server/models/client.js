'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Types = Schema.Types

const updated = require('./fields/updated')

const schema = new Schema({
  ...updated,
  
  user: {
    required: true,
    type: Types.ObjectId,
    ref: 'User',
    unique: true
  },
  date_created: {
    required: true,
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Client', schema)
