'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const random = require('crypto-random-string')

const updated = require('./fields/updated')

const schema = new Schema({
  ...updated,
  
  /**
   * PRIVATE
   */
  email: {
    required: true,
    type: String,
    unique: true,
    select: false
  },
  password: {
    required: true,
    type: String,
    select: false
  },
  access_level: {
    required: true,
    type: Number,
    select: false,
    default: 0,
    min: 0,
    max: 5
  },
  // Crypto key to refresh access token (length=32)
  // The key should also be changed if user changed the password
  login_key: {
    required: true,
    type: String,
    select: false,
    default: () => random(32)
  },

  /**
   * PUBLIC
   */
  avatar: {
    type: String
  },
  firstname: {
    required: true,
    type: String
  },
  lastname: {
    required: true,
    type: String
  },
  birthdate: {
    required: true,
    select: false,
    type: Date
  },
  gender: {
    required: true,
    type: String
  },
  status: {
    required: true,
    type: Number,
    default: 0,
    min: 0,
    max: 3
  },
  date_created: {
    required: true,
    type: Date,
    default: Date.now
  }
});

/**
 * Exports and static enums
 */
module.exports = Object.assign(
  mongoose.model('User', schema),
  {
    GENDER: {
      MALE: 'M',
      FEMALE: 'F'
    },
    STATUS: {
      PENDING: 0,
      ACTIVE: 1,
      INACTIVE: 2,
      BAN: 3
    },
    ACCESS_LEVEL: {
      USER: 0,
      TESTER: 1,
      MODERATOR: 2,
      ADMIN: 3,
      SUPERADMIN: 4,
      ROOT: 5
    }
  }
)