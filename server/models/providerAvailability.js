'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Types = Schema.Types

const schema = new Schema(
  {
    provider: {
      required: true,
      type: Types.ObjectId,
      ref: 'Provider'
    },
    day: {
      required: true,
      ref: Number,
      min: 1,
      max: 7
    },
    // 23:59 Format
    time_start: {
      required: true,
      type: Number,
      min: 0.00,
      max: 23.59
    },
    // 23:59 Format
    time_end: {
      required: true,
      type: Number,
      min: 0.00,
      max: 23.59
    }
  }
);

module.exports = Object.assign(
  mongoose.model('ProviderAvailability', schema),
  {
    DAY: {
      MON: 1, TUE: 2, WED: 3, THU: 4, FRI: 5, SAT: 6, SUN: 7
    }
  }
)
