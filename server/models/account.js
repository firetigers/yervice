'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Types = Schema.Types

const updated = require('./fields/updated')

const schema = new Schema({
  ...updated,

  user: {
    required: true,
    type: Types.ObjectId,
    ref: 'User'
  },
  type: {
    // S (service provider) | C (client)
    required: true,
    type: String
  },
  date_created: {
    type: Date,
    default: Date.now
  }
});

module.exports = Object.assign(
  mongoose.model('Account', schema),
  {
    TYPE: {
      SERVICE_PROVIDER: 'S',
      CLIENT: 'C'
    }
  }
)