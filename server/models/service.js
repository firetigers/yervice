'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const updated = require('./fields/updated')
const _ = require('lodash')

const PRICING = {
  HOUR: 'HOUR',
  JOB: 'JOB',
  ITEM: 'ITEM'
}

const schema = new Schema({
  ...updated,

  name: {
    required: true,
    unique: true,
    type: String
  },
  description: {
    type: String
  },
  images: {
    banner: String,
    thumbnail: String
  },
  // Is service can be done with multiple provider or not
  solo: {
    require: true,
    type: Boolean,
    default: false
  },
  // Pricing type
  pricing: {
    require: true,
    type: String,
    default: PRICING.HOUR,
    enum: _.values(PRICING)
  },
  // Default price, will be replaced in `ProviderService` model
  price: {
    required: true,
    type: Number,
    min: 0
  },
  active: {
    required: true,
    default: true,
    type: Boolean
  }
});

module.exports = Object.assign(
  mongoose.model('Service', schema),
  {
    PRICING
  }
)