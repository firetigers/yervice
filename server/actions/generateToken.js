'use strict'

const _take = require('lodash/take')
const config = global.Config
const jwt = require('jsonwebtoken')
const uuidv4 = require('uuid/v4');
const crypto = require(__server + '/utils/crypto')
const random = require('crypto-random-string')

function encryptFingerprint(fp, ips, key) {
  return crypto.encrypt(
    fp + // Fingerprint
    '@' + JSON.stringify(_take(ips, 5)) + // Ip address
    '@' + random(32) // Salt
    , key)
}

function generate(payload, secret, expiration) {
  return new Promise((resolve, reject) => {
    jwt.sign(
      payload,
      secret,
      { expiresIn: 3600 * expiration },
      (err, token) => {
        if (err) return reject(Code.error('EAGE'))
        resolve(token)
      }
    )
  })
}

/**
 * For normal users
 */
module.exports = (user, fp, ips, refresh) => {
  return Promise.all(
    [
      // Access token
      generate(
        {
          // jti: uuidv4(),
          // typ: 'access',
          uid: user.id
        },
        config.jwt.secretAccess,
        config.jwt.expirationAccess
      ),
      // Refresh token
      !refresh ? null : generate(
        {
          // jti: uuidv4(),
          // typ: 'refresh',
          key: encryptFingerprint(fp, ips, user.login_key),
          uid: user.id
        },
        config.jwt.secretRefresh,
        config.jwt.expirationRefresh
      )
    ])
    .then(results => ({
      access_token: results[0],
      refresh_token: results[1]
    }))
}

/**
 * For insiders
 */
module.exports.forInsider = (user, fp, ips) => {
  return generate(
    {
      // jti: uuidv4(),
      // key: encryptFingerprint(fp, ips, user.login_key),
      // typ: 'insider',
      uid: user.id
    },
    config.jwt.secretInsider,
    config.jwt.expirationInsider
  ).then(token => ({ insider_token: token }))
}