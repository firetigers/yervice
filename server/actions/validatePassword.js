'use strict'

const bcrypt = require('bcryptjs')

/**
 * Validate user by
 * - email or userid
 * - password
 */
module.exports = (userid, email, password) => {
  return Model.User.findOne(
    {
      $or: [
        { _id: userid },
        { email }
      ],
      status: Model.User.STATUS.ACTIVE
    },
    'password login_key')
    .exec()
    .then(user => {
      if (!user) throw Code.error('EANF')
      // Check password
      return bcrypt.compare(password, user.password)
        .then(isMatch => {
          if (isMatch) {
            return user;
          } else {
            throw Code.error('EAMM')
          }
        })
    })
}