'use strict'

const uuidv4 = require('uuid/v4')

module.exports = (email, id) => {
  let token = uuidv4()
  return Model.Confirmation.updateOne(
    {
      email
    }, {
      email,
      token,
      user: id,
      kind: Model.Confirmation.KIND.ACTIVATION,
      date_created: Date.now()
    }, {
      new: true,
      upsert: true
    })
    .exec()
    .then(() => ({
      id,
      token
    }))
}