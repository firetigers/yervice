'use strict'

const config = global.Config

/**
 * This action fixes root issues
 * - Multiple root users
 * - Root email mismatched (db vs env)
 */
module.exports = () => {
  // Get root user from db
  const oFind = {
    access_level: Model.User.ACCESS_LEVEL.ROOT
  }
  return Model.User.find(oFind, 'email')
    .exec()
    .then((roots) => {
      // If no root then thats good
      if (roots.length <= 0) return 0
      // Root exist validate it
      if (roots.length > 1 ||
        (roots[0].email !== config.rootEmail)) {
        // Multiple root or mismatch root email.
        // Demote all roots to user level.
        return Model.User.updateMany(oFind, {
          access_level: Model.User.ACCESS_LEVEL.USER
        }).exec().return(2)
      }
      // Single root and email is matching
      return 1
    })
}