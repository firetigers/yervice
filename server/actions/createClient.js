'use strict'

module.exports = (uid) => {
  return Model.Client.findOne({ user: uid })
    .exec()
    .then(doc => {
      if (doc) throw Code.error('EAAX')
      return Model.Client.create({
        user: uid
      })
    })
}