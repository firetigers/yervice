'use strict'

module.exports = (uid) => {
  return Model.Provider.findOne({ user: uid })
    .exec()
    .then(doc => {
      if (doc) throw Code.error('EAAX')
      return Model.Provider.create({
        user: uid
      })
    })
}