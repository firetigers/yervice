'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const authInside = require(__server + '/middlewares/auth/inside')
const authRouteCode = require(__server + '/middlewares/auth/routeCode')

// Must valid user
router.get('/', authInside())

// The code is cached inside the authRouteCode middleware
// router.get('/dashboard', authRouteCode)
