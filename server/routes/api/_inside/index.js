'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const authAccess = require(__server + '/middlewares/auth/access')
const access = require(__server + '/middlewares/accessControl')
const generateToken = require(__server + '/actions/generateToken')
const validatePassword = require(__server + '/actions/validatePassword')
const authRouteCode = require(__server + '/middlewares/auth/routeCode')

/**
 * User must be an insider
 */
router.all('*', authAccess(), access.above(Model.User.ACCESS_LEVEL.USER))

// Get access token of insider
router.post('/login', (req, res) => {
  // #SECURITY: Prevent brute-force login
  Promise.resolve()
    .then(() => {
      return validatePassword(res.locals.user.id, null, req.body.password)
    })
    .then(user => {
      // Create a route code for dashboard
      return authRouteCode.open('/_inside/dashboard')
        .then(code => {
          return generateToken.forInsider(user, req.body.fingerprint, req.ips)
            .then(data => {
              data.route_code = code
              return data
            })
        })
    })
    .then(data => res.jsonSuccess(data))
    .catch(err => res.jsonError(err))
})