'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const createClient = require(__server + '/actions/createClient')

/**
 * Creates a client account for current user
 */
router.post(
  '/',
  (req, res) => {
    createClient(res.locals.user.id)
      .then(client => res.jsonSuccess(client))
      .catch(err => res.jsonError(err))
  }
)