'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const authAccess = require(__server + '/middlewares/auth/access')
const modelFindOne = require('../../../middlewares/model/findOne')

// Require auth access for all methods including GET
router.all('*', authAccess())

/**
 * Send current user's client account
 */
router.get(
  '/',
  modelFindOne(
    Model.Client,
    {
      user: 'locals.user.id'
    }
  )
)