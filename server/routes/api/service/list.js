'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const modelList = require(__server + '/middlewares/model/list')

router.get(
  '/',
  modelList(
    Model.Service,
    {
      active: true
    },
    {
      select: '-description -date_updated -updated_by'
    }
  )
)