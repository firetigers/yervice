'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const _values = require('lodash/values')
const accessControl = require(__server + '/middlewares/accessControl')
const { validate, body } = require(__server + '/middlewares/validation')
const modelUpsert = require(__server + '/middlewares/model/upsert')

router.post(
  '/',
  // Access control
  accessControl(Model.User.ACCESS_LEVEL.ADMIN),
  // Input validation
  [
    body('name').isLength({ min: 1 }),
    body('pricing').isIn(_values(Model.Service.PRICING)),
    body('price').isInt({ min: 0 })
  ],
  validate,
  // Write data
  modelUpsert(
    Model.Service,
    (req, res) => {
      // https://picsum.photos/200/300?image=0
      return {
        'name': req.body.name,
        'description': req.body.description,
        'active': req.body.active,
        'pricing': req.body.pricing,
        'price': req.body.price,
        'images.banner': req.body.image_banner,
        'images.thumbnail': req.body.image_thumbnail
      }
    }
  )
)