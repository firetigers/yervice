'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const authAccess = require(__server + '/middlewares/auth/access')

// Require auth for write methods
router.post('*', authAccess())
router.delete('*', authAccess())