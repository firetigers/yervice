'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const modelList = require(__server + '/middlewares/model/list')

router.get(
  '/:id',
  modelList(
    Model.ProviderService,
    {
      service: 'params.id'
    },
    {
      populate: {
        path: 'provider',
        populate: {
          path: 'user'
        }
      },
      map: doc => doc.provider
    }
  )
)