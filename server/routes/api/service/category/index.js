'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const accessControl = require(__server + '/middlewares/accessControl')
const { validate, body } = require(__server + '/middlewares/validation')
const modelUpsert = require(__server + '/middlewares/model/upsert')
const modelMustNotExist = require(__server + '/middlewares/model/mustNotExist')
const modelMustExistById = require(__server + '/middlewares/model/mustExistById')

/**
 * This service to category
 */
router.post(
  '/',
  // Access control
  accessControl(Model.User.ACCESS_LEVEL.ADMIN),
  // Input validation
  [
    body('service').isMongoId(),
    body('category').isMongoId()
  ],
  validate,
  // Make sure documents exists in database
  modelMustExistById(Model.Service, 'body.service'),
  modelMustExistById(Model.Category, 'body.category'),
  // Pevent multiple relationship document
  modelMustNotExist(
    Model.ServiceCategory,
    {
      service: 'body.service',
      category: 'body.category'
    }
  ),
  // Write data
  modelUpsert(
    Model.ServiceCategory,
    [
      'service',
      'category'
    ]
  )
)