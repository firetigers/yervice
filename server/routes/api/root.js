'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const config = global.Config
const fixRoot = require('../../actions/fixRoot')
const authAccess = require(__server + '/middlewares/auth/access')

/**
 * This route is to activate the root
 * email specified in config.rootEmail
 */
router.post(
  '/claim',
  authAccess(),
  (req, res) => {
    // Fix root first
    Promise.resolve()
      .then(() => {
        if (config.rootEmail !== req.body.email)
          throw Code.error('EAIP')
        return fixRoot()
      })
      .then(result => {
        // 0 = no root found
        // 1 = single root found and email is matching
        // 2 = issue was found and fixed (demoted all root to user level)
        if (result === 1) throw Code.error('EAAX')
        return Model.User.findOne(
          {
            _id: res.locals.user.id,
            status: Model.User.STATUS.ACTIVE
          },
          '+email')
          .exec()
      })
      .then(user => {
        if (!user) throw Code.error('EANF')
        // Make sure user email match in body and database
        if (user.email !== req.body.email) throw Code.error('EAIP')
        // Ok. Change access level to root
        user.access_level = Model.User.ACCESS_LEVEL.ROOT
        return user.save()
      })
      .then(() => res.send('Root Activated'))
      .catch((err) => res.jsonError(err))
  }
)