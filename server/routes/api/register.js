'use strict'

const config = global.Config
const express = require('express')
const router = express.Router()
module.exports = router

const gravatar = require("gravatar")
const bcrypt = require('bcryptjs')
const createActivation = require(__server + '/actions/createActivation')
const createProvider = require(__server + '/actions/createProvider')
const createClient = require(__server + '/actions/createClient')
const { validate, body } = require(__server + '/middlewares/validation')
const _values = require('lodash/values')

router.post(
  '/',
  [
    body('email').isEmail(),
    body('password').isLength({ min: 8 }),
    body('firstname').isLength({ min: 1 }),
    body('lastname').isLength({ min: 1 }),
    body('birthdate').isISO8601(),
    body('gender').isIn(_values(Model.User.GENDER)),
    body('type').isIn(['client', 'provider']),
    body('addr_line1').isLength({ min: 2 }),
    body('addr_line2').isLength({ min: 2 }),
    body('addr_country').isISO31661Alpha2(), // AE
    body('addr_region').isLength({ min: 2 }), // Dubai
    body('addr_locality').isLength({ min: 2 }) // Business Bay
  ],
  validate,
  (req, res) => {
    Model.User.findOne(
      {
        email: req.body.email
      })
      .exec()
      // Encrypting password
      .then(user => {
        if (user) throw Code.error('EAAX')
        // Generate password hash
        return new Promise((resolve, reject) => {
          bcrypt.genSalt(10, (err, salt) => {
            if (err) return reject(Code.error('EAGE', '1782950119079936'))
            bcrypt.hash(req.body.password, salt, (err, hash) => {
              if (err) return reject(Code.error('EAGE', '7256632893374464'))
              resolve(hash)
            })
          })
        })
      })
      // Creating user document
      .then(hash => {
        return Model.User.create({
          email: req.body.email,
          password: hash,
          // more fields ...
          avatar: gravatar.url(req.body.email, null, true),
          firstname: req.body.firstname,
          lastname: req.body.lastname,
          birthdate: req.body.birthdate,
          gender: req.body.gender
        })
      })
      // Creating address document
      .then(user => {
        return Model.Address.create({
          user: user.id,
          line1: req.body.addr_line1,
          line2: req.body.addr_line2,
          country: req.body.addr_country,
          region: req.body.addr_region,
          locality: req.body.addr_locality
        }).return(user)
      })
      // Creating activation link to be sent to email
      .then(user => {
        let prom
        if (req.body.type === 'client') prom = createClient(user.id)
        else if (req.body.type === 'provider') prom = createProvider(user.id)
        else throw Code.errer('EAIP')
        // Chain promise
        return prom.then(() => {
          // Create activation document
          return createActivation(user.email, user.id)
        })
      })
      // Sending activation link
      .then(activation => {
        // #CHECK: Do not respond with activation code on production
        res.jsonSuccess(
          config.demo ? {
            url: `/api/activation?user=${activation.id}&token=${activation.token}`,
            id: activation.id,
            token: activation.token
          } : null)
      })
      // Roll back on error
      .catch(err => {
        // Roll back! Delete user
        switch (err.code) {
          case 'EAAX':
            // Nothing to do
            break
          default:
            Model.User.findOneAndDelete({ email: req.body.email })
              .exec()
              .then(user => {
                if (!user) return null
                return Promise.all([
                  Model.Address.findOneAndDelete({ user: user.id }).exec(),
                  Model.Provider.findOneAndDelete({ user: user.id }).exec(),
                  Model.Client.findOneAndDelete({ user: user.id }).exec()
                ])
              })
        }
        res.jsonError(err)
      })
  }
)