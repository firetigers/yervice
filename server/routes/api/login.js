'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const authRefresh = require(__server + '/middlewares/auth/refresh')
const generateToken = require(__server + '/actions/generateToken')
const validatePassword = require(__server + '/actions/validatePassword')

function getUserData(data) {
  return Promise.resolve(data)
    .then(data => {
      // Convert to real user model
      return Model.User.findById(data.user.id).exec()
        .then(user => {
          data.user = user
          return data
        })
    })
    .then(data => {
      return Promise.all(
        [
          Model.Provider.findOne({ user: data.user.id }).exec(),
          Model.Client.findOne({ user: data.user.id }).exec(),
          Model.Address.find({ user: data.user.id }).exec()
        ])
        .then(results => {
          return {
            ...data,
            provider: results[0],
            client: results[1],
            user: {
              ...(data.user.toObject()),
              addresses: results[2]
            }
          }
        })
    })
}

/**
 * Log user in. Creates new token.
 * 
 * Data:
 * email - User email
 * password - User password
 * fingerprint - Client's hash from Fingerprintjs2
 */
router.post('/', (req, res) => {
  // #SECURITY: Prevent brute-force login
  validatePassword(
    null,
    req.body.email,
    req.body.password)
    .then(user => {
      return generateToken(user, req.body.fingerprint, req.ips, true)
        .then(token => ({ ...token, user }))
    })
    .then(getUserData)
    .then(data => res.jsonSuccess(data))
    .catch(err => res.jsonError(err))
})

/**
 * Creates new access token from refresh token.
 * 
 * Data:
 * fingerprint - Client's hash from Fingerprintjs2
 */
router.post(
  '/refresh',
  authRefresh(),
  (req, res) => {
    // #SECURITY: Prevent brute-force login for refresh token
    // Create new refresh token if old token is expiring
    let { user } = res.locals
    generateToken(
      user,
      req.body.fingerprint,
      req.ips,
      res.locals.expiring)
      .then(token => ({ ...token, user }))
      .then(getUserData)
      .then(data => res.jsonSuccess(data))
      .catch(err => res.jsonError(err))
  }
)