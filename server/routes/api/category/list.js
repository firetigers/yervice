'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const modelList = require(__server + '/middlewares/model/list')

router.get(
  '/',
  modelList(
    Model.Category,
    {
      active: true
    },
    {
      select: '-date_modified -modified_by'
    }
  )
)