'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const modelFindById = require(__server + '/middlewares/model/findById')

router.get(
  '/:id',
  modelFindById(
    Model.Category,
    {
      populate: 'parent'
    }
  )
)