'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const accessControl = require(__server + '/middlewares/accessControl')
const { validate, body } = require(__server + '/middlewares/validation')
const modelDeleteById = require(__server + '/middlewares/model/deleteById')

router.delete(
  '/',
  // Access control
  accessControl(Model.User.ACCESS_LEVEL.ROOT),
  // Input validation
  [
    body('id').isMongoId()
  ],
  validate,
  // Delete document data
  // The id is from `body.id`
  modelDeleteById(Model.Category)
)