'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const modelList = require(__server + '/middlewares/model/list')

router.get(
  '/:id',
  modelList(
    Model.ServiceCategory,
    (req) => {
      return {
        category: req.params.id
      }
    },
    {
      populate: [
        'service'
      ]
    }
  )
)