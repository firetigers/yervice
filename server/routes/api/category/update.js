'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const accessControl = require(__server + '/middlewares/accessControl')
const { validate, body } = require(__server + '/middlewares/validation')
const modelUpsert = require(__server + '/middlewares/model/upsert')

router.post(
  '/',
  // Access control
  accessControl(Model.User.ACCESS_LEVEL.ADMIN),
  // Input validation
  [
    body('name').isLength({ min: 1 })
  ],
  validate,
  // Write data
  modelUpsert(
    Model.Category,
    [
      'name',
      'parent'
    ]
  )
)