'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const createProvider = require(__server + '/actions/createProvider')

/**
 * Creates a service provider account for current user
 */
router.post(
  '/',
  (req, res) => {
    createProvider(res.locals.user.id)
      .then(provider => res.jsonSuccess(provider))
      .catch(err => res.jsonError(err))
  }
)