'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const authAccess = require(__server + '/middlewares/auth/access')
const modelFindOne = require('../../../middlewares/model/findOne')

// Require auth for write methods
router.post('*', authAccess())
router.delete('*', authAccess())

/**
 * Send current user's provider account
 */
router.get(
  '/',
  // Set `authAccess` for this route only
  authAccess(),
  modelFindOne(
    Model.Provider,
    {
      user: 'locals.user.id'
    }
  )
)