'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const modelFindOne = require('../../../middlewares/model/findOne')

/**
 * Send other user's client account
 */
router.get(
  '/:id',
  modelFindOne(
    Model.Provider,
    {
      user: 'params.id'
    },
    {
      populate: 'user'
    }
  )
)