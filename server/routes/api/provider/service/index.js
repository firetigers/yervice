'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const authProvider = require(__server + '/middlewares/auth/provider')
const { validate, body } = require(__server + '/middlewares/validation')
const modelUpsert = require(__server + '/middlewares/model/upsert')
const modelMustExistById = require(__server + '/middlewares/model/mustExistById')

// Require SP account for write methods
router.post('*', authProvider())
router.delete('*', authProvider())

router.post(
  '/',
  [
    body('price').isInt({ min: 0 })
  ],
  validate,
  modelMustExistById(
    Model.ProviderService
  ),
  modelUpsert(
    Model.ProviderService,
    [
      'price'
    ]
  )
)