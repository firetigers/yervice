'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const accessControl = require(__server + '/middlewares/accessControl')
const { validate, body } = require(__server + '/middlewares/validation')
const modelDeleteById = require(__server + '/middlewares/model/deleteById')
const modelFindOne = require(__server + '/middlewares/model/findOne')

// PRIVATE: Must be an SP account
router.delete(
  '/',
  accessControl(Model.User.ACCESS_LEVEL.USER),
  // Input validation
  [
    body('id').isMongoId()
  ],
  validate,
  
  // TODO: Make sure SP owns the document

  // Delete document data
  // The id is from `body.id`
  modelDeleteById(Model.ProviderService)
)