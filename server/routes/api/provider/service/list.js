'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const modelList = require(__server + '/middlewares/model/list')
const authAccess = require(__server + '/middlewares/auth/access')
const authProvider = require(__server + '/middlewares/auth/provider')

router.get(
  '/:id?',
  // If `id` is provided, do not throw error, continue instead
  authAccess({
    throw: req => !req.params.id
  }),
  authProvider({
    throw: req => !req.params.id
  }),
  // Retrieve and send list
  modelList(
    Model.ProviderService,
    (req, res) => {
      return {
        // Get id from param or from current user provider
        provider: req.params.id || (res.locals.provider && res.locals.provider.id)
      }
    },
    {
      populate: {
        path: 'service'
      }
    }
  )
)