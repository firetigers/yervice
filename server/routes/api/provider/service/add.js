'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const accessControl = require(__server + '/middlewares/accessControl')
const { validate, body } = require(__server + '/middlewares/validation')
const modelUpsert = require(__server + '/middlewares/model/upsert')
const modelMustNotExist = require(__server + '/middlewares/model/mustNotExist')
const modelMustExistById = require(__server + '/middlewares/model/mustExistById')

/**
 * Adds a service to a provider
 */
router.post(
  '/',
  // Access control
  accessControl(Model.User.ACCESS_LEVEL.USER),
  // Input validation
  [
    body('service').isMongoId()
  ],
  validate,
  // Make sure service exists in database
  modelMustExistById(Model.Service, 'body.service'),
  // Check duplicates for bridge model
  // `.provider.id` is injected from `authProvider` middleware
  // `modelMustNotExist` will get data from `req` and `res`
  modelMustNotExist(
    Model.ProviderService,
    {
      provider: 'locals.provider.id',
      service: 'body.service'
    }
  ),
  // Write data
  modelUpsert(
    Model.ProviderService,
    // `.result` is injected by preceding middleware
    (req, res) => ({ ...res.locals.result, price: req.body.price }),
    // Prevent from checking id in body from client
    {
      id: false,
      populate: 'service'
    }
  )
)