'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const authAccess = require(__server + '/middlewares/auth/access')

router.all('*', authAccess())