'use strict';

const express = require('express')
const router = express.Router()
module.exports = router

router.get('/', (req, res) => {
  Model.Confirmation.findOne({
    user: req.query.user
  }).exec()
    .then(confirm => {
      if (!confirm) throw Code.error('EANF')
      if (confirm.token !== req.query.token) throw Code.error('EAMM')
      // Token verified and activate user
      return confirm.remove().then(() => {
        return Model.User.updateOne(
          {
            _id: confirm.user
          }, {
            status: Model.User.STATUS.ACTIVE
          }).exec()
      })
    })
    .then(() => res.jsonSuccess())
    .catch(err => res.jsonError(err))
})

router.get('/resend', (req, res) => {
  res.jsonSuccess('Not available yet')
})