'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const authAccess = require(__server + '/middlewares/auth/access')

// Require auth for write methods
router.post('*', authAccess())
router.delete('*', authAccess())

router.get('/', (req, res) => {
  // Get current user info
  Model.User.findById(res.locals.user._id)
    .then(user => {
      if (!user) throw Code.error('EANF')
      res.jsonSuccess(user)
    })
    .catch(err => res.jsonError(err))
})