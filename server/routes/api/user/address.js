'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const modelUpsert = require(__server + '/middlewares/model/upsert')
const modelMustExist = require(__server + '/middlewares/model/mustExist')
const modelDeleteById = require(__server + '/middlewares/model/deleteById')
const { validate, body } = require(__server + '/middlewares/validation')

router.post(
  '/',
  [
    body('line1').isLength({ min: 2 }),
    body('line2').isLength({ min: 2 }),
    body('country').isISO31661Alpha2(), // AE
    body('region').isLength({ min: 2 }), // Dubai
    body('locality').isLength({ min: 2 }) // Business Bay
  ],
  validate,
  modelUpsert(
    Model.Address,
    ['line1', 'line2', 'country', 'region', 'locality'],
    {
      moreFields: (req, res) => ({
        user: res.locals.user.id
      })
    }
  )
)


router.delete(
  '/',
  // Input validation
  [
    body('id').isMongoId()
  ],
  validate,
  // Make sure user own the address
  modelMustExist(
    Model.Address,
    { user: 'locals.user.id' }
  ),
  // Delete document data
  // The id is from `body.id`
  modelDeleteById(Model.Address)
)