'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const { validate, param } = require(__server + '/middlewares/validation')

router.get(
  '/:id',
  [
    param('id').isMongoId()
  ],
  validate,
  (req, res) => {
    if (req.params.id) {
      // Get other user info
      Model.User.
        findById(
          req.params.id,
          // Fields to send
          'avatar firstname lastname gender date_created'
        )
        .then(user => {
          if (!user) throw Code.error('EANF')
          res.jsonSuccess(user)
          return null
        })
        .catch(err => res.jsonError(err))
    } else {
      res.jsonError('EAIP')
    }
  }
)