'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const moment = require('moment')
const { validate, body } = require(__server + '/middlewares/validation')
const modelFindOne = require(__server + '/middlewares/model/findOne')
const modelUpsert = require(__server + '/middlewares/model/upsert')
const authClient = require(__server + '/middlewares/auth/client')

router.post(
  // Path
  '/',

  // Auth
  authClient(),

  // Input validation
  [
    body('service').isMongoId(),
  ],
  validate,

  // Get required models, will send error if not exist
  modelFindOne(
    Model.Service,
    {
      _id: 'body.service'
    },
    {
      send: false, // Do not send, attach to `res.locals` instead
      key: 'foundService' // `res.locals.foundService`
    }
  ),

  // Write order
  modelUpsert(
    Model.Order,
    (req, res) => {
      return {
        service: res.locals.foundService.id,
        client: res.locals.client.id, // Took from `authClient` middleware
        location: req.body.location,
        datetime: moment(req.body.date + ' ' + req.body.time),
        payload: {}
      }
    },
    {
      send: false,
      key: 'order'
    }
  ),

  (req, res, next) => {
    next()
  },

  (req, res) => {
    res.jsonSuccess(res.locals.order)
  }

)