'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const authAccess = require(__server + '/middlewares/auth/access')

// Require auth access for all methods including GET
router.all('*', authAccess())