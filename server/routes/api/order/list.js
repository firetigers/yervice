'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

const authClient = require(__server + '/middlewares/auth/client')
const modelList = require(__server + '/middlewares/model/list')

// Require auth access for all methods including GET
router.get(
  '/client',
  authClient(),
  modelList(
    Model.Order,
    {
      client: 'locals.client.id'
    }
  )
)