'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

/**
 * Disabled methods
 */
router.put('*', (req, res) => res.jsonError('EAMN'))