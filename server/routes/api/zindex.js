'use strict'

const express = require('express')
const router = express.Router()
module.exports = router

router.all(
  '*',
  (req, res) => {
    // This will trigger if the route is not handled
    // Return 404 error
    res.jsonError('EGNF', 'Route Not Found')
  }
)