'use strict'

const crypto = require('crypto');

const IV_LENGTH = 16; // For AES, this is always 16
const ALGORITHM = 'aes-256-cbc'

/**
 * Encrypt text with key
 * @param {string} text - Data to encrypt
 * @param {string} key - Key for encryption
 */

function encrypt(text, key) {
  let iv = crypto.randomBytes(IV_LENGTH);
  let cipher = crypto.createCipheriv(ALGORITHM, new Buffer(key), iv);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return iv.toString('hex') + ':' + encrypted.toString('hex');
}

function decrypt(text, key) {
  let textParts = text.split(':');
  let iv = new Buffer(textParts.shift(), 'hex');
  let encryptedText = new Buffer(textParts.join(':'), 'hex');
  let decipher = crypto.createDecipheriv(ALGORITHM, new Buffer(key), iv);
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  return decrypted.toString();
}

module.exports = { decrypt, encrypt };