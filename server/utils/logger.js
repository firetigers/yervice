'use strict'

// Imports
const production = process.env.NODE_ENV == 'production'
const configLogger = require('../config/logger')
const log4js = require('log4js')

// Configuration
log4js.configure(configLogger)

// Expose global own console class
const Logger = {
  console: log4js.getLogger(),
  // For development, log all to console (default)
  file: log4js.getLogger(production ? 'file' : undefined)
}

// Exports
module.exports = Logger