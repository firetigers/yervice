'use strict'

const RequestError = require('../classes/request_error')


/**
 * List of Codes
 * 
 * Errors and Non-errors
 */

function code(code, status, message) {
  codes[code] = { code, status, message }
}

function map(name, lowCode, highCode) {
  // Create class error name
  if (!maps[name]) maps[name] = {}
  // Create lower code, defaults to `_` and 'EAUE'
  maps[name][lowCode || '_'] = highCode || 'EGUE'
}

// Codes holder
let codes = {}
let maps = {}


// - - START OF CODES - - -

/**
 * Generic Success Codes
 */
code('OK', 200, 'Generic Success')

/**
 * Generic Error Codes
 * 
 * eg. EGXX
 * E = Error | G = Generic | XX = Specific Error
 */
code('EGGE', 400, 'Generic Error')
code('EGUE', 400, 'Unkown Error')
code('EGIE', 500, 'Internal Error')
code('EGNF', 404, 'Not Found')

/**
 * API Error Codes
 *
 * eg. EAXX
 * E = Error | A = API | XX = Specific Error
 */
code('EAGE', 400, 'Generic Error')
code('EAIP', 422, 'Invalid Parameters')
code('EAUE', 400, 'Unknown Error')
code('EAUA', 401, 'Unauthorized')
code('EAMM', 401, 'Mismatch')
code('EANO', 403, 'Not Owner')
code('EAMN', 405, 'Method Not Allow')
code('EANF', 404, 'Document Not Found')
code('EANS', 400, 'Document Not Saved')
code('EANC', 400, 'Document Not Created')
code('EANU', 400, 'Document Not Updated')
code('EAND', 400, 'Document Not Deleted')
code('EANP', 400, 'Document Not Updatable')
code('EANR', 400, 'Document Not Creatable')
code('EANL', 400, 'Document Not Deletable')
code('EAAX', 400, 'Document Already Exist')
code('EADK', 400, 'Document Duplicate Key')
code('EACR', 400, 'Circular Reference Error')
code('EANSP', 400, 'No Service Provider Account')
code('EANCL', 400, 'No Client Account')

// - - END OF CODES - - -
// - - START OF CODE MAPS - - -

map('MongoError', 11000, 'EADK')

// - - END OF CODE MAPS - - -

/**
 * Export codes
 */
exports.get = code => {
  return codes[code]
}

exports.getMap = (err) => {
  let errObj,
    errCode = 'EGGE',
    errClass = maps[err.name]
  if (errClass) errCode = errClass[err.code]
  errObj = exports.get(errCode)
  errObj.message = err.message
  return errObj
}

/**
 * Generate an error object without throwing
 * 
 * @param {string} code - 4 Letter Code
 */
exports.error = (code = 'EGGE', msg = '') => {
  var errObj
  if ('string' !== typeof code)
    errObj = exports.getMap(code)
  else if (code[0] !== 'E')
    throw new Error('Unable to generate error code')
  else
    errObj = exports.get(code)
  if (!errObj) throw new Error('Error code not found')
  return new RequestError(errObj.code, errObj.status, msg || errObj.message)
}

exports.success = (code = 'OK') => {
  if (code[0] !== 'O') throw new Error('Unable to generate success code')
  var sucObj = exports.get(code)
  if (!sucObj) throw new Error('Success code not found')
  return { ...sucObj }
}
