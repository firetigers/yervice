'use strict'

/**
 * Collection of function helpers
 */

const _get = require('lodash/get')

// Replaces schema values from req and res
exports.hydrateObject = (schema, req, res) => {
  let element, value, newObj = {}
  for (let key in schema) {
    element = schema[key]
    if (typeof element !== 'string') {
      newObj[key] = element
      continue
    }
    value = _get(req, element)
    if (value !== undefined) {
      newObj[key] = value
    } else {
      value = _get(res, element)
      newObj[key] = value !== undefined ? value : element
    }
  }
  return newObj
}