'use strict'

const config = global.Config
const express = require('express')
const codes = require('../utils/codes')
const RequestError = require('../classes/request_error')

/**
 * 
 * Directly extend express's response prototype to avoid overhead.
 * Overhead that creates function on each and every request.
 *
 * NOTE: ExpressJS does NOT send error stack trace on NODE_ENV=production mode.
 *
 */

express.response.jsonError = function (code = 'EGGE', message = '') {
  if ('string' === typeof code) {
    // Send error by high level code
    // eg. code = 'EAGE' (Generic Error)
    let err = codes.error(code, message)
    this.status(err.status).json(err)
  } else {
    // Send error by low level code
    // eg. code = new Error()
    if (code) {
      // Convert errors that is not our own error class
      if (code.code && !(code instanceof RequestError))
        code = codes.error(code)
      else if (!code.status)
        code = codes.error('EAUE', config.dev ? code : message)
    } else {
      code = codes.error('EAUE')
    }
    this.status(code.status).json(code)
  }
}

express.response.jsonSuccess = function (data, code = 'OK') {
  if ('string' === typeof data) data = { message: data }
  if ('string' === typeof code) {
    let s = codes.success(code)
    this.status(s.status).json({ ...s, success: true, payload: data })
  } else {
    this.status((code && code.status) || 200).json({ ...code, success: true, payload: data })
  }
}

express.response.endError = function (httpCode) {
  this.status(httpCode || 400).end()
}