'use strict'

const exts = [
  require('./express')
]

module.exports = server => {
  for (let i = 0; i < exts.length; i++) {
    if (typeof exts[i] === 'function') exts[i](server);
  }
}