'use strict'

const passport = require('passport')
const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt
const jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken()
const crypto = require(__server + '/utils/crypto')
const timestamp = require(__server + '/utils/timestamp')

module.exports = server => {

  let selectAccess = 'access_level'
  let selectAccessWithData = undefined
  let optionsAccess = {
    jwtFromRequest,
    secretOrKey: Config.jwt.secretAccess
  }

  let selectRefresh = 'login_key'
  let optionsRefresh = {
    jwtFromRequest,
    secretOrKey: Config.jwt.secretRefresh
  }

  function findUser(id, select, done) {
    Model.User.findById(id, select)
      .then(user => {
        if (user) done(null, user)
        else throw Code.error('EAUA')
        return null
      })
      .catch(done)
  }

  function authAccessCallback(payload, done) {
    findUser(payload.uid, selectAccess, done)
  }

  function authAccessUserDataCallback(payload, done) {
    findUser(payload.uid, selectAccessWithData, done)
  }

  function authRefreshCallback(payload, done) {
    // If refresh token is expiring in 24 hours then
    // provide new refresh token
    Model.User.findById(payload.uid, selectRefresh)
      .then(user => {
        if (user && user.login_key) {
          let parts = crypto.decrypt(payload.key, user.login_key).split('@')
          done(null, {
            user: user,
            fp: parts[0],
            ips: parts[1],
            // Include a `expiring` signal
            expiring: timestamp(payload.exp, 24).expiring
          })
        } else {
          throw Code.error('EAUA')
        }
        // Silenced the warning 
        // `a promise was created in a handler but was not returned from it`
        return null
      })
      .catch(done)
  }

  passport.use(
    'jwt_access',
    new JwtStrategy(optionsAccess, authAccessCallback)
  )

  passport.use(
    'jwt_access_user_data',
    new JwtStrategy(optionsAccess, authAccessUserDataCallback)
  )

  passport.use(
    'jwt_refresh',
    new JwtStrategy(optionsRefresh, authRefreshCallback)
  )


}