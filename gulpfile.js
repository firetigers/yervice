'use strict'

const gulp = require('gulp')
const cp_exec = require('child_process').exec
const nodemon = require('gulp-nodemon')
const stylus = require('gulp-stylus')
const autoprefixer = require('gulp-autoprefixer')

// Configs
const configApp = require('./server/config')
const configNodemon = require('./nodemon.json')

// Variables
const www = configApp.paths.www

// Run command
function exec(cmd, done) {
  let cp = cp_exec(cmd, done)
  let dataCb = (data) => console.log(data.trim())
  cp.stdout.on('data', dataCb)
  cp.stderr.on('data', dataCb)
}

/**
 * WATCHERS
 */

// Run everything
gulp.task('dev', ['server', 'style'])

// Run and watch server. Express and Nextjs
gulp.task('server', function (done) {
  configNodemon.done = done
  nodemon(configNodemon)
})

// Run and watch stylus
gulp.task('style', function () {
  gulp.watch(`${www}/style/**/*.styl`, ['build_stylus'])
})

/**
 * BUILDERS
 */

gulp.task('build_stylus', function () {
  return gulp.src(`${www}/style/main.styl`)
    .pipe(stylus())
    .pipe(gulp.dest(`${www}/static/styles`))
})

/**
 * RELEASES
 */

gulp.task('release_stylus', function () {
  return gulp.src(`${www}/static/styles/main.css`)
    .pipe(autoprefixer())
    // add minifier
    .pipe(gulp.dest(`${www}/static/styles`))
})