## Database

* Provider availability
* Price is bound to user-service model

## Pricing

* Per hour (eg. cleaning)
* Per job (eg. mobile repair)

## Service Attributes Samples

# Cleaning

* Number of cleaners?
* How many hours?
* Cleaning materials needed? 
* Window cleaning or Ironing?
* Additional comments

# Moving
# Handyman


## Libs

* node-cache - does not support clustering. user redis or database cacheing instead