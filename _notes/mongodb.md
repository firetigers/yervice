## Compound Index

- Mongodb does not support compound index for ObjectId
- Need to filter on code level

## Delete vs Remove

`remove` is a deprecated function and has been replaced by `delete`
