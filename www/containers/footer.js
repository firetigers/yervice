import { Component } from 'react'
import Link from 'next/link'

export default class extends Component {
  render() {
    return (
      <div className="footer links flex justify-between items-center h3">
        <ul className="list pa0 ma0 w-100 pl3">
          <li className="dib"><Link href="/about"><a className="ph3-ns ph2 pv2">About</a></Link></li>
          <li className="dib"><Link href="/terms"><a className="ph3-ns ph2 pv2">Terms</a></Link></li>
          <li className="dib"><Link href="/privacy"><a className="ph3-ns ph2 pv2">Privacy</a></Link></li>
        </ul>
        <div className="w5 tc"><span className="ml3">2018 Yervice</span></div>
      </div>
    )
  }
} 