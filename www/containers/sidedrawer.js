import { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer'
import Button from '@material-ui/core/Button'
import List from '@material-ui/core/List'
import Divider from '@material-ui/core/Divider'
import OdsListItems from './sidedrawerContent'
import Header from './header'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import Link from 'next/link'

const styles = {
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
  root: {
    flexGrow: 1,
  },
  flex: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  }
}

class SwipeableTemporaryDrawer extends Component {

  state = {
    top: false,
    left: false,
    bottom: false,
    right: false,
  }

  odsSidebar = {
    socialArea: {
      height: '60px',
      width: '100%',
      position: 'relative',
      bottom: '0vh'
    },
    fbIcon: {
      fontSize: "1.5em",
      color: "#555",
      position: "absolute",
      top: "1rem",
      left: "15%"
    },
    IgIcon: {
      fontSize: "1.5em",
      color: "#555",
      position: "absolute",
      top: "1rem",
      left: "45%"
    },
    TwIcon: {
      fontSize: "1.5em",
      color: "#555",
      position: "absolute",
      top: "1rem",
      left: "75%"
    }
  }

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    })
  }

  render() {
    const { classes, user, account, reset } = this.props
    return (
      <div className="dib">
        <IconButton onClick={this.toggleDrawer('right', true)} className="z-2" color="inherit" aria-label="Menu">
          <MenuIcon />
        </IconButton>
        <SwipeableDrawer
          anchor="right"
          open={this.state.right}
          onClose={this.toggleDrawer('right', false)}
          onOpen={this.toggleDrawer('right', true)}>
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer('right', false)}
            onKeyDown={this.toggleDrawer('right', false)}>
            <div className={`${classes.list} mb3 bb b--light-gray pb2`}>
              <div className="tc links pv4 mb2 bb b--light-gray">
                {
                  user.id ? (
                    <div>
                      <Link href="/user">
                        <a className="b ph3 dib">
                          <img src={user.avatar} className="br-100 pa1 ba b--black-10 dib mb3" />
                          <br />
                          {user.firstname} {user.lastname}
                        </a>
                      </Link>
                    </div>
                  ) : (
                      <div>
                        <img className="w4 center db" src="/static/images/logos/logo-dark-color.svg" />
                      </div>
                    )
                }
              </div>
              <List><OdsListItems user={user} account={account} reset={reset} /></List>
            </div>
            <div style={this.odsSidebar.socialArea} >
              <i className="icon-facebook" style={this.odsSidebar.fbIcon} ></i>
              <i className="icon-instagram" style={this.odsSidebar.IgIcon} ></i>
              <i className="icon-twitter" style={this.odsSidebar.TwIcon} ></i>
            </div>
          </div>
        </SwipeableDrawer>
      </div>
    )
  }
}

SwipeableTemporaryDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(SwipeableTemporaryDrawer)