import { Component } from 'react'
import Link from 'next/link'
import Router from 'next/router'
import Sidedrawer from './sidedrawer'
import { connect } from 'react-redux'
import { reset } from '../store/actions/auth'

class Header extends Component {

  state = {}

  links = {
    logo: (
      <Link href="/">
        <a className="dib pl3">
          <img className="w5 db" src="/static/images/logos/logo-dark-color.svg" />
        </a>
      </Link>
    ),
    howitworks: (
      <Link href="/whatisyervice">
        <a className="pv2 ph3 dib">
          <i className="icon-information-solid f3 mr2 v-mid"></i>
          <span className="b v-mid">WHAT IS YERVICE?</span>
        </a>
      </Link>
    ),
    home: (
      <Link href="/">
        <a className="pv2 ph3 dib">
          <i className="icon-home3 f3 mr2 v-mid"></i>
          <span className="b v-mid">Home</span>
        </a>
      </Link>
    ),
    dashboard: (
      <Link href="/">
        <a className="pv2 ph3 dib">
          <i className="icon-home3 f3 mr2 v-mid"></i>
          <span className="b v-mid">Dashboard</span>
        </a>
      </Link>
    )
  }

  constructor(props) {
    super(props)
    if (global.document)
      this.isLoginPage = Router.route == '/login'
  }

  render() {
    let { user, account, reset } = this.props
    return (
      <div className="header links fixed w-100 bg-almost-white flex items-center justify-between z-999">
        <div className="logo w6 nowrap ml0 ml2-l">
          {this.links[this.props.logo || 'logo']}
        </div>
        <div className="menu w-100">
          {this.props.children}
        </div>
        <div className="user w5 tr">
          {
            user.id ? (
              <Link href="/dashboard">
                <a key="user" className="dib nowrap">
                  <img src={user.avatar} className="br-100 pa1 ba b--black-10 dib v-mid mr2 mr3-l" />
                  <span className="v-mid b dn dib-ns">{user.firstname}</span>
                </a>
              </Link>
            ) : (!this.isLoginPage && (
              <Link href="/login">
                <a key="login" className="ph3 pv2 dn dib-l">
                  <span className="fw7 v-mid">Login</span>
                </a>
              </Link>
            ))
          }
        </div>
        <div className="w4 tc">
          <Sidedrawer user={user} account={account} reset={reset} />
        </div>
      </div>
    )
  }
}

const stateProps = ({ user, account }) => ({
  user,
  account
})

const dispatchProps = dispatch => ({
  reset: () => dispatch(reset())
})

export default connect(stateProps, dispatchProps)(Header)