import { Component } from 'react'
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PersonAddIcon from '@material-ui/icons/PersonAddRounded';
import LibraryIcon from '@material-ui/icons/LocalLibraryRounded';
import HelpIcon from '@material-ui/icons/Help';
import PersonIcon from '@material-ui/icons/PersonRounded';
import ExitIcon from '@material-ui/icons/ExitToApp';
import WebIcon from '@material-ui/icons/Web';
import Link from 'next/link';
import Router from 'next/router'
import { logout as authLogout } from '../store/effects/auth'

export default class extends Component {

  isAllAccount = false

  constructor(props) {
    super(props)
    if (props.account.provider.id &&
      props.account.client.id)
      this.isAllAccount = true
  }

  logout = (e) => {
    e.preventDefault()
    authLogout()
    this.props.reset()
    Router.push('/')
  }

  render() {
    let { account } = this.props
    return (
      <div>
        {
          !this.props.user.id ?
            (
              <div>
                <Link href="/login">
                  <ListItem button>
                    <ListItemIcon>
                      <PersonIcon />
                    </ListItemIcon>
                    <ListItemText primary="LOGIN" />
                  </ListItem>
                </Link>
                <Link href="/signup">
                  <ListItem button>
                    <ListItemIcon>
                      <PersonAddIcon />
                    </ListItemIcon>
                    <ListItemText primary="SIGN UP" />
                  </ListItem>
                </Link>
              </div>
            ) :
            (
              <div>
                {account.provider.id && (
                  <Link href="/provider">
                    <ListItem button>
                      <ListItemIcon>
                        <WebIcon />
                      </ListItemIcon>
                      <ListItemText primary={`${this.isAllAccount ? 'PROVIDER' : 'DASHBOARD'}`} />
                    </ListItem>
                  </Link>
                )}
                {account.client.id && (
                  <Link href="/client">
                    <ListItem button>
                      <ListItemIcon>
                        <WebIcon />
                      </ListItemIcon>
                      <ListItemText primary={`${this.isAllAccount ? 'CLIENT' : 'DASHBOARD'}`} />
                    </ListItem>
                  </Link>
                )}
                <a onClick={this.logout}>
                  <ListItem button>
                    <ListItemIcon>
                      <ExitIcon />
                    </ListItemIcon>
                    <ListItemText primary="LOGOUT" />
                  </ListItem>
                </a>
              </div>
            )
        }
        <div className="bt b--light-gray mt3 pt3">
          <Link href="/about">
            <ListItem button>
              <ListItemIcon>
                <LibraryIcon />
              </ListItemIcon>
              <ListItemText primary="ABOUT US" />
            </ListItem>
          </Link>
          <Link href="/help">
            <ListItem button>
              <ListItemIcon>
                <HelpIcon />
              </ListItemIcon>
              <ListItemText primary="HELP" />
            </ListItem>
          </Link>
        </div>
      </div>
    )
  }
}