import { Component } from 'react'
import { connect } from 'react-redux'
import Link from 'next/link'
import moment from 'moment'
import Delete from '@material-ui/icons/Delete'
import Edit from '@material-ui/icons/Edit'
import Button from '@material-ui/core/Button'
import _find from 'lodash/find'
import AsyncComponent from '../../components/async'
import { loadClientOrders } from '../../store/effects/order'
import Tooltip from '@material-ui/core/Tooltip'

class OrdersContent extends Component {

  getService = (_id) => {
    return _find(this.props.service.list, { _id }) || {}
  }

  getColorClass = (status) => {
    switch (status) {
      case 'HOLD': return 'orange'
      case 'SEARCHING': return 'purple'
      case 'WAITING': return 'yellow'
      case 'WORKING': return 'green'
      case 'COMPLETED': return 'blue'
      case 'CANCELED': return 'dark-pink'
      case 'REJECTED': return 'red'
      default: return 'light-gray'
    }
  }

  render() {
    let { orders } = this.props
    return (
      <div>
        <ul className="list ma0 pa0">
          {orders.map((item, i) => {
            let service = this.getService(item.service)
            return (
              <li key={item._id}>
                <Link href={`/service/order?id=${item._id}`}>
                  <a className={`link flex justify-between items-center 
                  ph3 pv3 hide-child hover-bg-white ${++i < orders.length ? 'br0 bb b--moon-gray' : 'br3'}`}>
                    <div>
                      <h4 className="ma0 mb3 color-b1">{service.name} Service</h4>
                      <div className="gray f6 ma0">
                        {`${moment(item.datetime).format("ddd, MMM Do YYYY, h:mm A")}`}
                      </div>
                    </div>
                    <div className="nowrap f6">
                      <Tooltip placement="left" title="Status">
                        <span className={`bg-${this.getColorClass(item.status)} white dib br-pill pv1 ph3`}>
                          {`${item.status}`}
                        </span>
                      </Tooltip>
                    </div>
                  </a>
                </Link>
              </li>
            )
          })}
        </ul>
      </div>
    )
  }

}

const stateProps = ({ account, service }) => ({
  account,
  service
})

export default connect(stateProps)(
  AsyncComponent(
    OrdersContent,
    props => {
      return loadClientOrders()
        .then(orders => ({ orders: orders || {} }))
    }
  )
)