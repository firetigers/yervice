import { Component } from "react";
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import Slide from '@material-ui/core/Slide'
import Button from '@material-ui/core/Button'
import { isMobile } from '../../utils/helpers'

function Transition(props) {
  return <Slide direction="up" {...props} />
}

export default (Content, opt) => {
  return class extends Component {

    state = {
      open: false,
    }

    handleOpen = () => {
      this.setState({ open: true })
    }

    handleClose = () => {
      this.setState({ open: false })
    }

    render() {
      return (
        <div>
          <div onClick={this.handleOpen} >
            {this.props.children}
          </div>
          <Dialog
            fullScreen={isMobile ? true : !!opt.fullscreen}
            open={this.state.open}
            TransitionComponent={Transition}
            keepMounted
            onClose={this.handleClose}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description">
            <div className="pb2 bb b--near-white tc">
              <h2 className="mt4 dark-gray">{opt.title}</h2>
              <p className="mid-gray">{opt.description}</p>
            </div>
            <div
              className={`absolute right-1 bottom-1 bg-white br2 ba b--moon-gray`}>
              {
                !opt.hideClose &&
                <Button onClick={this.handleClose} color="primary">
                  {opt.close || 'CLOSE'}
                </Button>
              }
            </div>
            <DialogContent>
              <Content onClose={this.handleClose} />
            </DialogContent>
          </Dialog>
        </div>
      )
    }
  }
}