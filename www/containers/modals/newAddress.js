import { Component } from 'react'
import { connect } from 'react-redux'
import Link from 'next/link'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import _find from 'lodash/find'
import _pick from 'lodash/pick'
import baseDialog from './base'
import { addNewAddress } from '../../store/actions/user'
// import { loadCountryList } from '../../store/actions/ui'
import { loadCountryList } from '../../store/dispatchers/ui'

const init = {
  errors: [],
  line1: '',
  line2: '',
  country: '',
  region: '',
  locality: '',
}

class NewAddressModal extends Component {

  state = {
    ...init,
    loading: false,
    loaded: false
  }

  constructor(props) {
    super(props)
    this.loadData()
  }

  loadData = (e) => {
    if (!global.document) return
    if (!this.state.loading && !this.state.loaded) {
      this.state.loading = true
      loadCountryList(this.props.dispatch)()
        .then(() => this.setState({ loaded: true, loading: false }))
    }
  }

  changeState = (e) => {
    e.preventDefault()
    this.setState({ [e.target.name]: e.target.value })
  }

  clickCancel = (e) => {
    e && e.preventDefault()
    this.setState({ ...init })
    this.props.onClose()
  }

  submitForm = (e) => {
    e.preventDefault();
    this.props.dispatch(addNewAddress({
      ..._pick(this.state, [
        'line1', 'line2', 'country', 'region', 'locality'
      ])
    })).then(() => {
      this.clickCancel()
    }).catch(err => {
      if (err.response)
        this.setState({ errors: err.response.data.payload })
    })
  }

  render() {
    return (
      <div className="center mw6 pb4" autoComplete="off">
        {this.state.loaded && <form onSubmit={this.submitForm}>
          <TextField
            id="line1"
            name="line1"
            label="Line 1"
            value={this.state.line1}
            onChange={this.changeState}
            margin="normal"
            fullWidth={true}
            error={!!_find(this.state.errors, { param: 'line1' })}
          />
          <TextField
            id="line2"
            name="line2"
            label="Line 2"
            value={this.state.line2}
            onChange={this.changeState}
            margin="normal"
            fullWidth={true}
            error={!!_find(this.state.errors, { param: 'line2' })}
          />
          <TextField
            select
            id="country"
            name="country"
            label="Country"
            value={this.state.country}
            onChange={this.changeState}
            margin="normal"
            fullWidth={true}
            error={!!_find(this.state.errors, { param: 'country' })}
            SelectProps={{
              native: true
            }}>
            <option key='_' value='' disabled></option>
            {this.props.country.list.map(option => (
              <option key={option['ISO3166-1-Alpha-2']} value={option['ISO3166-1-Alpha-2']}>
                {option.name}
              </option>
            ))}
          </TextField>
          <TextField
            id="region"
            name="region"
            label="Region"
            value={this.state.region}
            onChange={this.changeState}
            margin="normal"
            fullWidth={true}
            error={!!_find(this.state.errors, { param: 'region' })}
          />
          <TextField
            id="locality"
            name="locality"
            label="Area"
            value={this.state.locality}
            onChange={this.changeState}
            margin="normal"
            fullWidth={true}
            error={!!_find(this.state.errors, { param: 'locality' })}
          />
          <br />
          <div className="tc mt4">
            <Button
              variant="contained"
              color="primary"
              type="submit">
              Add Address
            </Button>
            &nbsp;&nbsp;&nbsp;
            <Button
              variant="contained"
              onClick={this.clickCancel}>
              Cancel
            </Button>
          </div>
        </form>}
      </div>
    )
  }

}

const stateProps = ({ ui: { country } }) => ({
  country
})

export default baseDialog(
  connect(stateProps)(NewAddressModal),
  {
    title: 'Add New Address',
    hideClose: true
  }
)
