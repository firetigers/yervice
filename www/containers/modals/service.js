import { Component } from 'react'
import { connect } from 'react-redux'
import Link from 'next/link'
import baseDialog from './base'

class ServiceModal extends Component {

  render() {
    return (
      <div className="center" >
        <ul className="cf ma0 pa0 list tc pb5">
          {
            this.props.service.list.map((item, i) => {
              return (
                <li key={item._id} data-iid={item._id} className="dib w5 pa2">
                  <Link href={`/service/order/place?id=${item._id}`}>
                    <a className="dib link dark-gray bg-white shadow-4 pointer br2">
                      <img src={item.images.thumbnail} className="br2 br--top" />
                      <span className="dib tc f4 pt2 pb3">{item.name}</span>
                    </a>
                  </Link>
                </li>
              )
            })
          }
        </ul>
      </div>
    )
  }
  
}

const stateProps = ({ service }) => ({
  service
})

export default baseDialog(
  connect(stateProps)(ServiceModal),
  {
    title: 'Find The Service You Need',
    description: 'Place a job order by selecting a service',
    fullscreen: true
  }
)
