import { Component } from 'react'
import Delete from '@material-ui/icons/Delete'
import Edit from '@material-ui/icons/Edit'
import Button from '@material-ui/core/Button'
import { connect } from 'react-redux';
import NewAddressModal from '../modals/newAddress'
import { remAddress } from '../../store/actions/user'

class AddressContent extends Component {

  clickRemove = e => {
    e.preventDefault()
    this.props.dispatch(remAddress(e.target.closest('li').dataset.id))
  }

  render() {
    return (
      <div>
        <ul className="list pa0 ma0">
          {this.props.addresses.map(addr => {
            return (
              <li
                className="flex items-center ph3 pv3 bb b--moon-gray hide-child"
                data-id={addr._id}
                key={addr._id}>
                <div className="w-100">
                  <div className="mb2">{addr.line1}</div>
                  <div className="f6 gray">
                    {addr.line2},&nbsp;
                    {addr.locality},&nbsp;
                    {addr.region},&nbsp;
                    {addr.country}
                  </div>
                </div>
                <div className="nowrap child">
                  <Button mini onClick={this.clickRemove}>
                    <Delete color="primary" />
                  </Button>
                </div>
              </li>
            )
          })}
        </ul>
        <div className="pv4 tc">
          <NewAddressModal>
            <Button
              variant="contained"
              color="primary"
              size="small">
              <span className="nowrap ph3">Add New Address</span>
            </Button>
          </NewAddressModal>
        </div>
      </div>
    )
  }
}

const stateProps = ({ user: { addresses } }) => ({
  addresses
})

export default connect(stateProps)(AddressContent)