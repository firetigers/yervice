import { Component } from 'react'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import AddIcon from '@material-ui/icons/Add'
import VisibilityIcon from '@material-ui/icons/Visibility'
import Input from '@material-ui/core/Input'

// SAMPLE DATA ONLY >>>
let id = 0
function createData(name, calories, fat, carbs, protein) {
  id += 1
  return { id, name, calories, fat, carbs, protein }
}
const data = [
  createData('0', 'Cleaning', '', 6.0, 24, 4.0),
  createData('1', 'Home Cleaning', 'Cleaning', 9.0, 37, 4.3),
  createData('2', 'Deep Clening', 'Cleaning', 16.0, 24, 6.0),
  createData('3', 'Moving', '', 3.7, 67, 4.3),
  createData('4', 'Office Moving', 'Moving', 16.0, 49, 3.9),
]
// <<< SAMPLE DATA ONLY

export default class extends Component {

  render() {
    return (
      <div>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Parent</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map(n => {
              return (
                <TableRow key={n.id}>
                  <TableCell>
                    {n.name}
                  </TableCell>
                  <TableCell>
                    {n.calories}
                  </TableCell>
                  <TableCell>
                    {n.fat}
                  </TableCell>
                  <TableCell>
                    <IconButton> <VisibilityIcon /> </IconButton>
                    <IconButton> <EditIcon /> </IconButton>
                    <IconButton> <DeleteIcon /> </IconButton>
                  </TableCell>
                </TableRow>
              )
            })}
            <TableRow key="new">
              <TableCell>New Category:</TableCell>
              <TableCell>
                <Input
                  placeholder="Name"
                />
              </TableCell>
              <TableCell>
                <Input
                  placeholder="Parent Id"
                />
              </TableCell>
              <TableCell>
                <IconButton> <AddIcon /> </IconButton>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    )
  }

}