import { createMuiTheme } from '@material-ui/core/styles'

export default createMuiTheme({
  palette: {
    primary: {
      main: '#2f4858'
    },
    secondary: {
      main: '#FF4136'
    },
    error:{
      main: '#E7040F'
    }
  }
})