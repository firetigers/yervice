import { Component } from 'react'
import { connect } from 'react-redux'
import Router from 'next/router'
import Layout from '../../layouts/main'
import Head from 'next/head'
import Link from 'next/link'
import _find from 'lodash/find'
import Button from '@material-ui/core/Button'
import ServiceModal from '../../containers/modals/service'
import { removeOrder } from '../../store/actions/client'
import Tabs from '../../components/tabs'
import OrdersContent from '../../containers/client/orders'


class ClientPage extends Component {

  state = {
    savedOrderService: null
  }

  constructor(props) {
    super(props)
    if (global.document && !props.client.id)
      return Router.push('/client/activate')
    let service = _find(props.service.list, { _id: this.savedOrderId() })
    // console.log(this.savedOrderId(), props.service.list, service)
    // if (props.service.list.length && !service && this.savedOrderId()) {
    // Not matching, remove saved order
    // props.dispatch(removeOrder())
    // } else {
    // }
    this.state.savedOrderService = service
  }

  getSavedOrderServiceName = () => {
    return (this.state.savedOrderService &&
      this.state.savedOrderService.name) || ''
  }

  savedOrderId = () => {
    return this.props.client.order.serviceId
  }

  clickRemoveSaved = (e) => {
    if (e) e.preventDefault()
    this.props.dispatch(removeOrder())
  }

  render() {
    return (
      <Layout auth>
        <Head>
          <title>Client Dashboard</title>
        </Head>
        <h1 className="tc">Client Dashboard</h1>
        <section className="tc mw6-ns center mb5">
          {
            this.savedOrderId() && (
              <div className="ba b--black-30 pa3 br3">
                <div className="mv3">You have an incomplete job order of</div>
                <p className="b color-b2 mb4 f4">{this.getSavedOrderServiceName()} Service</p>
                <div>
                  <Link href={`/service/order/place?id=${this.savedOrderId()}`}>
                    <Button color="primary">Continue</Button>
                  </Link>
                  &nbsp;&nbsp;&nbsp;
                  <Button color="secondary"
                    onClick={this.clickRemoveSaved}>Cancel</Button>
                </div>
              </div>
            )
          }
          <div className="dib w5 center mt5 nowrap">
            <ServiceModal>
              <a className="db nowrap w-100 ph4 pv3 br-pill bg-color-text white bn f4 fw7 pointer">
                New Job Order
              </a>
            </ServiceModal>
          </div>
        </section>
        <section>
          <div className="ba b--moon-gray br3 mw7 center">
            <Tabs classNameTabs="bb b--moon-gray" default="0">
              <div name="Current Orders">
                <OrdersContent />
              </div>
              <div name="History" className="pa3">
                Order History
              </div>
            </Tabs>
          </div>
        </section>
      </Layout>
    )
  }
}

const stateProps = ({ user, service, account: { client } }) => ({
  user,
  client,
  service
})

export default connect(stateProps)(ClientPage)