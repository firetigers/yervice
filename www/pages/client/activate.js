import { Component } from 'react'
import { connect } from 'react-redux'
import Router from 'next/router'
import Head from 'next/head'
import Layout from '../../layouts/main'
import { setClient } from '../../store/actions/account'
import { activate } from '../../store/effects/account'
import type from '../../store/types'
import { toastError } from '../../utils/toaster'

class ProviderPage extends Component {

  constructor(props) {
    super(props)
    if (global.document && props.client.id) Router.push('/client')
  }

  clickActivate = (e) => {
    e.preventDefault()
    activate(type.CLIENT)
      .then(client => this.props.setClient(client))
      .then(() => Router.push('/client'))
      .catch(err => toastError('Error, please try again later'))
  }

  render() {
    return (
      <Layout auth>
        <Head>
          <title>Provider Activation</title>
        </Head>
        <section className="tc mw6 center">
          <h1>Client Activation</h1>
          <p className="fw6">Do you need services from Service Providers?</p>
          <p>If so, please click the button below.</p>
          <br />
          <a
            className="pv3 ph5 br3 dib bg-color-b2 white grow no-underline pointer"
            onClick={this.clickActivate}  >
            <h2>Activate Now</h2>
          </a>
          <br />
          <div className="mw6 center ph5 mt5">
            <p className="lh-copy gray f6">
              <b className="f5">Note:</b>
              <br />
              You have been redirected here because you are trying to access features that are only for Clients
          </p>
          </div>
        </section>
      </Layout>
    )
  }

}

const stateProps = ({ user, account: { client } }) => ({
  user,
  client
})

const dispatchProps = dispatch => ({
  setClient: client => dispatch(setClient(client))
})

export default connect(stateProps, dispatchProps)(ProviderPage)