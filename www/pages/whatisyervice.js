import { Component } from 'react'
import Layout from '../layouts/main'
import Head from 'next/head'
import Link from 'next/link'

export default class extends Component {
  render() {
    return (
      <Layout>
        <Head>
          <title>What is Yervice?</title>
        </Head>
        
        <div className="fl w-100 dt">
            <div className="w-100 bg-white pv3 ph2 tc vh-75 dtc v-mid hero">
                <h1 className="white f2 text-shadow-1">Trusted Quality Service On-the-Go</h1>
                <h4 className="white f5 text-shadow-1">Simple, fast and quality personalized services at the touch of a button</h4>
                <Link href="/howitworks">
                <button className="w-80 w5-ns mb6 mb6-ns ph3 pv3 br-pill bg-color-a2 white bn f4 fw7 pointer mt4">HOW IT WORKS</button>
                </Link>
            </div>
        </div>

        <div className="fl w-100 dt">
            <div className="w-100 bg-white pv3 ph2 tc dtc v-mid">
                
            <div className="mw9 center ph3-ns">
              <div className="cf ph2-ns">
                <div className="fl w-100 w-third-ns pa4">
                  <div className="shadow-3 br4 bg-white pv4 tc">
                    <img src="https://image.flaticon.com/icons/svg/1039/1039344.svg" className="br-100 h3 w3 dib" title="Photo of a kitty staring at you" />
                    <h2 className="f4">Open Community</h2>
                    <hr className="mw3 bb bw1 b--black-10"/>
                    <p className="lh-copy measure center f6 black-70 tl tc pa3">
                      Everyone is always welcome to provide service and acquire service.
                    </p>
                  </div>
                </div>
                <div className="fl w-100 w-third-ns pa4">
                  <div className="shadow-3 br4 bg-white pv4 tc">
                    <img src="https://image.flaticon.com/icons/svg/1039/1039340.svg" className="br-100 h3 w3 dib" title="Photo of a kitty staring at you" />
                    <h2 className="f4">Fast &amp; Easy</h2>
                    <hr className="mw3 bb bw1 b--black-10"/>
                    <p className="lh-copy measure center f6 black-70 tl tc pa3">
                      Quickly acquire service from millions of people waiting for your click.
                    </p>
                  </div>
                </div>
                <div className="fl w-100 w-third-ns pa4">
                  <div className="shadow-3 br4 bg-white pv4 tc">
                  <img src="https://image.flaticon.com/icons/svg/1039/1039349.svg" className="br-100 h3 w3 dib" title="Photo of a kitty staring at you" />
                    <h2 className="f4">Quality</h2>
                    <hr className="mw3 bb bw1 b--black-10"/>
                    <p className="lh-copy measure center f6 black-70 tl tc pa3">
                      Provides recommendation and rating based system for clients.
                    </p>
                  </div>
                </div>
              </div>
            </div>

            </div>
        </div>

        <div className="fl w-100 dt">
            <div className="w-100 bg-white pv3 ph5-ns ph3 tc dtc v-mid bg-color-a1">
                <h1 className="white f2 text-shadow-1">The Project</h1>
                <h4 className="white f4 text-shadow-1">In a fast paced world integrated with technology <span className="gold b f3">Yervice</span> has created the perfect hybrid combination needed in today’s services industry. Combining personalized services that you pick, in addition the ability to choose from various services providers based customized profiles and user reviews.</h4>
                <h4 className="white f4 text-shadow-1">Quality is a high priority for us at <span className="gold b f3">Yervice</span> and guaranteeing the work being done for all our users. More importantly with our emergency services we are able to help in the most sensitive times and deliver when you need it most.</h4>
                
            </div>
        </div>

      </Layout>
    )
  }
}