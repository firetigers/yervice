import { Component } from 'react'
import AdminLayout from '../../layouts/inside'
import Tabs from '../../components/tabs'
import DashboardTab from '../../containers/inside/tabs/dashboard'
import CategoriesTab from '../../containers/inside/tabs/categories'
import ServicesTab from '../../containers/inside/tabs/services'
import UsersTab from '../../containers/inside/tabs/users'

export default class extends Component {

  // If `getInitialProps` exists,
  // It will be executed on the server after Express route handlers
  static async getInitialProps({ req, res }) {
    const userAgent = req ? req.headers['user-agent'] : navigator.userAgent
    return { userAgent }
  }

  render() {
    return (
      <AdminLayout>
        <Tabs default="1">
          <DashboardTab title="Dashboard" />
          <CategoriesTab title="Categories" />
          <ServicesTab title="Services" />
          <UsersTab title="Users" />
        </Tabs>
      </AdminLayout>
    )
  }
}