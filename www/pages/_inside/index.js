import { Component } from 'react'
import Button from '@material-ui/core/Button';

export default class extends Component {

  static async getInitialProps({ req, res }) {
    const userAgent = req ? req.headers['user-agent'] : navigator.userAgent
    return { userAgent }
  }

  render() {
    return (
      <div className="tc">
        <span className="dib mt4">
          <Button variant="contained" color="primary">
            Log In
          </Button>
        </span>
      </div>
    )
  }
}