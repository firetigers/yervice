'use strict'

import { Component } from 'react'
import Layout from '../../layouts/main'
import Head from 'next/head'
import axios from 'axios'
import Router from 'next/router'

export default class extends Component {

  state = {}

  static async getInitialProps({ query }) {
    return { query }
  }

  clickActivate = (e) => {
    e.preventDefault()
    this.setState({ disabled: true })
    axios
      .get(`/api/activation?user=${this.props.query.id}&token=${this.props.query.tk}`)
      .then(() => Router.push('/login'))
  }

  render() {
    return (
      <Layout path={this.props.path}>
        <Head>
          <title>Activate User</title>
        </Head>
        <h1>Todo: Activate User</h1>
        <ul>
          <li>Send activation link to email</li>
        </ul>
        <hr />
        <h3>For Demo Only</h3>
        <p>Click the following button to activate now.</p>
        <p>
          <button onClick={this.clickActivate} disabled={this.state.disabled}>
            Activate Now
          </button>
        </p>
      </Layout>
    )
  }
}