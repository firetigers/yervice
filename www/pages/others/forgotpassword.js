import { Component } from 'react'
import Layout from '../../layouts/main'
import Head from 'next/head'

export default class extends Component {
  render() {
    return (
      <Layout>
        <Head>
          <title>Forgot Password</title>
        </Head>
        <h1>Todo: Forgot Password</h1>
        <ul>
          <li>Require user email</li>
          <li>Send a link to reset the password</li>
          <li>The page should have security token</li>
        </ul>
      </Layout>
    )
  }
}