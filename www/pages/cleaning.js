import Head from 'next/head'
import Link from 'next/link'
import Layout from '../layouts/main'

export default () => {

  return (
    <Layout>
      <Head>
        <title>Book Cleaning Service</title>
      </Head>
        
        <div className="fl w-100 bg-gold">
            <div className="bg-white pv4 ph4 bg-gold">
                <div><span>Book a cleaner online from AED 35 per hour*</span></div>
                <small>Describe what you need in the form below and we will find you a professional cleaner right away.</small>
            </div>
        </div>

        <div className="mw9 center ph3-ns">
            <div className="cf ph2-ns">
                <div className="fl w-100 w-70-ns pa2">
                    <div className="pv4">

                        <div className="w-100 pa2">
                            <div className="pv2">
                                <div className="ml4 mb3">How often do you need cleaning?</div>
                                
                                <div className="mw9 center ph3-ns">
                                    <div className="cf ph2-ns">
                                        <div className="fl w-50 w-25-ns pa2">
                                            <div className="ba b--black-20 br4 bg-white pv4 tc f6 pointer bg-color-a1 white">
                                                <div>Every Week</div>
                                                <small>Same cleaner Everytime</small>
                                            </div>
                                        </div>
                                        <div className="fl w-50 w-25-ns pa2">
                                            <div className="ba b--black-20 br4 bg-white pv4 tc f6 pointer">
                                                <div>Every 2 Week</div>
                                                <small>Same cleaner Everytime</small>
                                            </div>
                                        </div>
                                        <div className="fl w-50 w-25-ns pa2">
                                            <div className="ba b--black-20 br4 bg-white pv4 tc f6 pointer">
                                                <div>Several Times a Week</div>
                                                <small>Same cleaner Everytime</small>
                                            </div>
                                        </div>
                                        <div className="fl w-50 w-25-ns pa2">
                                            <div className="ba b--black-20 br4 bg-white pv4 tc f6 pointer">
                                                <div>One time only</div>
                                                <small>Same cleaner Everytime</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="w-100 pa2">
                            <div className="pv2">
                                <div className="ml4 mb3">How many cleaners do you need?</div>
                                
                                <div className="mw9 center ph3-ns">
                                    <div className="cf ph2-ns">
                                        <div className="fl w-50 w-25-ns pa2">
                                            <div className="ba b--black-20 br4 bg-white pv4 tc f6 pointer bg-color-a1 white">
                                                <div>1</div>
                                                <div>Cleaner</div>
                                            </div>
                                        </div>
                                        <div className="fl w-50 w-25-ns pa2">
                                            <div className="ba b--black-20 br4 bg-white pv4 tc f6 pointer">
                                                <div>2</div>
                                                <div>Cleaner</div>
                                            </div>
                                        </div>
                                        <div className="fl w-50 w-25-ns pa2">
                                            <div className="ba b--black-20 br4 bg-white pv4 tc f6 pointer">
                                                <div>3</div>
                                                <div>Cleaner</div>
                                            </div>
                                        </div>
                                        <div className="fl w-50 w-25-ns pa2">
                                            <div className="ba b--black-20 br4 bg-white pv4 tc f6 pointer">
                                                <div>4</div>
                                                <div>Cleaner</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="w-100 pa2">
                            <div className="pv2">
                                <div className="ml4 mb3">How long should they stay?</div>
                                
                                <div className="mw9 center ph3-ns">
                                    <div className="cf ph2-ns">
                                        <div className="fl w-50 w-25-ns pa2">
                                            <div className="ba b--black-20 br4 bg-white pv4 tc f6 pointer bg-color-a1 white">
                                                <div>2 Hours</div>
                                                <div>AED 50 / Hour</div>
                                            </div>
                                        </div>
                                        <div className="fl w-50 w-25-ns pa2">
                                            <div className="ba b--black-20 br4 bg-white pv4 tc f6 pointer">
                                                <div>3 Hours</div>
                                                <div>AED 40 / Hour</div>
                                            </div>
                                        </div>
                                        <div className="fl w-50 w-25-ns pa2">
                                            <div className="ba b--black-20 br4 bg-white pv4 tc f6 pointer">
                                                <div>4 Hours</div>
                                                <div>AED 35 / Hour</div>
                                            </div>
                                        </div>
                                        <div className="fl w-50 w-25-ns pa2">
                                            <div className="ba b--black-20 br4 bg-white pv4 tc f6 pointer">
                                                <div>5 Hours +</div>
                                                <div>AED 35 / Hour</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div className="fl w-100 w-30-ns pa2 dn db-ns">
                    <div className="pv4">
                        <div className="fl w-100 pa2">
                            <div className="ba b--gold br3 bg-white pv2 mt3 tc f5 b gray pointer">
                                <div>Booking Summary</div>
                                <hr className="b--gold"/>
                                <div className="w-100 pa2 f7">
                                    <div className="dib w-60 tl">Number of cleaners</div><div className="dib w-40 tr">1</div>
                                    <hr/><br/>
                                    <div className="dib w-60 tl">Duration</div><div className="dib w-40 tr">4 Hours</div>
                                    <hr/><br/>
                                    <div className="dib w-60 tl">Frequency</div><div className="dib w-40 tr">Every Week</div>
                                    <hr/><br/>
                                    <div className="dib w-60 tl">Date</div><div className="dib w-40 tr">18/08/2018</div>
                                    <hr/><br/>
                                    <div className="dib w-60 tl">Week Day</div><div className="dib w-40 tr">Friday</div>
                                    <hr/><br/>
                                    <div className="dib w-60 tl">Prefered Time</div><div className="dib w-40 tr">10 am</div>
                                    <hr/><br/>
                                    <div className="dib w-60 tl">Hourly Rate/cleaner</div><div className="dib w-40 tr">AED35</div>
                                    <hr/><br/>
                                    <div className="dib w-60 tl b black">TOTAL</div><div className="dib w-40 tr black b">AED35</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
    </Layout>
  )

}
