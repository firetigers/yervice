import React from 'react'
import App, { Container } from 'next/app'
import { MuiThemeProvider } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import JssProvider from 'react-jss/lib/JssProvider'
import getPageContext from '../utils/pageContext'
import Router from 'next/router'
import NProgress from 'nprogress'
import storeWithRedux from '../store'
import initClient from '../init/client'
import initServerProps from '../init/serverProps'

Router.onRouteChangeStart = () => NProgress.start()
Router.onRouteChangeComplete = () => NProgress.done()
Router.onRouteChangeError = () => NProgress.done()

class MyApp extends App {

  pageContext = null

  constructor(props) {
    super(props)
    this.pageContext = getPageContext()
    if (global.document) initClient(props)
  }

  static async getInitialProps(cntx) {
    let prom = super.getInitialProps(cntx)
    if (!global.document) {
      prom = prom.then(data => {
        return initServerProps(cntx.req, cntx.res)
          .then(props => {
            return {
              pageProps: {
                ...data.pageProps,
                ...props
              }
            }
          })
      })
    }
    return prom
  }

  componentDidMount() {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side')
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles)
    }
  }

  render() {
    const { Component, pageProps } = this.props
    return (
      <Container>
        {/* Wrap every page in Jss and Theme providers */}
        <JssProvider
          registry={this.pageContext.sheetsRegistry}
          generateClassName={this.pageContext.generateClassName}
        >
          {/* MuiThemeProvider makes the theme available down the React
              tree thanks to React context. */}
          <MuiThemeProvider
            theme={this.pageContext.theme}
            sheetsManager={this.pageContext.sheetsManager}
          >
            {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
            <CssBaseline />
            {/* Pass pageContext to the _document though the renderPage enhancer
                to render collected styles on server side. */}
            <Component
              {...pageProps}
              pageContext={this.pageContext}
            />
          </MuiThemeProvider>
        </JssProvider>
      </Container>
    )
  }
}

export default storeWithRedux(MyApp)
