import { Component } from 'react'
import { connect } from 'react-redux'
import Head from 'next/head'
import Router from 'next/router'
import _find from 'lodash/find'
import _range from 'lodash/range'
import _omit from 'lodash/omit'
import _defaults from 'lodash/defaults'
import moment from 'moment'
import TextField from '@material-ui/core/TextField'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import Layout from '../../../layouts/main'
import Calculator from '../../../components/calculator'
import NewAddressModal from '../../../containers/modals/newAddress'
import { setOrder as setClientOrderAction, removeOrder } from '../../../store/actions/client'
import { placeClientOrder } from '../../../store/effects/order'

class PlaceOrderPage extends Component {

  classAttr = 'mb4'
  classLabel = 'fw6'

  state = {
    service: null,
    attributes: null
  }

  constructor(props) {
    super(props)
    let query = props.query || {}
    this.state.service = _find(props.service.list, { _id: query.id }) || {}
    // Check previous order
    let prevOder = _omit(props.client.order, ['serviceId'])
    this.state.attributes = _defaults(prevOder, {
      numProviders: 1,
      numHours: 1,
      numItems: 1,
      location: '',
      date: '2018-08-30',
      time: '00:00'
    })
    if (global.document)
      if (props.user.addresses[0])
        this.state.attributes.location = props.user.addresses[0]._id
  }

  static async getInitialProps({ query }) {
    return { query }
  }

  isAuth = () => !!this.props.user.id
  isClient = () => !!this.props.client.id
  isPerHour = () => this.state.service.pricing == 'HOUR'
  isPerJob = () => this.state.service.pricing == 'JOB'
  isPerItem = () => this.state.service.pricing == 'ITEM'

  getLocationName = (id) => {
    let l = _find(this.props.user.addresses, { _id: id })
    return (l && `${l.line1}, ${l.line2}, ${l.locality}, ${l.region}, ${l.country}`) || ''
  }

  changeAttribute = (e) => {
    e.preventDefault()
    this.setState({
      attributes: {
        ...this.state.attributes,
        [e.target.name]: e.target.value
      }
    })
  }

  submitForm = (e) => {
    e.preventDefault()
    let { attributes: attrs } = this.state
    if (this.isClient()) {
      // Immediately place order
      placeClientOrder({
        service: this.state.service._id,
        ...attrs,
        location: this.getLocationName(attrs.location)
      }).then(res => {
        this.props.dispatch(removeOrder())
        Router.push('/service/order?id=' + res._id)
      })
    } if (this.isAuth()) {
      Router.push('/client/activate')
    } else {
      // Let user signup first but save the order locally
      this.props.dispatch(setClientOrderAction({
        serviceId: this.state.service._id,
        ...attrs
      }))
      Router.push('/signup?as=client')
    }
  }

  viewPricing = (service, attributes) => {
    switch (service.pricing) {
      case 'HOUR':
        return (
          <div className={this.classAttr}>
            <label htmlFor="select-num-hours" className={this.classLabel}>
              How long should they work?
            </label>
            <TextField
              id="select-num-hours"
              select
              name="numHours"
              fullWidth={true}
              value={attributes.numHours}
              onChange={this.changeAttribute}
              margin="normal">
              {_range(1, 5).map((item, i) => (
                <MenuItem key={item} value={item}>
                  {item} Hour{i != 0 && 's'}
                </MenuItem>
              ))}
            </TextField>
          </div>
        )
      case 'ITEM':
        return (
          <div className={this.classAttr}>
            <label htmlFor="select-num-items" className={this.classLabel}>
              How many items should they work on?
            </label>
            <TextField
              id="select-num-items"
              select
              name="numItems"
              fullWidth={true}
              value={attributes.numItems}
              onChange={this.changeAttribute}
              margin="normal">
              {_range(1, 4).map((item, i) => (
                <MenuItem key={item} value={item}>
                  {item} Item{i != 0 && 's'}
                </MenuItem>
              ))}
            </TextField>
          </div>
        )
    }
    return null
  }

  viewLocation = () => {
    return (
      this.isAuth() &&
      <div className={this.classAttr}>
        <label htmlFor="select-location">
          <span className={`${this.classLabel} dib mb2`}>
            Set location
          </span>
          <p className="f6 gray ma0">The location you would like the Servicer Providers to go to</p>
        </label>
        <div className="flex flex-column flex-row-l items-center">
          <TextField
            id="select-location"
            select
            name="location"
            fullWidth={true}
            value={this.state.attributes.location || ''}
            onChange={this.changeAttribute}
            margin="normal">
            {this.props.user.addresses.map(addr => {
              return (
                <MenuItem key={addr._id} value={addr._id}>
                  {this.getLocationName(addr._id)}
                </MenuItem>
              )
            })}
          </TextField>
          <div className="mnw4 tr">
            <NewAddressModal>
              <Button variant="contained" color="primary" size="small">
                <span className="nowrap ">New Address</span>
              </Button>
            </NewAddressModal>
          </div>
        </div>
      </div>
    )
  }

  viewDateTime = () => {
    return (
      this.isAuth() &&
      <div className={this.classAttr}>
        <label htmlFor="select-date">
          <span className="dib mb2" className={this.classLabel}>
            Set date and time
          </span>
        </label>
        <div className="flex flex-column flex-row-l items-center">
          <TextField
            id="select-date"
            name="date"
            type="date"
            // fullWidth={true}
            value={this.state.attributes.date}
            onChange={this.changeAttribute}
            margin="normal" />
          &nbsp;&nbsp;&nbsp;
          <TextField
            id="select-time"
            name="time"
            type="time"
            // fullWidth={true}
            value={this.state.attributes.time}
            onChange={this.changeAttribute}
            margin="normal" />
        </div>
      </div>
    )
  }

  render() {
    let { service, attributes } = this.state
    return (
      <Layout>
        <Head>
          <title>Book Cleaning Service</title>
        </Head>
        <section className="tc mb3 mb5-l">
          <div className="title pb3 mb3">
            <h1>Job Order</h1>
            <div className="f4 b color-a2">
              <span>{service.name} Service</span>
            </div>
          </div>
        </section>
        <form onSubmit={this.submitForm}>
          <section className="mw6 mw8-l center flex flex-column flex-row-l mb5 mb4-l">
            <main className="w-100 pr4-l ba b--silver br3 pa3 pt4 mr3 bg-white mb4">
              {
                !service.solo && (
                  <div className={this.classAttr}>
                    <label htmlFor="select-num-provs" className={this.classLabel}>
                      How many service providers do you need?
                    </label>
                    <TextField
                      id="select-num-provs"
                      select
                      name="numProviders"
                      fullWidth={true}
                      value={attributes.numProviders}
                      onChange={this.changeAttribute}
                      margin="normal">
                      {_range(1, 11).map((item, i) => (
                        <MenuItem key={item} value={item}>
                          {item} Service Provider{i != 0 && 's'}
                        </MenuItem>
                      ))}
                    </TextField>
                  </div>
                )
              }
              {this.viewPricing(service, attributes)}
              {this.viewLocation()}
              {this.viewDateTime()}
            </main>
            <aside className="w-100 w-40-l">
              <Calculator
                service={service}
                order={attributes} />
            </aside>
          </section>
          <section className="tc">
            <button
              type="submit"
              className="ph4 pv2 mt4 br-pill bn shadow-4 f4 white bg-color-primary-light pointer nowrap">
              <span className="dib mb1">
                {
                  this.isAuth() ?
                    ('Place Job Order') :
                    ('Save and Signup')
                }
              </span>
            </button>
          </section>
        </form>
      </Layout>
    )
  }

}

const stateProps = ({ service, user, account: { client } }) => ({
  service,
  user,
  client
})

export default connect(stateProps)(PlaceOrderPage)