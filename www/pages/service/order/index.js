import { Component } from "react"
import { connect } from "react-redux"
import Head from 'next/head'
import Layout from '../../../layouts/main'

class OrderPage extends Component {

  static async getInitialProps({ query }) {
    return { query }
  }

  render() {
    return (
      <Layout>
        <Head>
          <title>Job Order Details</title>
        </Head>
        <h1>Todo: Display Job Order Details</h1>
        <ul>
          <li>Create content for Order ID: {this.props.query.id}</li>
        </ul>
      </Layout>
    )
  }

}

export default connect()(OrderPage) 