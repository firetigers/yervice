import { Component } from 'react'
import { connect } from 'react-redux'
import Router from 'next/router'
import Head from 'next/head'
import Switch from '@material-ui/core/Switch'
import EditIcon from '@material-ui/icons/Edit'
import DoneIcon from '@material-ui/icons/Done'
import IconButton from '@material-ui/core/IconButton'
import Input from '@material-ui/core/Input'
import Tooltip from '@material-ui/core/Tooltip'
import _find from 'lodash/find'
import _findIndex from 'lodash/findIndex'
import _remove from 'lodash/remove'
import _isEmpty from 'lodash/isEmpty'
import numbro from 'numbro'
import Layout from '../../layouts/main'
import { loadServices, addService, remService } from '../../store/dispatchers/provider'
import { setServicePrice } from '../../store/actions/provider'


class ProviderPage extends Component {

  state = {
    init: false,
    editingId: null
  }

  constructor(props) {
    super(props)
    if (global.document && !props.account.provider.id) Router.push('/provider/activate')
  }

  loadData = () => {
    if (global.document && this.props.account.provider.id) {
      if (!this.state.init && this.props.ui.ready) {
        this.setState({ init: true })
        this.props.loadProviderServices()
          .then(() => this.setState())
      }
    }
  }

  componentDidMount() {
    this.loadData()
  }

  componentDidUpdate() {
    this.loadData()
  }

  isMyService = (id) => {
    return _findIndex(this.props.provider.services,
      (item) => item.service._id === id) > -1 ? true : false
  }

  getPricingTypeName = (service) => {
    switch (service.pricing) {
      case 'HOUR': return 'Hour'
      case 'JOB': return 'Job'
      case 'ITEM': return 'Item'
    }
  }

  changeService = (e, newValue) => {
    e.preventDefault()
    let id = e.target.getAttribute('iid')
    if (!id) return
    if (newValue) {
      // Add service
      if (!this.isMyService(id)) {
        let service = _find(this.props.service.list, { _id: id })
        this.props.addProvService(id, service.price)
      }
    } else {
      // Remove service
      this.props.remProvService(id)
    }
  }

  clickEdit = (e) => {
    e.preventDefault()
    let elLi = e.target.closest('li'),
      id = elLi.dataset.iid,
      sameId = this.state.editingId == id
    this.setState({ editingId: sameId ? null : id })
    if (sameId) {
      // Done, save to server
      let value = elLi.find('input').value
      if (_isEmpty(value)) return
      this.props.dispatch(setServicePrice(id, value));
    }
  }

  render() {
    return (
      <Layout auth>
        <Head>
          <title>Provider Dashboard</title>
        </Head>
        <h1 className="tc">Provider Dashboard</h1>
        <br />
        <section className="mw6 mw7-l center">
          <h3>My Services</h3>
          <ul className="list pl0 ml0 center ba b--light-silver br2">
            {
              _isEmpty(this.props.provider.services) ?
                (
                  <li className="pt3 pb4 ph2 tc gray">
                    <h2 className="mb2">No Services</h2>
                    <span className="lh-copy f5 fw6 dib mb4">Subscribe to services below and earn money .</span>
                    <br />
                    <span className="lh-copy f6 dib">Or, submit your service if not listed.</span>
                  </li>
                ) :
                (
                  this.props.provider.services.map(item => {
                    return (
                      <li key={item._id}
                        data-iid={item._id}
                        className="ph3 pv3 bb b--light-silver flex items-center hover-bg-white">
                        <div className="dib w-100">
                          <span className="dib b lh-title mb2">
                            {item.service.name}
                          </span>
                          <br />
                          <div className="dib v-mid mr2">
                            {this.state.editingId == item._id ?
                              <Input placeholder="Set new price" onBlur={this.clickEdit} /> :
                              <span>AED {numbro(item.price).format()} per {this.getPricingTypeName(item.service)}</span>
                            }
                          </div>
                        </div>
                        <div className="dib nowrap">
                          <Tooltip
                            placement="left"
                            title={this.state.editingId ? 'Done' : 'Edit'}>
                            <IconButton onClick={this.clickEdit}>
                              {this.state.editingId == item._id ? <DoneIcon /> : <EditIcon />}
                            </IconButton>
                          </Tooltip>
                          <Switch
                            inputProps={{ iid: item._id }}
                            onChange={this.changeService}
                            checked={true} />
                        </div>
                      </li>
                    )
                  })
                )
            }
          </ul>
          <div className="h1"></div>
          <h3>Available Services</h3>
          <span className="lh-copy f6 silver">
            Prices are shown in default base prices. You can set your own prices after you subscribe.
          </span>
          <ul className="list pl0 ml0 center ba b--light-silver br2">
            {
              this.props.service.list.map(item => {
                if (this.isMyService(item._id)) return null
                return (
                  <li key={item._id}
                    className="ph3 pv3 bb b--light-silver flex items-center hover-bg-white">
                    <div className="dib w-100">
                      <span className="dib b lh-title mb2">
                        {item.name}
                      </span>
                      <br />
                      <span className="v-mid">
                        AED {numbro(item.price).format()} per {this.getPricingTypeName(item)}
                      </span>
                    </div>
                    <div className="dib nowrap">
                      <Tooltip
                        placement="left"
                        title="Subscribe">
                        <Switch
                          inputProps={{ iid: item._id }}
                          onChange={this.changeService}
                          checked={false} />
                      </Tooltip>
                    </div>
                  </li>
                )
              })
            }
          </ul>
        </section>
      </Layout>
    )
  }

}

const stateProps = ({ user, account, provider, service, ui }) => ({
  user,
  account,
  provider,
  service,
  ui
})

const dispatchProps = dispatch => ({
  dispatch,
  loadProviderServices: loadServices(dispatch),
  addProvService: addService(dispatch),
  remProvService: remService(dispatch)
})

export default connect(stateProps, dispatchProps)(ProviderPage)