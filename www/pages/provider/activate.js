import { Component } from 'react'
import { connect } from 'react-redux'
import Router from 'next/router'
import Head from 'next/head'
import Layout from '../../layouts/main'
import { setProvider } from '../../store/actions/account'
import { activate } from '../../store/effects/account'
import type from '../../store/types'
import { toastError } from '../../utils/toaster'

class ProviderPage extends Component {

  constructor(props) {
    super(props)
    if (global.document && props.provider.id) Router.push('/provider')
  }

  clickActivate = (e) => {
    e.preventDefault()
    activate(type.PROVIDER)
      .then(provider => this.props.setProvider(provider))
      .then(() => Router.push('/provider'))
      .catch(err => toastError('Error, please try again later'))
  }

  render() {
    return (
      <Layout auth>
        <Head>
          <title>Provider Activation</title>
        </Head>
        <section className="tc mw6 center">
          <h1>Service Provider Activation</h1>
          <p className="fw6">Do you want to be a Service Provider?</p>
          <p>If so, please click the button below.</p>
          <br />
          <a
            className="pv3 ph5 br3 dib bg-color-a2 white grow no-underline pointer"
            onClick={this.clickActivate}  >
            <h2>Activate Now</h2>
          </a>
          <br />
          <div className="mw6 center ph5 mt5">
            <p className="lh-copy gray f6">
              <b className="f5">Note:</b>
              <br />
              You have been redirected here because you are trying to access features that are only for Service Providers
          </p>
          </div>
        </section>
      </Layout>
    )
  }

}

const stateProps = ({ user, account: { provider } }) => ({
  user,
  provider
})

const dispatchProps = dispatch => ({
  setProvider: provider => dispatch(setProvider(provider))
})

export default connect(stateProps, dispatchProps)(ProviderPage)