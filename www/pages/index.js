import { Component } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import Layout from '../layouts/main'
import ServiceModal from '../containers/modals/service'

export default class extends Component {
  render() {
    return (
      <Layout logo="howitworks" >
        <Head>
          <title>Home</title>
        </Head>
        <main className="w-100">
          <div className="mw6 center tc">
            <img className="mt4 mw5 mb5" src="/static/images/logos/logo-dark-color.svg" />
            <div className="mb3 mb4-l f4 fw6">Find your service</div>
            <div className="mb5 mb6-l mw6 ph5-ns">
              <ServiceModal>
                <a className="db nowrap w-100 ph4 pv3 br-pill bg-color-text white bn f4 fw7 pointer">
                  Select Service
              </a>
              </ServiceModal>
            </div>
            <div className="tc pt4">
              <div className="mw4 center mb4">
                <span className="ph3 bg-almost-white relative top-1 z-1 gray f6 fw6">OR</span><hr />
              </div>
              <Link href="/signup?as=provider">
                <a className="dib pv2 ph4 link ph4 br-pill bn shadow-4 f5 white bg-color-a1">Be a Service Provider</a>
              </Link>
            </div>
          </div>
        </main>
      </Layout>
    )
  }

}
