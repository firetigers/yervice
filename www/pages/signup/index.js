import { Component } from 'react'
import { connect } from 'react-redux'
import Router from 'next/router'
import Head from 'next/head'
import _padStart from 'lodash/padStart'
import _pick from 'lodash/pick'
import Layout from '../../layouts/main'
import Link from 'next/link'
import Progress from '../../components/progress'
import { register } from '../../store/effects/user'
import { getDaysOfMonth, getMonthList, getMonthWord } from '../../utils/dateList'
import { loadCountryList } from '../../store/dispatchers/ui'

class SignUp extends Component {

  classStyle = 'gray w-100 pa2 ph4 mv2 br-pill shadow-4 b f4 bg-light-gray'

  state = {
    init: false,
    now_year: (new Date).getFullYear(),
    gender: '',
    bd_month: '',
    bd_day: '',
    bd_year: '',
    addr_country: ''
  }

  constructor(props) {
    super(props)
    if (global.document) {
      if (props.user.id) {
        // User exist but wants to signup as either provide or client.
        // Activate account type instead of signing up a new user
        if (props.query.as === 'provider') {
          Router.push('/provider' + (props.account.provider.id ? '' : '/activate'))
        } else if (props.query.as === 'client' && !props.account.client.id) {
          Router.push('/client' + (props.account.provider.id ? '' : '/activate'))
        } else {
          Router.push('/dashboard')
        }
        return
      }
      // Provider the dispatch function to our dispatcher
      this.dispatchLoadCountyList = loadCountryList(this.props.dispatch)
    }
  }

  static async getInitialProps({ query, res }) {
    if (!query.as) {
      if (res) res.redirect('/signup/select');
      else Router.push('/signup/select')
    }
    return { query }
  }

  componentDidMount() {
    this.loadData()
  }

  componentDidUpdate() {
    this.loadData()
  }

  loadData = () => {
    if (!this.state.init && this.props.ui.ready) {
      this.setState({ init: true })
      // May not exist, if constructor creates a redirect
      if (this.dispatchLoadCountyList)
        this.dispatchLoadCountyList()
    }
  }

  changeState = (e) => {
    e.preventDefault()
    this.setState({ [e.target.name]: e.target.value })
  }

  changeStateAddress = (e) => {
    e.preventDefault()
    this.setState({
      address: {
        ...this.state.address,
        [e.target.name]: e.target.value
      }
    })
  }

  submitForm = (e) => {
    e.preventDefault()
    if (this.state.working) return
    this.setState({ working: true, err: null })
    let { bd_day, bd_month, bd_year } = this.state
    register(
      {
        ..._pick(this.state, [
          'email', 'password', 'firstname', 'lastname', 'gender',
          'addr_line1', 'addr_line2', 'addr_country', 'addr_region',
          'addr_locality'
        ]),
        type: this.props.query.as,
        birthdate: bd_year + '-' +
          _padStart(parseInt(bd_month, 10) + 1, 2, 0) + '-' +
          _padStart(parseInt(bd_day, 10), 2, 0)
      })
      .then(res => ({ id: res.data.payload.id, token: res.data.payload.token }))
      .then(({ id, token }) => Router.push(`/others/activate?id=${id}&tk=${token}`))
      .catch(err => {
        this.setState({
          working: false,
          err: err.response ? err.response.data : err
        })
      })
  }

  getMainErrorMessage = () => {
    if (!this.state.err) return ''
    switch (this.state.err.code) {
      case 'EAIP':
        return 'Please fix invalid fields'
      case 'EAAX':
        return 'User Already Registered'
      default:
        return ''
    }
  }

  getError = (name, bool) => {
    if (this.state.err && Array.isArray(this.state.err.payload)) {
      if (this.state.err.payload
        .find(errField => errField.param === name))
        return bool ? true : 'red'
    }
    return bool ? false : 'transparent'
  }

  render() {
    let { getError } = this
    return (
      <Layout>
        <Head>
          <title>Sign Up</title>
        </Head>
        <div className="color-text open-sans">
          <main className="tc">
            <div className="w-100 pa2 z-0">
              <form onSubmit={this.submitForm}>
                <h1>SIGN UP</h1>
                <h2>
                  <span>as </span>
                  <span className={`color-${this.props.query.as === 'provider' ? 'a2' : 'b2'}`}>
                    {this.props.query.as === 'provider' ? 'Service Provider' : 'Client'}
                  </span>
                </h2>
                <div className="red">&nbsp;{this.getMainErrorMessage()}&nbsp;</div>
                <div className="mt3 mw6 center">
                  <input
                    className={`ba b--${getError('firstname')} ${this.classStyle}`}
                    type="text"
                    name="firstname"
                    onChange={this.changeState}
                    placeholder="Fist Name" />
                  <br />
                  <input
                    className={`ba b--${getError('lastname')} ${this.classStyle}`}
                    type="text"
                    name="lastname"
                    onChange={this.changeState}
                    placeholder="Last Name" />
                  <br />
                  <select
                    className={`ba b--${getError('gender')} ${this.classStyle}`}
                    value={this.state.gender}
                    name="gender"
                    onChange={this.changeState}>
                    >
                    <option value="" disabled>Gender</option>
                    <option value="M">Male</option>
                    <option value="F">Female</option>
                  </select>
                  <br />
                  {/* --- Birth Date --- */}
                  <div>
                    <h3 className={`${getError('birthdate') === 'transparent' ? '' : 'red'}`}>Birth Date</h3>
                    <select
                      className={`${this.classStyle} bn`}
                      value={this.state.bd_month}
                      name="bd_month"
                      onChange={this.changeState}>
                      <option value="" disabled>Month</option>
                      {getMonthList().map((month, i) => {
                        return (<option key={i} value={i}>{month}</option>)
                      })}
                    </select>
                    <br />
                    <div className="w-100 flex">
                      <select
                        className={`${this.classStyle} fl w-50 bn mr2`}
                        value={this.state.bd_day}
                        name="bd_day"
                        onChange={this.changeState}>
                        <option value="" disabled>Day</option>
                        {(Array.from(Array(getDaysOfMonth(parseInt(this.state.bd_month, 10) || 0)).keys())).map(item => {
                          let _val = item + 1
                          return (
                            <option key={_val} value={_val}>{_val}</option>
                          )
                        })}
                      </select>
                      <select
                        className={`${this.classStyle} fl w-50 bn ml2`}
                        value={this.state.bd_year}
                        name="bd_year"
                        onChange={this.changeState}>
                        <option value="" disabled>Year</option>
                        {(Array.from(Array(100).keys())).map(item => {
                          let _val = this.state.now_year - item
                          return (
                            <option key={_val} value={_val}>{_val}</option>
                          )
                        })}
                      </select>
                    </div>
                  </div>
                  <br />
                  {/* --- Address--- */}
                  <div>
                    <h3 className={`${getError('address') === 'transparent' ? '' : 'red'}`}>Address</h3>
                    <input
                      className={`ba b--${getError('addr_line1')} ${this.classStyle}`}
                      type="text"
                      name="addr_line1"
                      onChange={this.changeState}
                      placeholder="Line 1" />
                    <br />
                    <input
                      className={`ba b--${getError('addr_line2')} ${this.classStyle}`}
                      type="text"
                      name="addr_line2"
                      onChange={this.changeState}
                      placeholder="Line 2" />
                    <br />
                    <select
                      className={`ba b--${getError('addr_country')} ${this.classStyle}`}
                      value={this.state.addr_country}
                      name="addr_country"
                      onChange={this.changeState}>
                      <option value="" disabled>Country</option>
                      {
                        this.props.ui.country.list.map(country => {
                          return (
                            <option
                              key={country['ISO3166-1-Alpha-2']}
                              value={country['ISO3166-1-Alpha-2']}>
                              {country.name}
                            </option>
                          )
                        })
                      }
                    </select>
                    <br />
                    <input
                      className={`ba b--${getError('addr_region')} ${this.classStyle}`}
                      type="text"
                      name="addr_region"
                      onChange={this.changeState}
                      placeholder="Region" />
                    <br />
                    <input
                      className={`ba b--${getError('addr_locality')} ${this.classStyle}`}
                      type="text"
                      name="addr_locality"
                      onChange={this.changeState}
                      placeholder="Area" />
                  </div>
                  <br />
                  {/* --- Login Credentials --- */}
                  <div>
                    <h3>Login Credentials</h3>
                    <input
                      className={`${this.classStyle} ba b--${getError('email')}`}
                      type="email"
                      name="email"
                      onChange={this.changeState}
                      placeholder="Email" />
                    <br />
                    <input
                      className={`${this.classStyle} ba b--${getError('password')}`}
                      type="password"
                      name="password"
                      onChange={this.changeState}
                      placeholder="Password" />
                    <p className="red mt2 mb0 f6">
                      &nbsp;{getError('password', true) && 'Must be 8+ characters long'}&nbsp;
                    </p>
                  </div>
                </div>
                <button
                  className="pv2 ph5 relative ma2 br-pill bn shadow-4 b f3 bg-color-text white mt4 pointer"
                  type="submit">
                  {this.state.working && (<span className="absolute left-1"><Progress color="#ffffff" /></span>)}
                  <span>Sign Up</span>
                </button>
                <div className="mt3 pointer ph3 ph5-ns lh-copy f6 mw6 tc center mid-gray links">
                  <span>By clicking Sign Up, you agree to our&nbsp;
                    <Link href="/terms"><a className="fw6">Terms & Conditions</a></Link>
                    &nbsp;and&nbsp;
                    <Link href="/privacy"><a className="fw6">Privacry Policy</a></Link>
                  </span>
                </div>
              </form>
            </div>
          </main>
        </div>
      </Layout>
    )
  }

}

const stateProps = ({ user, account, ui }) => ({
  user,
  account,
  ui
})

export default connect(stateProps)(SignUp)