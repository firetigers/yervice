import { Component } from 'react'
import { connect } from 'react-redux'
import Router from 'next/router'
import Head from 'next/head'
import Layout from '../../layouts/main'
import Link from 'next/link'

class SignUpSelect extends Component {

  constructor(props) {
    super(props)
    if (global.document && props.user.id) Router.push('/')
  }

  render() {
    return (
      <Layout>
        <Head>
          <title>Sign Up</title>
        </Head>
        <main className="color-text tc">
          <h1>SIGN UP</h1>
          <br />
          <p className="lh-copy fw6">What type of account would you like to create?</p>
          <br />
          {/* justify-center  */}
          <section className="mw6 mw7-l flex flex-column flex-row-l center mb4 lh-copy ph3 ">
            <Link href="/signup?as=provider">
              <a className="h4 w-100 br3 bg-color-a2 white grow no-underline mr4 mb4">
                <h2 className="mb3">Service Provider</h2>
                <p>A person who provide service</p>
              </a>
            </Link>
            <Link href="/signup?as=client">
              <a className="h4 w-100 br3 bg-color-b2 white grow no-underline">
                <h2 className="mb3">Client</h2>
                <p>A person who require service</p>
              </a>
            </Link>
          </section>
          <br />
          <section className="mw6 center links f6 ph3">
            <p className="lh-copy gray"><b>Note:</b><br />You can be a service provider and a client at the same time. <br />
              <Link href="/help">
                <a>Please click here for more info</a>
              </Link>
            </p>
          </section>
        </main>
      </Layout>
    )
  }

}

const stateProps = ({ user }) => ({
  user
})

const dispatchProps = dispatch => ({
  // Nother
})

export default connect(stateProps, dispatchProps)(SignUpSelect)