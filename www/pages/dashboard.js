import { Component } from 'react'
import { connect } from 'react-redux'
import Router from 'next/router'
import Head from 'next/head'
import Layout from '../layouts/main'
import Link from 'next/link'

class SignUpSelect extends Component {

  isAllAccount = false

  constructor(props) {
    super(props)
    if (global.document) {
      let sp = props.account.provider.id
      let cl = props.account.client.id
      if (sp && cl) {
        this.isAllAccount = true
      } else {
        // Not all account or no account at all
        if (cl) Router.push('/client')
        else Router.push('/provider')
      }
    }
  }

  render() {
    return (
      <Layout auth hide={!this.isAllAccount}>
        <Head>
          <title>Select Dashboard</title>
        </Head>
        <main className="color-text tc">
          <h1>Select Dashboard</h1>
          <br />
          <p className="lh-copy fw6">Which dashboard would you like to view?</p>
          <br />
          {/* justify-center  */}
          <section className="mw6 mw7-l flex flex-column flex-row-l center mb4 lh-copy ph3 ">
            <Link href="/provider">
              <a className="h4 w-100 br3 bg-color-a2 white grow no-underline mr4 mb4">
                <h2 className="mb3">Service Provider</h2>
              </a>
            </Link>
            <Link href="/client">
              <a className="h4 w-100 br3 bg-color-b2 white grow no-underline">
                <h2 className="mb3">Client</h2>
              </a>
            </Link>
          </section>
        </main>
      </Layout>
    )
  }

}

const stateProps = ({ user, account }) => ({
  user, account
})

const dispatchProps = dispatch => ({
  // Nother
})

export default connect(stateProps, dispatchProps)(SignUpSelect)