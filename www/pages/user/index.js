import { Component } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import Layout from '../../layouts/main'
import Tabs from '../../components/tabs'

import ContentAddresses from '../../containers/user/addresses'

export default class extends Component {
  render() {
    return (
      <Layout>
        <Head>
          <title>User</title>
        </Head>
        <h1 className="tc">User Information</h1>
        <br/>
        <div className="ba b--moon-gray br3 mw7 center">
          <Tabs classNameTabs="bb b--moon-gray" default="0">
            <div name="General" className="pa3">
              <h1>Todo: General Info</h1>
              <ul>
                <li>Display user information</li>
                <li>User specific settings and control</li>
                <li>Allow user to activate both Service Provider and Client Accounts</li>
              </ul>
              <hr />
              <h3>For Demo Only</h3>
              <p>Click the following button to activate now.</p>
              <p>
                <Link href="/provider/activate">
                  <a>Activate Service Provider</a>
                </Link>
                <br />
                <br />
                <Link href="/client/activate">
                  <a>Activate Client </a>
                </Link>
              </p>
            </div>
            <div name="Addresses" className="">
              <ContentAddresses />
            </div>
          </Tabs>
        </div>
      </Layout>
    )
  }
}