import { Component } from 'react'
import { connect } from 'react-redux'
import Head from 'next/head'
import Link from 'next/link'
import Router from 'next/router'
import Layout from '../layouts/main'
import { login as loginDispatcher } from '../store/dispatchers/auth'
import Progress from '../components/progress'

class LoginPage extends Component {

  state = {}

  constructor(props) {
    super(props)
    if (global.document && props.user.id) Router.push('/dashboard')
  }

  changeState = (e) => {
    e.preventDefault()
    this.setState({ [e.target.name]: e.target.value })
  }

  submitForm = (e) => {
    e.preventDefault()
    if (this.state.working) return
    this.setState({ working: true, err: null })
    this.props.login(this.state.email, this.state.password)
      .then(() => Router.push('/dashboard'))
      .catch(err => this.setState({ working: false, err }))
  }

  render() {
    return (
      <Layout logo="home">
        <Head>
          <title>Login</title>
        </Head>
        <div className="color-text">
          <main className="tc">
            <div className="w-100 pa2 z-0 links">
              <form onSubmit={this.submitForm}>
                <div className="mt3 w5 center ph4 ph0-l">
                  <img className="mw5" src="/static/images/logos/logo-dark-color.svg" />
                </div>
                <div className="mt5 mw6 center">
                  <input className="w-100 pa2 ph4 mb3 br-pill bn shadow-4 b f4 bg-light-gray" onChange={this.changeState} name="email" type="email" placeholder="Email" />
                  <br />
                  <input className="w-100 pa2 ph4 mb2 br-pill bn shadow-4 b f4 bg-light-gray" onChange={this.changeState} name="password" type="password" placeholder="Password" />
                  <div className="red pv2">&nbsp;{this.state.err ? 'Invalid email or password' : ''}&nbsp;</div>
                  <button className="relative pv2 ph5 ma2 br-pill bn shadow-4 b f3 bg-color-text white pointer" type="submit">
                    {this.state.working && (<span className="absolute left-1"><Progress color="#ffffff" /></span>)}
                    <span>Login</span>
                  </button>
                </div>
                <Link href="/others/forgotpassword">
                  <a className="dib mt3 b">
                    <span>Forgot Password?</span>
                  </a>
                </Link>
              </form>
            </div>
            <div className="w-100 pa2 mt4 dt mw5 center">
              <div>
                <span className="pa3 bg-almost-white relative top-1 z-1">OR</span><hr />
              </div>
              <Link href="/signup">
                <button className="ph4 pv2 mt4 br-pill bn shadow-4 f4 white bg-color-primary-light pointer nowrap">Sign Up</button>
              </Link>
            </div>
          </main>
        </div>
      </Layout>
    )
  }

}

const stateProps = ({ user }) => ({
  user
})

const dispatchProps = dispatch => ({
  login: loginDispatcher(dispatch)
})

export default connect(stateProps, dispatchProps)(LoginPage)