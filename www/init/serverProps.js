
/**
 * Sets initial data for pages.
 * This runs on first load and only on server
 * 
 * eg. first route or refresh page
 */

const _pick = require('lodash/pick')
const includeConfig = [
  'production',
  'browsers'
]

export default (req, res) => {
  // Get all required data
  return Promise.all(
    [
      Model.Category.find({}).exec(),
      Model.Service.find({}).exec()
    ])
    .then(results => {
      return {
        config: _pick(global.Config, includeConfig),
        categories: results[0],
        services: results[1]
      }
    })
}