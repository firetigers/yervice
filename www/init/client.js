/**
 * Initialize client side libraries
 */

import Router from 'next/router'
import axios from 'axios'
import numbro from 'numbro'
import { setServices } from '../store/actions/service'
import { setCategories } from '../store/actions/category'
import { setUIReady } from '../store/actions/ui'
import { refreshLogin } from '../store/effects/auth'
import elementPolyfills from '../utils/elementPolyfills'
import { logout as authLogout } from '../store/effects/auth'
import { reset } from '../store/actions/auth'
import { refresh } from '../store/dispatchers/auth'
let refreshCount = 1

function setAxios(dispatch) {
  // Intercept axios for authentication
  axios.interceptors.response.use(
    res => res,
    err => {
      if (err.response && err.response.data.code === 'EAUA') {
        // Refresh token, if can't refresh, goto login page
        return Promise.resolve()
          .then(() => {
            // Set a threshold to avoid infinit refresh loop
            if (++refreshCount > 3) throw new Error()
            return refreshLogin()
          })
          .then(() => {
            // Update the config with the new access token
            // and re-initiate request
            err.config.headers['Authorization'] = axios.defaults
              .headers.common['Authorization']
            return axios(err.config)
          })
          .catch(() => {
            refreshCount = 1
            authLogout()
            dispatch(reset())
            Router.push('/login')
          })
      }
      return Promise.reject(err)
    }
  )
}

function setNumbro() {
  numbro.setDefaults({
    thousandSeparated: true,
    mantissa: 2
  });
}

export default (props) => {
  let { dispatch } = props.store
  elementPolyfills()
  setAxios(dispatch)
  setNumbro()
  // Set init props from server
  dispatch(setServices(props.pageProps.services))
  dispatch(setCategories(props.pageProps.categories))
  // Set ui ready
  refresh(dispatch)(true).catch(() => null)
    .finally(() => dispatch(setUIReady(true)))
}