import { Component } from 'react'
import { connect } from 'react-redux'
import Router from 'next/router'
import Footer from '../containers/footer'
import Header from '../containers/header'
import Progess from '../components/progress'

class Layout extends Component {

  constructor(props) {
    super(props)
    if (global.document) {
      if (props.auth && !props.user.id) Router.push('/login')
    }
  }

  render() {
    return (
      <div className="layout">
        <Header logo={this.props.logo}></Header>
        <div className="content color-text ph3 pt3 pb4 pb5-l">
          {
            !this.props.ui.ready || this.props.hide || (this.props.auth && !this.props.user.id) ?
              (
                <div className="tc pt5">
                  <Progess size={30} />
                </div>
              ) :
              this.props.children
          }
        </div>
        <Footer />
      </div>
    )
  }

}

const stateProps = ({ user, ui }) => ({
  user,
  ui
})

export default connect(stateProps)(Layout)