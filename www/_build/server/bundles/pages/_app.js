module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 67);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash_keyBy__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash_keyBy___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash_keyBy__);

var actionTypes = [
/**
 * NAMES
 */
'PROVIDER', 'CLIENT',
/**
 * ACTIONS
 * 
 * Define all possible actions
 * 
 * Format: <name...>_<action>
 * eg. 
 *  - USER_SET
 *  - AUTH_LOGIN (Authentication Login)
 */
'UI_READY_SET', 'COUNTRY_LIST_SET', 'PROVIDER_SET', 'PROVIDER_SERVICES_SET', 'PROVIDER_SERVICE_ADD', 'PROVIDER_SERVICE_REM', 'PROVIDER_SERVICE_PRICE_SET', 'CLIENT_SET', 'CLIENT_ORDER_SET', 'CLIENT_ORDER_REM', 'CATEGORY_LIST_SET', 'SERVICE_LIST_SET', 'USER_SET', 'USER_ADDRESS_ADD', 'USER_ADDRESS_REM', 'AUTH_LOGIN', 'AUTH_REFRESH', 'RESET'];
/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0_lodash_keyBy___default()(actionTypes, function (action) {
  return action;
}));

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),
/* 3 */,
/* 4 */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = login;
/* harmony export (immutable) */ __webpack_exports__["c"] = refreshLogin;
/* harmony export (immutable) */ __webpack_exports__["b"] = logout;
/* unused harmony export getUser */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1____ = __webpack_require__(13);



function getScript(source, callback) {
  var script = document.createElement('script');
  var prior = document.getElementsByTagName('script')[0];
  script.async = 1;

  script.onload = script.onreadystatechange = function (_, isAbort) {
    if (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {
      script.onload = script.onreadystatechange = null;
      script = undefined;

      if (!isAbort) {
        if (callback) return callback();
      }
    }

    if (callback) return callback(new Error('Aborted'));
  };

  script.onerror = function (e) {
    if (callback) return callback(err);
  };

  script.src = source;
  prior.parentNode.insertBefore(script, prior);
}

function downloadFingerprint() {
  if (global.Fingerprint2) return;
  return new Promise(function (resolve, reject) {
    getScript('/static/libs/fingerprint2.min.js', function (err) {
      if (err) return reject(err);
      resolve();
    });
  });
}

function setAuth(res) {
  // Save access token to every request
  __WEBPACK_IMPORTED_MODULE_0_axios___default.a.defaults.headers.common['Authorization'] = 'Bearer ' + res.data.payload.access_token; // if (global.Fingerprint2) global.Fingerprint2 = null

  return res.data.payload;
}

function unsetAuth() {
  delete __WEBPACK_IMPORTED_MODULE_0_axios___default.a.defaults.headers.common['Authorization'];
}

function getFingerprint() {
  return Promise.resolve().then(downloadFingerprint).then(function () {
    return Promise.delay(1000);
  }).then(function () {
    return new Promise(function (resolve, reject) {
      new global.Fingerprint2().get(function (fp) {
        if (fp) resolve(fp);else reject(new Error('Fingerprint hash required'));
      });
    });
  });
}

function login(email, password) {
  return Promise.resolve().then(getFingerprint).then(function (fp) {
    return __WEBPACK_IMPORTED_MODULE_0_axios___default.a.post('/api/login', {
      email: email,
      password: password,
      fingerprint: fp
    });
  }).then(setAuth);
}
function refreshLogin(checkToken) {
  return Promise.resolve().then(function () {
    if (checkToken && __WEBPACK_IMPORTED_MODULE_0_axios___default.a.defaults.headers.common['Authorization']) throw new Error('Already has token');
  }).then(getFingerprint).then(function (fp) {
    var rt = Object(__WEBPACK_IMPORTED_MODULE_1____["b" /* getState */])().auth.refresh_token;
    if (!rt) throw new Error('No refresh token');
    return __WEBPACK_IMPORTED_MODULE_0_axios___default.a.post('/api/login/refresh', {
      fingerprint: fp
    }, {
      headers: {
        'Authorization': 'Bearer ' + Object(__WEBPACK_IMPORTED_MODULE_1____["b" /* getState */])().auth.refresh_token
      }
    });
  }).then(setAuth);
}
function logout() {
  unsetAuth();
}
function getUser() {
  return __WEBPACK_IMPORTED_MODULE_0_axios___default.a.get('/api/user').then(function (res) {
    return res.data.payload;
  });
}

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("lodash/remove");

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("lodash/get");

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),
/* 10 */,
/* 11 */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/regenerator");

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = require("lodash/find");

/***/ }),
/* 13 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "@babel/runtime/regenerator"
var regenerator_ = __webpack_require__(11);
var regenerator__default = /*#__PURE__*/__webpack_require__.n(regenerator_);

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "react-redux"
var external__react_redux_ = __webpack_require__(4);
var external__react_redux__default = /*#__PURE__*/__webpack_require__.n(external__react_redux_);

// EXTERNAL MODULE: external "redux"
var external__redux_ = __webpack_require__(2);
var external__redux__default = /*#__PURE__*/__webpack_require__.n(external__redux_);

// EXTERNAL MODULE: external "redux-promise-middleware"
var external__redux_promise_middleware_ = __webpack_require__(20);
var external__redux_promise_middleware__default = /*#__PURE__*/__webpack_require__.n(external__redux_promise_middleware_);

// EXTERNAL MODULE: external "lodash/throttle"
var throttle_ = __webpack_require__(21);
var throttle__default = /*#__PURE__*/__webpack_require__.n(throttle_);

// EXTERNAL MODULE: external "lodash/get"
var get_ = __webpack_require__(8);
var get__default = /*#__PURE__*/__webpack_require__.n(get_);

// EXTERNAL MODULE: external "lodash/pick"
var pick_ = __webpack_require__(14);
var pick__default = /*#__PURE__*/__webpack_require__.n(pick_);

// EXTERNAL MODULE: external "redux-devtools-extension"
var external__redux_devtools_extension_ = __webpack_require__(22);
var external__redux_devtools_extension__default = /*#__PURE__*/__webpack_require__.n(external__redux_devtools_extension_);

// CONCATENATED MODULE: ./store/storage.js
var readState = function readState() {
  try {
    var serializedState = localStorage.getItem('state');
    if (serializedState === undefined || serializedState === null) return;
    return JSON.parse(serializedState);
  } catch (err) {// Do nothing, function will return undefined
  }
};
var writeState = function writeState(state) {
  try {
    localStorage.setItem('state', JSON.stringify(state));
  } catch (err) {// Do nothing, function will return undefined
  }
};
// EXTERNAL MODULE: ./store/types.js
var types = __webpack_require__(1);

// CONCATENATED MODULE: ./store/reducers/auth.js
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


var init = {
  refresh_token: null
};
/* harmony default export */ var auth = (function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : init;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].AUTH_LOGIN + '_FULFILLED':
      return {
        refresh_token: action.payload.refresh_token || state.refresh_token
      };

    case types["a" /* default */].RESET:
      return _objectSpread({}, init);
  }

  return state;
});
// EXTERNAL MODULE: external "lodash/remove"
var remove_ = __webpack_require__(7);
var remove__default = /*#__PURE__*/__webpack_require__.n(remove_);

// CONCATENATED MODULE: ./store/reducers/user.js
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function user__objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { user__defineProperty(target, key, source[key]); }); } return target; }

function user__defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




var user_init = {
  id: null,
  avatar: null,
  firstname: null,
  lastname: null,
  addresses: []
};
/* harmony default export */ var reducers_user = (function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : user_init;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].USER_SET:
      var user = action.user;
      return user__objectSpread({}, state, {
        addresses: user.addresses || [],
        lastname: user.lastname,
        firstname: user.firstname,
        avatar: user.avatar,
        id: user._id
      });

    case types["a" /* default */].USER_ADDRESS_ADD + '_FULFILLED':
      return user__objectSpread({}, state, {
        addresses: _toConsumableArray(state.addresses).concat([action.payload.data.payload])
      });

    case types["a" /* default */].USER_ADDRESS_REM + '_FULFILLED':
      remove__default()(state.addresses, {
        _id: get__default()(action, 'payload.data.payload._id')
      });

      return user__objectSpread({}, state, {
        addresses: _toConsumableArray(state.addresses)
      });

    case types["a" /* default */].RESET:
      return user__objectSpread({}, user_init);
  }

  return state;
});
// CONCATENATED MODULE: ./store/reducers/account/provider.js
function provider__objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { provider__defineProperty(target, key, source[key]); }); } return target; }

function provider__defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


var provider_init = {
  id: null
};
/* harmony default export */ var provider = (function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : provider_init;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].PROVIDER_SET:
      return provider__objectSpread({}, state, {
        id: action.id
      });

    case types["a" /* default */].RESET:
      return provider__objectSpread({}, provider_init);
  }

  return state;
});
// CONCATENATED MODULE: ./store/reducers/account/client.js
function client__objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { client__defineProperty(target, key, source[key]); }); } return target; }

function client__defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


var client_init = {
  id: null,
  order: {}
};
/* harmony default export */ var client = (function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : client_init;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].CLIENT_ORDER_SET:
      return client__objectSpread({}, state, {
        order: action.order || {}
      });

    case types["a" /* default */].CLIENT_ORDER_REM:
      return client__objectSpread({}, state, {
        order: {}
      });

    case types["a" /* default */].CLIENT_SET:
      return client__objectSpread({}, state, {
        id: action.id
      });

    case types["a" /* default */].RESET:
      return client__objectSpread({}, client_init);
  }

  return state;
});
// CONCATENATED MODULE: ./store/reducers/account/index.js




/* harmony default export */ var account = (Object(external__redux_["combineReducers"])({
  client: client,
  provider: provider
}));
// EXTERNAL MODULE: external "lodash/find"
var find_ = __webpack_require__(12);
var find__default = /*#__PURE__*/__webpack_require__.n(find_);

// CONCATENATED MODULE: ./store/reducers/provider/services.js
function services__toConsumableArray(arr) { return services__arrayWithoutHoles(arr) || services__iterableToArray(arr) || services__nonIterableSpread(); }

function services__nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function services__iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function services__arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }




var services_init = [];
/* harmony default export */ var services = (function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : services_init;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].PROVIDER_SERVICES_SET:
      return action.list || [];

    case types["a" /* default */].PROVIDER_SERVICE_ADD:
      return services__toConsumableArray(state).concat([action.service]);

    case types["a" /* default */].PROVIDER_SERVICE_REM:
      remove__default()(state, function (item) {
        return item._id === action.provServiceId;
      });

      return services__toConsumableArray(state);

    case types["a" /* default */].PROVIDER_SERVICE_PRICE_SET + '_FULFILLED':
      var service = find__default()(state, {
        _id: action.payload._id
      });

      if (service) service.price = action.payload.price;
      return services__toConsumableArray(state);

    case types["a" /* default */].RESET:
      return services_init;
  }

  return state;
});
// CONCATENATED MODULE: ./store/reducers/provider/index.js


/* harmony default export */ var reducers_provider = (Object(external__redux_["combineReducers"])({
  services: services
}));
// CONCATENATED MODULE: ./store/reducers/client/index.js
function reducers_client__objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { reducers_client__defineProperty(target, key, source[key]); }); } return target; }

function reducers_client__defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


var reducers_client_init = {
  orders: []
};
/* harmony default export */ var reducers_client = (function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : reducers_client_init;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].RESET:
      return reducers_client__objectSpread({}, reducers_client_init);
  }

  return state;
});
// CONCATENATED MODULE: ./store/reducers/service/list.js
function list__toConsumableArray(arr) { return list__arrayWithoutHoles(arr) || list__iterableToArray(arr) || list__nonIterableSpread(); }

function list__nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function list__iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function list__arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }


var list_init = [];
/* harmony default export */ var list = (function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : list_init;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].SERVICE_LIST_SET:
      return list__toConsumableArray(action.list || []);
  }

  return state;
});
// CONCATENATED MODULE: ./store/reducers/service/index.js


/* harmony default export */ var reducers_service = (Object(external__redux_["combineReducers"])({
  list: list
}));
// CONCATENATED MODULE: ./store/reducers/category/list.js
function category_list__toConsumableArray(arr) { return category_list__arrayWithoutHoles(arr) || category_list__iterableToArray(arr) || category_list__nonIterableSpread(); }

function category_list__nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function category_list__iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function category_list__arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }


var category_list_init = [];
/* harmony default export */ var category_list = (function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : category_list_init;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].CATEGORY_LIST_SET:
      return category_list__toConsumableArray(action.list || []);
  }

  return state;
});
// CONCATENATED MODULE: ./store/reducers/category/index.js


/* harmony default export */ var category = (Object(external__redux_["combineReducers"])({
  list: category_list
}));
// CONCATENATED MODULE: ./store/reducers/ui/country.js
function country__objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { country__defineProperty(target, key, source[key]); }); } return target; }

function country__defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


var country_init = {
  list: []
};
/* harmony default export */ var country = (function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : country_init;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].COUNTRY_LIST_SET + '_FULFILLED':
      return country__objectSpread({}, state, {
        list: action.payload || []
      });
  }

  return state;
});
// CONCATENATED MODULE: ./store/reducers/ui/index.js


 // Ready

var ui_ready = function ready() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].UI_READY_SET:
      return action.ready || false;
  }

  return state;
};

/* harmony default export */ var ui = (Object(external__redux_["combineReducers"])({
  ready: ui_ready,
  country: country
}));
// CONCATENATED MODULE: ./store/reducers/index.js









/**
 * Root reducer
 */

/* harmony default export */ var reducers = (Object(external__redux_["combineReducers"])({
  account: account,
  auth: auth,
  category: category,
  client: reducers_client,
  service: reducers_service,
  provider: reducers_provider,
  user: reducers_user,
  ui: ui
}));
/**
 * Filter states that will be saved in localstorage
 */

var persist = ['account', 'auth', 'user'];
// CONCATENATED MODULE: ./store/index.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return store_getState; });



function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function store__objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { store__defineProperty(target, key, source[key]); }); } return target; }

function store__defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }











var theStore;

function initializeStore() {
  var rootState = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var middlewares = Object(external__redux_["applyMiddleware"])(external__redux_promise_middleware__default()());
  if (!get__default()(global, '__NEXT_DATA__.props.pageProps.config.production')) middlewares = Object(external__redux_devtools_extension_["composeWithDevTools"])(middlewares);
  return Object(external__redux_["createStore"])(reducers, rootState, middlewares);
}

var store_getState = function getState(path) {
  return path ? get__default()(theStore.getState(), path) : theStore.getState();
};
/* harmony default export */ var store = __webpack_exports__["a"] = (function (App) {
  return (
    /*#__PURE__*/
    function (_Component) {
      _inherits(AppWithRedux, _Component);

      function AppWithRedux(props) {
        var _this;

        _classCallCheck(this, AppWithRedux);

        _this = _possibleConstructorReturn(this, (AppWithRedux.__proto__ || Object.getPrototypeOf(AppWithRedux)).call(this, props)); // Create store

        if (global.document) {
          // Client side createStore
          theStore = initializeStore(readState());
          theStore.subscribe(throttle__default()(function () {
            writeState(pick__default()(_this.reduxStore.getState(), persist));
          }, 1000));
        } else {
          // Server side createStore (no need initial state)
          theStore = Object(external__redux_["createStore"])(reducers);
        }

        _this.reduxStore = theStore;
        return _this;
      }

      _createClass(AppWithRedux, [{
        key: "render",
        value: function render() {
          return external__react__default.a.createElement(external__react_redux_["Provider"], {
            store: this.reduxStore
          }, external__react__default.a.createElement(App, _extends({}, this.props, {
            store: this.reduxStore
          })));
        }
      }], [{
        key: "getInitialProps",
        value: function () {
          var _getInitialProps = _asyncToGenerator(
          /*#__PURE__*/
          regenerator__default.a.mark(function _callee(appContext) {
            var appProps;
            return regenerator__default.a.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    appProps = {};

                    if (!(typeof App.getInitialProps === 'function')) {
                      _context.next = 5;
                      break;
                    }

                    _context.next = 4;
                    return App.getInitialProps.call(App, appContext);

                  case 4:
                    appProps = _context.sent;

                  case 5:
                    return _context.abrupt("return", store__objectSpread({}, appProps));

                  case 6:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));

          return function getInitialProps(_x) {
            return _getInitialProps.apply(this, arguments);
          };
        }()
      }]);

      return AppWithRedux;
    }(external__react_["Component"])
  );
});

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = require("lodash/pick");

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/styles");

/***/ }),
/* 16 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return reset; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return login; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return refresh; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__effects_auth__ = __webpack_require__(6);



var reset = function reset() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__types__["a" /* default */].RESET
  };
};
var login = function login(email, password) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__types__["a" /* default */].AUTH_LOGIN,
    payload: Object(__WEBPACK_IMPORTED_MODULE_1__effects_auth__["a" /* login */])(email, password)
  };
};
var refresh = function refresh(checkToken) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__types__["a" /* default */].AUTH_REFRESH,
    payload: Object(__WEBPACK_IMPORTED_MODULE_1__effects_auth__["c" /* refreshLogin */])(checkToken)
  };
};

/***/ }),
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */
/***/ (function(module, exports) {

module.exports = require("redux-promise-middleware");

/***/ }),
/* 21 */
/***/ (function(module, exports) {

module.exports = require("lodash/throttle");

/***/ }),
/* 22 */
/***/ (function(module, exports) {

module.exports = require("redux-devtools-extension");

/***/ }),
/* 23 */
/***/ (function(module, exports) {

module.exports = require("lodash/keyBy");

/***/ }),
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */,
/* 39 */,
/* 40 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["b"] = register;
/* harmony export (immutable) */ __webpack_exports__["a"] = postNewAddress;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_axios__);

function register(fields) {
  return __WEBPACK_IMPORTED_MODULE_0_axios___default.a.post('/api/register', fields);
}
function postNewAddress(fields) {
  return __WEBPACK_IMPORTED_MODULE_0_axios___default.a.post('/api/user/address', fields);
}

/***/ }),
/* 41 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return setUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return addNewAddress; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return remAddress; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__effects_user__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_axios__);



var setUser = function setUser(user) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__types__["a" /* default */].USER_SET,
    user: user
  };
};
var addNewAddress = function addNewAddress(fields) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__types__["a" /* default */].USER_ADDRESS_ADD,
    payload: Object(__WEBPACK_IMPORTED_MODULE_1__effects_user__["a" /* postNewAddress */])(fields)
  };
};
var remAddress = function remAddress(id) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__types__["a" /* default */].USER_ADDRESS_REM,
    payload: __WEBPACK_IMPORTED_MODULE_2_axios___default.a.delete('/api/user/address', {
      data: {
        id: id
      }
    })
  };
};

/***/ }),
/* 42 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return setUIReady; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loadCountryList; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_axios__);


var setUIReady = function setUIReady(ready) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__types__["a" /* default */].UI_READY_SET,
    ready: ready
  };
};
var loadCountryList = function loadCountryList() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__types__["a" /* default */].COUNTRY_LIST_SET,
    payload: __WEBPACK_IMPORTED_MODULE_1_axios___default.a.get('/static/country-codes.min.json').then(function (response) {
      return response.data;
    })
  };
};

/***/ }),
/* 43 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return setProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return setClient; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__types__ = __webpack_require__(1);

var setProvider = function setProvider(provider) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__types__["a" /* default */].PROVIDER_SET,
    id: provider._id
  };
};
var setClient = function setClient(client) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__types__["a" /* default */].CLIENT_SET,
    id: client._id
  };
};

/***/ }),
/* 44 */,
/* 45 */,
/* 46 */,
/* 47 */,
/* 48 */,
/* 49 */
/***/ (function(module, exports) {

module.exports = require("numbro");

/***/ }),
/* 50 */,
/* 51 */,
/* 52 */,
/* 53 */,
/* 54 */,
/* 55 */,
/* 56 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return login; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return refresh; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__actions_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__actions_user__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__actions_account__ = __webpack_require__(43);




function hydrate(dispatch) {
  return function (_ref) {
    var _ref$value = _ref.value,
        user = _ref$value.user,
        provider = _ref$value.provider,
        client = _ref$value.client;
    dispatch(Object(__WEBPACK_IMPORTED_MODULE_1__actions_user__["c" /* setUser */])(user));
    if (provider) dispatch(Object(__WEBPACK_IMPORTED_MODULE_2__actions_account__["b" /* setProvider */])(provider));
    if (client) dispatch(Object(__WEBPACK_IMPORTED_MODULE_2__actions_account__["a" /* setClient */])(client));
  };
}

var login = function login(dispatch) {
  return function (email, password) {
    return dispatch(Object(__WEBPACK_IMPORTED_MODULE_0__actions_auth__["a" /* login */])(email, password)).then(hydrate(dispatch));
  };
};
var refresh = function refresh(dispatch) {
  return function () {
    var checkToken = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
    return dispatch(Object(__WEBPACK_IMPORTED_MODULE_0__actions_auth__["b" /* refresh */])(checkToken)).then(hydrate(dispatch));
  };
};

/***/ }),
/* 57 */,
/* 58 */,
/* 59 */,
/* 60 */,
/* 61 */,
/* 62 */,
/* 63 */,
/* 64 */,
/* 65 */,
/* 66 */,
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(68);


/***/ }),
/* 68 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: external "@babel/runtime/regenerator"
var regenerator_ = __webpack_require__(11);
var regenerator__default = /*#__PURE__*/__webpack_require__.n(regenerator_);

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "next/app"
var app_ = __webpack_require__(69);
var app__default = /*#__PURE__*/__webpack_require__.n(app_);

// EXTERNAL MODULE: external "@material-ui/core/styles"
var styles_ = __webpack_require__(15);
var styles__default = /*#__PURE__*/__webpack_require__.n(styles_);

// EXTERNAL MODULE: external "@material-ui/core/CssBaseline"
var CssBaseline_ = __webpack_require__(70);
var CssBaseline__default = /*#__PURE__*/__webpack_require__.n(CssBaseline_);

// EXTERNAL MODULE: external "react-jss/lib/JssProvider"
var JssProvider_ = __webpack_require__(71);
var JssProvider__default = /*#__PURE__*/__webpack_require__.n(JssProvider_);

// EXTERNAL MODULE: external "jss"
var external__jss_ = __webpack_require__(72);
var external__jss__default = /*#__PURE__*/__webpack_require__.n(external__jss_);

// CONCATENATED MODULE: ./style/mui/theme.js

/* harmony default export */ var theme = (Object(styles_["createMuiTheme"])({
  palette: {
    primary: {
      main: '#2f4858'
    },
    secondary: {
      main: '#FF4136'
    },
    error: {
      main: '#E7040F'
    }
  }
}));
// CONCATENATED MODULE: ./utils/pageContext.js




function createPageContext() {
  return {
    theme: theme,
    // This is needed in order to deduplicate the injection of CSS in the page.
    sheetsManager: new Map(),
    // This is needed in order to inject the critical CSS.
    sheetsRegistry: new external__jss_["SheetsRegistry"](),
    // The standard class name generator.
    generateClassName: Object(styles_["createGenerateClassName"])()
  };
}

function getPageContext() {
  // Make sure to create a new context for every server-side request so that data
  // isn't shared between connections (which would be bad).
  if (!process.browser) {
    return createPageContext();
  } // Reuse context on the client-side.


  if (!global.__INIT_MATERIAL_UI__) {
    global.__INIT_MATERIAL_UI__ = createPageContext();
  }

  return global.__INIT_MATERIAL_UI__;
}
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(5);
var router__default = /*#__PURE__*/__webpack_require__.n(router_);

// EXTERNAL MODULE: external "nprogress"
var external__nprogress_ = __webpack_require__(73);
var external__nprogress__default = /*#__PURE__*/__webpack_require__.n(external__nprogress_);

// EXTERNAL MODULE: ./store/index.js + 16 modules
var store = __webpack_require__(13);

// EXTERNAL MODULE: external "axios"
var external__axios_ = __webpack_require__(9);
var external__axios__default = /*#__PURE__*/__webpack_require__.n(external__axios_);

// EXTERNAL MODULE: external "numbro"
var external__numbro_ = __webpack_require__(49);
var external__numbro__default = /*#__PURE__*/__webpack_require__.n(external__numbro_);

// EXTERNAL MODULE: ./store/types.js
var types = __webpack_require__(1);

// CONCATENATED MODULE: ./store/actions/service.js

var service_setServices = function setServices(list) {
  return {
    type: types["a" /* default */].SERVICE_LIST_SET,
    list: list
  };
};
// CONCATENATED MODULE: ./store/actions/category.js

var category_setCategories = function setCategories(list) {
  return {
    type: types["a" /* default */].CATEGORY_LIST_SET,
    list: list
  };
};
// EXTERNAL MODULE: ./store/actions/ui/index.js
var ui = __webpack_require__(42);

// EXTERNAL MODULE: ./store/effects/auth.js
var auth = __webpack_require__(6);

// CONCATENATED MODULE: ./utils/elementPolyfills.js
/* harmony default export */ var elementPolyfills = (function () {
  (function (ElementProto) {
    /**
     * matches
     */
    if (typeof ElementProto.matches !== 'function') {
      ElementProto.matches = ElementProto.msMatchesSelector || ElementProto.mozMatchesSelector || ElementProto.webkitMatchesSelector || function matches(selector) {
        var element = this;
        var elements = (element.document || element.ownerDocument).querySelectorAll(selector);
        var index = 0;

        while (elements[index] && elements[index] !== element) {
          ++index;
        }

        return Boolean(elements[index]);
      };
    }
    /**
     * closest
     */


    if (typeof ElementProto.closest !== 'function') {
      ElementProto.closest = function closest(selector) {
        var element = this;

        while (element && element.nodeType === 1) {
          if (element.matches(selector)) {
            return element;
          }

          element = element.parentNode;
        }

        return null;
      };
    }
    /**
     * find & findAll (querySelector & querySelectorAll)
     */


    if (typeof ElementProto.find !== 'function') {
      ElementProto.find = ElementProto.querySelector;
    }

    if (typeof ElementProto.findAll !== 'function') {
      ElementProto.findAll = ElementProto.querySelectorAll;
    }
  })(global.Element.prototype);
});
// EXTERNAL MODULE: ./store/actions/auth.js
var actions_auth = __webpack_require__(16);

// EXTERNAL MODULE: ./store/dispatchers/auth.js
var dispatchers_auth = __webpack_require__(56);

// CONCATENATED MODULE: ./init/client.js
/**
 * Initialize client side libraries
 */











var refreshCount = 1;

function setAxios(dispatch) {
  // Intercept axios for authentication
  external__axios__default.a.interceptors.response.use(function (res) {
    return res;
  }, function (err) {
    if (err.response && err.response.data.code === 'EAUA') {
      // Refresh token, if can't refresh, goto login page
      return Promise.resolve().then(function () {
        // Set a threshold to avoid infinit refresh loop
        if (++refreshCount > 3) throw new Error();
        return Object(auth["c" /* refreshLogin */])();
      }).then(function () {
        // Update the config with the new access token
        // and re-initiate request
        err.config.headers['Authorization'] = external__axios__default.a.defaults.headers.common['Authorization'];
        return external__axios__default()(err.config);
      }).catch(function () {
        refreshCount = 1;
        Object(auth["b" /* logout */])();
        dispatch(Object(actions_auth["c" /* reset */])());
        router__default.a.push('/login');
      });
    }

    return Promise.reject(err);
  });
}

function setNumbro() {
  external__numbro__default.a.setDefaults({
    thousandSeparated: true,
    mantissa: 2
  });
}

/* harmony default export */ var client = (function (props) {
  var dispatch = props.store.dispatch;
  elementPolyfills();
  setAxios(dispatch);
  setNumbro(); // Set init props from server

  dispatch(service_setServices(props.pageProps.services));
  dispatch(category_setCategories(props.pageProps.categories)); // Set ui ready

  Object(dispatchers_auth["b" /* refresh */])(dispatch)(true).catch(function () {
    return null;
  }).finally(function () {
    return dispatch(Object(ui["b" /* setUIReady */])(true));
  });
});
// CONCATENATED MODULE: ./init/serverProps.js
/**
 * Sets initial data for pages.
 * This runs on first load and only on server
 * 
 * eg. first route or refresh page
 */
var _pick = __webpack_require__(14);

var includeConfig = ['production', 'browsers'];
/* harmony default export */ var serverProps = (function (req, res) {
  // Get all required data
  return Promise.all([Model.Category.find({}).exec(), Model.Service.find({}).exec()]).then(function (results) {
    return {
      config: _pick(global.Config, includeConfig),
      categories: results[0],
      services: results[1]
    };
  });
});
// CONCATENATED MODULE: ./pages/_app.js


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return _get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }













router__default.a.onRouteChangeStart = function () {
  return external__nprogress__default.a.start();
};

router__default.a.onRouteChangeComplete = function () {
  return external__nprogress__default.a.done();
};

router__default.a.onRouteChangeError = function () {
  return external__nprogress__default.a.done();
};

var _app_MyApp =
/*#__PURE__*/
function (_App) {
  _inherits(MyApp, _App);

  function MyApp(props) {
    var _this;

    _classCallCheck(this, MyApp);

    _this = _possibleConstructorReturn(this, (MyApp.__proto__ || Object.getPrototypeOf(MyApp)).call(this, props));
    Object.defineProperty(_assertThisInitialized(_this), "pageContext", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: null
    });
    _this.pageContext = getPageContext();
    if (global.document) client(props);
    return _this;
  }

  _createClass(MyApp, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      // Remove the server-side injected CSS.
      var jssStyles = document.querySelector('#jss-server-side');

      if (jssStyles && jssStyles.parentNode) {
        jssStyles.parentNode.removeChild(jssStyles);
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _props = this.props,
          Component = _props.Component,
          pageProps = _props.pageProps;
      return external__react__default.a.createElement(app_["Container"], null, external__react__default.a.createElement(JssProvider__default.a, {
        registry: this.pageContext.sheetsRegistry,
        generateClassName: this.pageContext.generateClassName
      }, external__react__default.a.createElement(styles_["MuiThemeProvider"], {
        theme: this.pageContext.theme,
        sheetsManager: this.pageContext.sheetsManager
      }, external__react__default.a.createElement(CssBaseline__default.a, null), external__react__default.a.createElement(Component, _extends({}, pageProps, {
        pageContext: this.pageContext
      })))));
    }
  }], [{
    key: "getInitialProps",
    value: function () {
      var _getInitialProps = _asyncToGenerator(
      /*#__PURE__*/
      regenerator__default.a.mark(function _callee(cntx) {
        var prom;
        return regenerator__default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                prom = _get(MyApp.__proto__ || Object.getPrototypeOf(MyApp), "getInitialProps", this).call(this, cntx);

                if (!global.document) {
                  prom = prom.then(function (data) {
                    return serverProps(cntx.req, cntx.res).then(function (props) {
                      return {
                        pageProps: _objectSpread({}, data.pageProps, props)
                      };
                    });
                  });
                }

                return _context.abrupt("return", prom);

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function getInitialProps(_x) {
        return _getInitialProps.apply(this, arguments);
      };
    }()
  }]);

  return MyApp;
}(app__default.a);

/* harmony default export */ var _app = __webpack_exports__["default"] = (Object(store["a" /* default */])(_app_MyApp));

/***/ }),
/* 69 */
/***/ (function(module, exports) {

module.exports = require("next/app");

/***/ }),
/* 70 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/CssBaseline");

/***/ }),
/* 71 */
/***/ (function(module, exports) {

module.exports = require("react-jss/lib/JssProvider");

/***/ }),
/* 72 */
/***/ (function(module, exports) {

module.exports = require("jss");

/***/ }),
/* 73 */
/***/ (function(module, exports) {

module.exports = require("nprogress");

/***/ })
/******/ ]);