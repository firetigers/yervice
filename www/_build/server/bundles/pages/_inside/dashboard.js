module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 78);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ 11:
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/regenerator");

/***/ }),

/***/ 17:
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/IconButton");

/***/ }),

/***/ 44:
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Edit");

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _default; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__material_ui_core_Tabs__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__material_ui_core_Tabs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__material_ui_core_Tabs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__material_ui_core_Tab__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__material_ui_core_Tab___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__material_ui_core_Tab__);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }




/**
 * This component is a wrapper for Material Tabs.
 * 
 * It automatically create tabs based on children.
 * Children must have `name` prop
 * 
 * eg.
 * <Tabs
 *    <div name="Tab A">Content A</div>
 *    <div name="Tab B">Content B</div>
 * </Tabs>
 */

var _default =
/*#__PURE__*/
function (_Component) {
  _inherits(_default, _Component);

  function _default(props) {
    var _this;

    _classCallCheck(this, _default);

    _this = _possibleConstructorReturn(this, (_default.__proto__ || Object.getPrototypeOf(_default)).call(this, props));
    Object.defineProperty(_assertThisInitialized(_this), "state", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: {
        value: 0
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "handleChange", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(e, _value) {
        _this.setState({
          value: _value
        });
      }
    });
    _this.state.value = parseInt(props.default || 0, 10);
    return _this;
  }

  _createClass(_default, [{
    key: "render",
    value: function render() {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", null, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        className: this.props.classNameTabs
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1__material_ui_core_Tabs___default.a, {
        value: this.state.value,
        onChange: this.handleChange
      }, this.props.children.map(function (child, i) {
        return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2__material_ui_core_Tab___default.a, {
          key: "tab" + i,
          label: child.props.name
        });
      }))), this.props.children[this.state.value]);
    }
  }]);

  return _default;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);



/***/ }),

/***/ 51:
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Tabs");

/***/ }),

/***/ 52:
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Tab");

/***/ }),

/***/ 53:
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Delete");

/***/ }),

/***/ 57:
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Input");

/***/ }),

/***/ 78:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(79);


/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: external "@babel/runtime/regenerator"
var regenerator_ = __webpack_require__(11);
var regenerator__default = /*#__PURE__*/__webpack_require__.n(regenerator_);

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// CONCATENATED MODULE: ./layouts/inside.js


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }



var inside__default =
/*#__PURE__*/
function (_Component) {
  _inherits(_default, _Component);

  function _default() {
    _classCallCheck(this, _default);

    return _possibleConstructorReturn(this, (_default.__proto__ || Object.getPrototypeOf(_default)).apply(this, arguments));
  }

  _createClass(_default, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", null, this.props.children);
    }
  }]);

  return _default;
}(external__react_["Component"]);


// EXTERNAL MODULE: ./components/tabs.js
var tabs = __webpack_require__(50);

// CONCATENATED MODULE: ./containers/inside/tabs/dashboard.js


function dashboard__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { dashboard__typeof = function _typeof(obj) { return typeof obj; }; } else { dashboard__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return dashboard__typeof(obj); }

function dashboard__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function dashboard__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function dashboard__createClass(Constructor, protoProps, staticProps) { if (protoProps) dashboard__defineProperties(Constructor.prototype, protoProps); if (staticProps) dashboard__defineProperties(Constructor, staticProps); return Constructor; }

function dashboard__possibleConstructorReturn(self, call) { if (call && (dashboard__typeof(call) === "object" || typeof call === "function")) { return call; } return dashboard__assertThisInitialized(self); }

function dashboard__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function dashboard__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }



var dashboard__default =
/*#__PURE__*/
function (_Component) {
  dashboard__inherits(_default, _Component);

  function _default() {
    dashboard__classCallCheck(this, _default);

    return dashboard__possibleConstructorReturn(this, (_default.__proto__ || Object.getPrototypeOf(_default)).apply(this, arguments));
  }

  dashboard__createClass(_default, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", null, "Dashboard Tab");
    }
  }]);

  return _default;
}(external__react_["Component"]);


// EXTERNAL MODULE: external "@material-ui/core/Table"
var Table_ = __webpack_require__(80);
var Table__default = /*#__PURE__*/__webpack_require__.n(Table_);

// EXTERNAL MODULE: external "@material-ui/core/TableBody"
var TableBody_ = __webpack_require__(81);
var TableBody__default = /*#__PURE__*/__webpack_require__.n(TableBody_);

// EXTERNAL MODULE: external "@material-ui/core/TableCell"
var TableCell_ = __webpack_require__(82);
var TableCell__default = /*#__PURE__*/__webpack_require__.n(TableCell_);

// EXTERNAL MODULE: external "@material-ui/core/TableHead"
var TableHead_ = __webpack_require__(83);
var TableHead__default = /*#__PURE__*/__webpack_require__.n(TableHead_);

// EXTERNAL MODULE: external "@material-ui/core/TableRow"
var TableRow_ = __webpack_require__(84);
var TableRow__default = /*#__PURE__*/__webpack_require__.n(TableRow_);

// EXTERNAL MODULE: external "@material-ui/core/IconButton"
var IconButton_ = __webpack_require__(17);
var IconButton__default = /*#__PURE__*/__webpack_require__.n(IconButton_);

// EXTERNAL MODULE: external "@material-ui/icons/Delete"
var Delete_ = __webpack_require__(53);
var Delete__default = /*#__PURE__*/__webpack_require__.n(Delete_);

// EXTERNAL MODULE: external "@material-ui/icons/Edit"
var Edit_ = __webpack_require__(44);
var Edit__default = /*#__PURE__*/__webpack_require__.n(Edit_);

// EXTERNAL MODULE: external "@material-ui/icons/Add"
var Add_ = __webpack_require__(85);
var Add__default = /*#__PURE__*/__webpack_require__.n(Add_);

// EXTERNAL MODULE: external "@material-ui/icons/Visibility"
var Visibility_ = __webpack_require__(86);
var Visibility__default = /*#__PURE__*/__webpack_require__.n(Visibility_);

// EXTERNAL MODULE: external "@material-ui/core/Input"
var Input_ = __webpack_require__(57);
var Input__default = /*#__PURE__*/__webpack_require__.n(Input_);

// CONCATENATED MODULE: ./containers/inside/tabs/categories.js


function categories__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { categories__typeof = function _typeof(obj) { return typeof obj; }; } else { categories__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return categories__typeof(obj); }

function categories__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function categories__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function categories__createClass(Constructor, protoProps, staticProps) { if (protoProps) categories__defineProperties(Constructor.prototype, protoProps); if (staticProps) categories__defineProperties(Constructor, staticProps); return Constructor; }

function categories__possibleConstructorReturn(self, call) { if (call && (categories__typeof(call) === "object" || typeof call === "function")) { return call; } return categories__assertThisInitialized(self); }

function categories__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function categories__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }












 // SAMPLE DATA ONLY >>>

var id = 0;

function createData(name, calories, fat, carbs, protein) {
  id += 1;
  return {
    id: id,
    name: name,
    calories: calories,
    fat: fat,
    carbs: carbs,
    protein: protein
  };
}

var data = [createData('0', 'Cleaning', '', 6.0, 24, 4.0), createData('1', 'Home Cleaning', 'Cleaning', 9.0, 37, 4.3), createData('2', 'Deep Clening', 'Cleaning', 16.0, 24, 6.0), createData('3', 'Moving', '', 3.7, 67, 4.3), createData('4', 'Office Moving', 'Moving', 16.0, 49, 3.9)]; // <<< SAMPLE DATA ONLY

var categories__default =
/*#__PURE__*/
function (_Component) {
  categories__inherits(_default, _Component);

  function _default() {
    categories__classCallCheck(this, _default);

    return categories__possibleConstructorReturn(this, (_default.__proto__ || Object.getPrototypeOf(_default)).apply(this, arguments));
  }

  categories__createClass(_default, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", null, external__react__default.a.createElement(Table__default.a, null, external__react__default.a.createElement(TableHead__default.a, null, external__react__default.a.createElement(TableRow__default.a, null, external__react__default.a.createElement(TableCell__default.a, null, "ID"), external__react__default.a.createElement(TableCell__default.a, null, "Name"), external__react__default.a.createElement(TableCell__default.a, null, "Parent"), external__react__default.a.createElement(TableCell__default.a, null, "Actions"))), external__react__default.a.createElement(TableBody__default.a, null, data.map(function (n) {
        return external__react__default.a.createElement(TableRow__default.a, {
          key: n.id
        }, external__react__default.a.createElement(TableCell__default.a, null, n.name), external__react__default.a.createElement(TableCell__default.a, null, n.calories), external__react__default.a.createElement(TableCell__default.a, null, n.fat), external__react__default.a.createElement(TableCell__default.a, null, external__react__default.a.createElement(IconButton__default.a, null, " ", external__react__default.a.createElement(Visibility__default.a, null), " "), external__react__default.a.createElement(IconButton__default.a, null, " ", external__react__default.a.createElement(Edit__default.a, null), " "), external__react__default.a.createElement(IconButton__default.a, null, " ", external__react__default.a.createElement(Delete__default.a, null), " ")));
      }), external__react__default.a.createElement(TableRow__default.a, {
        key: "new"
      }, external__react__default.a.createElement(TableCell__default.a, null, "New Category:"), external__react__default.a.createElement(TableCell__default.a, null, external__react__default.a.createElement(Input__default.a, {
        placeholder: "Name"
      })), external__react__default.a.createElement(TableCell__default.a, null, external__react__default.a.createElement(Input__default.a, {
        placeholder: "Parent Id"
      })), external__react__default.a.createElement(TableCell__default.a, null, external__react__default.a.createElement(IconButton__default.a, null, " ", external__react__default.a.createElement(Add__default.a, null), " "))))));
    }
  }]);

  return _default;
}(external__react_["Component"]);


// CONCATENATED MODULE: ./containers/inside/tabs/services.js


function services__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { services__typeof = function _typeof(obj) { return typeof obj; }; } else { services__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return services__typeof(obj); }

function services__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function services__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function services__createClass(Constructor, protoProps, staticProps) { if (protoProps) services__defineProperties(Constructor.prototype, protoProps); if (staticProps) services__defineProperties(Constructor, staticProps); return Constructor; }

function services__possibleConstructorReturn(self, call) { if (call && (services__typeof(call) === "object" || typeof call === "function")) { return call; } return services__assertThisInitialized(self); }

function services__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function services__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }



var services__default =
/*#__PURE__*/
function (_Component) {
  services__inherits(_default, _Component);

  function _default() {
    services__classCallCheck(this, _default);

    return services__possibleConstructorReturn(this, (_default.__proto__ || Object.getPrototypeOf(_default)).apply(this, arguments));
  }

  services__createClass(_default, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", null, "Services Tab");
    }
  }]);

  return _default;
}(external__react_["Component"]);


// CONCATENATED MODULE: ./containers/inside/tabs/users.js


function users__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { users__typeof = function _typeof(obj) { return typeof obj; }; } else { users__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return users__typeof(obj); }

function users__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function users__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function users__createClass(Constructor, protoProps, staticProps) { if (protoProps) users__defineProperties(Constructor.prototype, protoProps); if (staticProps) users__defineProperties(Constructor, staticProps); return Constructor; }

function users__possibleConstructorReturn(self, call) { if (call && (users__typeof(call) === "object" || typeof call === "function")) { return call; } return users__assertThisInitialized(self); }

function users__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function users__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }



var users__default =
/*#__PURE__*/
function (_Component) {
  users__inherits(_default, _Component);

  function _default() {
    users__classCallCheck(this, _default);

    return users__possibleConstructorReturn(this, (_default.__proto__ || Object.getPrototypeOf(_default)).apply(this, arguments));
  }

  users__createClass(_default, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", null, "Users Tab");
    }
  }]);

  return _default;
}(external__react_["Component"]);


// CONCATENATED MODULE: ./pages/_inside/dashboard.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _inside_dashboard__default; });



function _inside_dashboard__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _inside_dashboard__typeof = function _typeof(obj) { return typeof obj; }; } else { _inside_dashboard__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _inside_dashboard__typeof(obj); }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _inside_dashboard__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inside_dashboard__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _inside_dashboard__createClass(Constructor, protoProps, staticProps) { if (protoProps) _inside_dashboard__defineProperties(Constructor.prototype, protoProps); if (staticProps) _inside_dashboard__defineProperties(Constructor, staticProps); return Constructor; }

function _inside_dashboard__possibleConstructorReturn(self, call) { if (call && (_inside_dashboard__typeof(call) === "object" || typeof call === "function")) { return call; } return _inside_dashboard__assertThisInitialized(self); }

function _inside_dashboard__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inside_dashboard__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }









var _inside_dashboard__default =
/*#__PURE__*/
function (_Component) {
  _inside_dashboard__inherits(_default, _Component);

  function _default() {
    _inside_dashboard__classCallCheck(this, _default);

    return _inside_dashboard__possibleConstructorReturn(this, (_default.__proto__ || Object.getPrototypeOf(_default)).apply(this, arguments));
  }

  _inside_dashboard__createClass(_default, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement(inside__default, null, external__react__default.a.createElement(tabs["a" /* default */], {
        "default": "1"
      }, external__react__default.a.createElement(dashboard__default, {
        title: "Dashboard"
      }), external__react__default.a.createElement(categories__default, {
        title: "Categories"
      }), external__react__default.a.createElement(services__default, {
        title: "Services"
      }), external__react__default.a.createElement(users__default, {
        title: "Users"
      })));
    }
  }], [{
    key: "getInitialProps",
    // If `getInitialProps` exists,
    // It will be executed on the server after Express route handlers
    value: function () {
      var _getInitialProps = _asyncToGenerator(
      /*#__PURE__*/
      regenerator__default.a.mark(function _callee(_ref) {
        var req, res, userAgent;
        return regenerator__default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                req = _ref.req, res = _ref.res;
                userAgent = req ? req.headers['user-agent'] : navigator.userAgent;
                return _context.abrupt("return", {
                  userAgent: userAgent
                });

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function getInitialProps(_x) {
        return _getInitialProps.apply(this, arguments);
      };
    }()
  }]);

  return _default;
}(external__react_["Component"]);



/***/ }),

/***/ 80:
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Table");

/***/ }),

/***/ 81:
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/TableBody");

/***/ }),

/***/ 82:
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/TableCell");

/***/ }),

/***/ 83:
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/TableHead");

/***/ }),

/***/ 84:
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/TableRow");

/***/ }),

/***/ 85:
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Add");

/***/ }),

/***/ 86:
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Visibility");

/***/ })

/******/ });