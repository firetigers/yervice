module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 130);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash_keyBy__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash_keyBy___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash_keyBy__);

var actionTypes = [
/**
 * NAMES
 */
'PROVIDER', 'CLIENT',
/**
 * ACTIONS
 * 
 * Define all possible actions
 * 
 * Format: <name...>_<action>
 * eg. 
 *  - USER_SET
 *  - AUTH_LOGIN (Authentication Login)
 */
'UI_READY_SET', 'COUNTRY_LIST_SET', 'PROVIDER_SET', 'PROVIDER_SERVICES_SET', 'PROVIDER_SERVICE_ADD', 'PROVIDER_SERVICE_REM', 'PROVIDER_SERVICE_PRICE_SET', 'CLIENT_SET', 'CLIENT_ORDER_SET', 'CLIENT_ORDER_REM', 'CATEGORY_LIST_SET', 'SERVICE_LIST_SET', 'USER_SET', 'USER_ADDRESS_ADD', 'USER_ADDRESS_REM', 'AUTH_LOGIN', 'AUTH_REFRESH', 'RESET'];
/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0_lodash_keyBy___default()(actionTypes, function (action) {
  return action;
}));

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("next/link");

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = login;
/* harmony export (immutable) */ __webpack_exports__["c"] = refreshLogin;
/* harmony export (immutable) */ __webpack_exports__["b"] = logout;
/* unused harmony export getUser */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1____ = __webpack_require__(13);



function getScript(source, callback) {
  var script = document.createElement('script');
  var prior = document.getElementsByTagName('script')[0];
  script.async = 1;

  script.onload = script.onreadystatechange = function (_, isAbort) {
    if (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {
      script.onload = script.onreadystatechange = null;
      script = undefined;

      if (!isAbort) {
        if (callback) return callback();
      }
    }

    if (callback) return callback(new Error('Aborted'));
  };

  script.onerror = function (e) {
    if (callback) return callback(err);
  };

  script.src = source;
  prior.parentNode.insertBefore(script, prior);
}

function downloadFingerprint() {
  if (global.Fingerprint2) return;
  return new Promise(function (resolve, reject) {
    getScript('/static/libs/fingerprint2.min.js', function (err) {
      if (err) return reject(err);
      resolve();
    });
  });
}

function setAuth(res) {
  // Save access token to every request
  __WEBPACK_IMPORTED_MODULE_0_axios___default.a.defaults.headers.common['Authorization'] = 'Bearer ' + res.data.payload.access_token; // if (global.Fingerprint2) global.Fingerprint2 = null

  return res.data.payload;
}

function unsetAuth() {
  delete __WEBPACK_IMPORTED_MODULE_0_axios___default.a.defaults.headers.common['Authorization'];
}

function getFingerprint() {
  return Promise.resolve().then(downloadFingerprint).then(function () {
    return Promise.delay(1000);
  }).then(function () {
    return new Promise(function (resolve, reject) {
      new global.Fingerprint2().get(function (fp) {
        if (fp) resolve(fp);else reject(new Error('Fingerprint hash required'));
      });
    });
  });
}

function login(email, password) {
  return Promise.resolve().then(getFingerprint).then(function (fp) {
    return __WEBPACK_IMPORTED_MODULE_0_axios___default.a.post('/api/login', {
      email: email,
      password: password,
      fingerprint: fp
    });
  }).then(setAuth);
}
function refreshLogin(checkToken) {
  return Promise.resolve().then(function () {
    if (checkToken && __WEBPACK_IMPORTED_MODULE_0_axios___default.a.defaults.headers.common['Authorization']) throw new Error('Already has token');
  }).then(getFingerprint).then(function (fp) {
    var rt = Object(__WEBPACK_IMPORTED_MODULE_1____["b" /* getState */])().auth.refresh_token;
    if (!rt) throw new Error('No refresh token');
    return __WEBPACK_IMPORTED_MODULE_0_axios___default.a.post('/api/login/refresh', {
      fingerprint: fp
    }, {
      headers: {
        'Authorization': 'Bearer ' + Object(__WEBPACK_IMPORTED_MODULE_1____["b" /* getState */])().auth.refresh_token
      }
    });
  }).then(setAuth);
}
function logout() {
  unsetAuth();
}
function getUser() {
  return __WEBPACK_IMPORTED_MODULE_0_axios___default.a.get('/api/user').then(function (res) {
    return res.data.payload;
  });
}

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("lodash/remove");

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("lodash/get");

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Button");

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/regenerator");

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = require("lodash/find");

/***/ }),
/* 13 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "@babel/runtime/regenerator"
var regenerator_ = __webpack_require__(11);
var regenerator__default = /*#__PURE__*/__webpack_require__.n(regenerator_);

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "react-redux"
var external__react_redux_ = __webpack_require__(4);
var external__react_redux__default = /*#__PURE__*/__webpack_require__.n(external__react_redux_);

// EXTERNAL MODULE: external "redux"
var external__redux_ = __webpack_require__(2);
var external__redux__default = /*#__PURE__*/__webpack_require__.n(external__redux_);

// EXTERNAL MODULE: external "redux-promise-middleware"
var external__redux_promise_middleware_ = __webpack_require__(20);
var external__redux_promise_middleware__default = /*#__PURE__*/__webpack_require__.n(external__redux_promise_middleware_);

// EXTERNAL MODULE: external "lodash/throttle"
var throttle_ = __webpack_require__(21);
var throttle__default = /*#__PURE__*/__webpack_require__.n(throttle_);

// EXTERNAL MODULE: external "lodash/get"
var get_ = __webpack_require__(8);
var get__default = /*#__PURE__*/__webpack_require__.n(get_);

// EXTERNAL MODULE: external "lodash/pick"
var pick_ = __webpack_require__(14);
var pick__default = /*#__PURE__*/__webpack_require__.n(pick_);

// EXTERNAL MODULE: external "redux-devtools-extension"
var external__redux_devtools_extension_ = __webpack_require__(22);
var external__redux_devtools_extension__default = /*#__PURE__*/__webpack_require__.n(external__redux_devtools_extension_);

// CONCATENATED MODULE: ./store/storage.js
var readState = function readState() {
  try {
    var serializedState = localStorage.getItem('state');
    if (serializedState === undefined || serializedState === null) return;
    return JSON.parse(serializedState);
  } catch (err) {// Do nothing, function will return undefined
  }
};
var writeState = function writeState(state) {
  try {
    localStorage.setItem('state', JSON.stringify(state));
  } catch (err) {// Do nothing, function will return undefined
  }
};
// EXTERNAL MODULE: ./store/types.js
var types = __webpack_require__(1);

// CONCATENATED MODULE: ./store/reducers/auth.js
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


var init = {
  refresh_token: null
};
/* harmony default export */ var auth = (function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : init;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].AUTH_LOGIN + '_FULFILLED':
      return {
        refresh_token: action.payload.refresh_token || state.refresh_token
      };

    case types["a" /* default */].RESET:
      return _objectSpread({}, init);
  }

  return state;
});
// EXTERNAL MODULE: external "lodash/remove"
var remove_ = __webpack_require__(7);
var remove__default = /*#__PURE__*/__webpack_require__.n(remove_);

// CONCATENATED MODULE: ./store/reducers/user.js
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function user__objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { user__defineProperty(target, key, source[key]); }); } return target; }

function user__defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




var user_init = {
  id: null,
  avatar: null,
  firstname: null,
  lastname: null,
  addresses: []
};
/* harmony default export */ var reducers_user = (function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : user_init;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].USER_SET:
      var user = action.user;
      return user__objectSpread({}, state, {
        addresses: user.addresses || [],
        lastname: user.lastname,
        firstname: user.firstname,
        avatar: user.avatar,
        id: user._id
      });

    case types["a" /* default */].USER_ADDRESS_ADD + '_FULFILLED':
      return user__objectSpread({}, state, {
        addresses: _toConsumableArray(state.addresses).concat([action.payload.data.payload])
      });

    case types["a" /* default */].USER_ADDRESS_REM + '_FULFILLED':
      remove__default()(state.addresses, {
        _id: get__default()(action, 'payload.data.payload._id')
      });

      return user__objectSpread({}, state, {
        addresses: _toConsumableArray(state.addresses)
      });

    case types["a" /* default */].RESET:
      return user__objectSpread({}, user_init);
  }

  return state;
});
// CONCATENATED MODULE: ./store/reducers/account/provider.js
function provider__objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { provider__defineProperty(target, key, source[key]); }); } return target; }

function provider__defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


var provider_init = {
  id: null
};
/* harmony default export */ var provider = (function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : provider_init;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].PROVIDER_SET:
      return provider__objectSpread({}, state, {
        id: action.id
      });

    case types["a" /* default */].RESET:
      return provider__objectSpread({}, provider_init);
  }

  return state;
});
// CONCATENATED MODULE: ./store/reducers/account/client.js
function client__objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { client__defineProperty(target, key, source[key]); }); } return target; }

function client__defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


var client_init = {
  id: null,
  order: {}
};
/* harmony default export */ var client = (function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : client_init;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].CLIENT_ORDER_SET:
      return client__objectSpread({}, state, {
        order: action.order || {}
      });

    case types["a" /* default */].CLIENT_ORDER_REM:
      return client__objectSpread({}, state, {
        order: {}
      });

    case types["a" /* default */].CLIENT_SET:
      return client__objectSpread({}, state, {
        id: action.id
      });

    case types["a" /* default */].RESET:
      return client__objectSpread({}, client_init);
  }

  return state;
});
// CONCATENATED MODULE: ./store/reducers/account/index.js




/* harmony default export */ var account = (Object(external__redux_["combineReducers"])({
  client: client,
  provider: provider
}));
// EXTERNAL MODULE: external "lodash/find"
var find_ = __webpack_require__(12);
var find__default = /*#__PURE__*/__webpack_require__.n(find_);

// CONCATENATED MODULE: ./store/reducers/provider/services.js
function services__toConsumableArray(arr) { return services__arrayWithoutHoles(arr) || services__iterableToArray(arr) || services__nonIterableSpread(); }

function services__nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function services__iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function services__arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }




var services_init = [];
/* harmony default export */ var services = (function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : services_init;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].PROVIDER_SERVICES_SET:
      return action.list || [];

    case types["a" /* default */].PROVIDER_SERVICE_ADD:
      return services__toConsumableArray(state).concat([action.service]);

    case types["a" /* default */].PROVIDER_SERVICE_REM:
      remove__default()(state, function (item) {
        return item._id === action.provServiceId;
      });

      return services__toConsumableArray(state);

    case types["a" /* default */].PROVIDER_SERVICE_PRICE_SET + '_FULFILLED':
      var service = find__default()(state, {
        _id: action.payload._id
      });

      if (service) service.price = action.payload.price;
      return services__toConsumableArray(state);

    case types["a" /* default */].RESET:
      return services_init;
  }

  return state;
});
// CONCATENATED MODULE: ./store/reducers/provider/index.js


/* harmony default export */ var reducers_provider = (Object(external__redux_["combineReducers"])({
  services: services
}));
// CONCATENATED MODULE: ./store/reducers/client/index.js
function reducers_client__objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { reducers_client__defineProperty(target, key, source[key]); }); } return target; }

function reducers_client__defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


var reducers_client_init = {
  orders: []
};
/* harmony default export */ var reducers_client = (function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : reducers_client_init;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].RESET:
      return reducers_client__objectSpread({}, reducers_client_init);
  }

  return state;
});
// CONCATENATED MODULE: ./store/reducers/service/list.js
function list__toConsumableArray(arr) { return list__arrayWithoutHoles(arr) || list__iterableToArray(arr) || list__nonIterableSpread(); }

function list__nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function list__iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function list__arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }


var list_init = [];
/* harmony default export */ var list = (function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : list_init;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].SERVICE_LIST_SET:
      return list__toConsumableArray(action.list || []);
  }

  return state;
});
// CONCATENATED MODULE: ./store/reducers/service/index.js


/* harmony default export */ var reducers_service = (Object(external__redux_["combineReducers"])({
  list: list
}));
// CONCATENATED MODULE: ./store/reducers/category/list.js
function category_list__toConsumableArray(arr) { return category_list__arrayWithoutHoles(arr) || category_list__iterableToArray(arr) || category_list__nonIterableSpread(); }

function category_list__nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function category_list__iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function category_list__arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }


var category_list_init = [];
/* harmony default export */ var category_list = (function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : category_list_init;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].CATEGORY_LIST_SET:
      return category_list__toConsumableArray(action.list || []);
  }

  return state;
});
// CONCATENATED MODULE: ./store/reducers/category/index.js


/* harmony default export */ var category = (Object(external__redux_["combineReducers"])({
  list: category_list
}));
// CONCATENATED MODULE: ./store/reducers/ui/country.js
function country__objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { country__defineProperty(target, key, source[key]); }); } return target; }

function country__defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


var country_init = {
  list: []
};
/* harmony default export */ var country = (function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : country_init;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].COUNTRY_LIST_SET + '_FULFILLED':
      return country__objectSpread({}, state, {
        list: action.payload || []
      });
  }

  return state;
});
// CONCATENATED MODULE: ./store/reducers/ui/index.js


 // Ready

var ui_ready = function ready() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case types["a" /* default */].UI_READY_SET:
      return action.ready || false;
  }

  return state;
};

/* harmony default export */ var ui = (Object(external__redux_["combineReducers"])({
  ready: ui_ready,
  country: country
}));
// CONCATENATED MODULE: ./store/reducers/index.js









/**
 * Root reducer
 */

/* harmony default export */ var reducers = (Object(external__redux_["combineReducers"])({
  account: account,
  auth: auth,
  category: category,
  client: reducers_client,
  service: reducers_service,
  provider: reducers_provider,
  user: reducers_user,
  ui: ui
}));
/**
 * Filter states that will be saved in localstorage
 */

var persist = ['account', 'auth', 'user'];
// CONCATENATED MODULE: ./store/index.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return store_getState; });



function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function store__objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { store__defineProperty(target, key, source[key]); }); } return target; }

function store__defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }











var theStore;

function initializeStore() {
  var rootState = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var middlewares = Object(external__redux_["applyMiddleware"])(external__redux_promise_middleware__default()());
  if (!get__default()(global, '__NEXT_DATA__.props.pageProps.config.production')) middlewares = Object(external__redux_devtools_extension_["composeWithDevTools"])(middlewares);
  return Object(external__redux_["createStore"])(reducers, rootState, middlewares);
}

var store_getState = function getState(path) {
  return path ? get__default()(theStore.getState(), path) : theStore.getState();
};
/* harmony default export */ var store = __webpack_exports__["a"] = (function (App) {
  return (
    /*#__PURE__*/
    function (_Component) {
      _inherits(AppWithRedux, _Component);

      function AppWithRedux(props) {
        var _this;

        _classCallCheck(this, AppWithRedux);

        _this = _possibleConstructorReturn(this, (AppWithRedux.__proto__ || Object.getPrototypeOf(AppWithRedux)).call(this, props)); // Create store

        if (global.document) {
          // Client side createStore
          theStore = initializeStore(readState());
          theStore.subscribe(throttle__default()(function () {
            writeState(pick__default()(_this.reduxStore.getState(), persist));
          }, 1000));
        } else {
          // Server side createStore (no need initial state)
          theStore = Object(external__redux_["createStore"])(reducers);
        }

        _this.reduxStore = theStore;
        return _this;
      }

      _createClass(AppWithRedux, [{
        key: "render",
        value: function render() {
          return external__react__default.a.createElement(external__react_redux_["Provider"], {
            store: this.reduxStore
          }, external__react__default.a.createElement(App, _extends({}, this.props, {
            store: this.reduxStore
          })));
        }
      }], [{
        key: "getInitialProps",
        value: function () {
          var _getInitialProps = _asyncToGenerator(
          /*#__PURE__*/
          regenerator__default.a.mark(function _callee(appContext) {
            var appProps;
            return regenerator__default.a.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    appProps = {};

                    if (!(typeof App.getInitialProps === 'function')) {
                      _context.next = 5;
                      break;
                    }

                    _context.next = 4;
                    return App.getInitialProps.call(App, appContext);

                  case 4:
                    appProps = _context.sent;

                  case 5:
                    return _context.abrupt("return", store__objectSpread({}, appProps));

                  case 6:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));

          return function getInitialProps(_x) {
            return _getInitialProps.apply(this, arguments);
          };
        }()
      }]);

      return AppWithRedux;
    }(external__react_["Component"])
  );
});

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = require("lodash/pick");

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/styles");

/***/ }),
/* 16 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return reset; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return login; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return refresh; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__effects_auth__ = __webpack_require__(6);



var reset = function reset() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__types__["a" /* default */].RESET
  };
};
var login = function login(email, password) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__types__["a" /* default */].AUTH_LOGIN,
    payload: Object(__WEBPACK_IMPORTED_MODULE_1__effects_auth__["a" /* login */])(email, password)
  };
};
var refresh = function refresh(checkToken) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__types__["a" /* default */].AUTH_REFRESH,
    payload: Object(__WEBPACK_IMPORTED_MODULE_1__effects_auth__["c" /* refreshLogin */])(checkToken)
  };
};

/***/ }),
/* 17 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/IconButton");

/***/ }),
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _default; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__material_ui_core_CircularProgress__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__material_ui_core_CircularProgress___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__material_ui_core_CircularProgress__);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




var _default =
/*#__PURE__*/
function (_Component) {
  _inherits(_default, _Component);

  function _default() {
    _classCallCheck(this, _default);

    return _possibleConstructorReturn(this, (_default.__proto__ || Object.getPrototypeOf(_default)).apply(this, arguments));
  }

  _createClass(_default, [{
    key: "render",
    value: function render() {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1__material_ui_core_CircularProgress___default.a, {
        size: this.props.size || 20,
        style: {
          color: this.props.color || "#333333"
        }
      });
    }
  }]);

  return _default;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);



/***/ }),
/* 19 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/CircularProgress");

/***/ }),
/* 20 */
/***/ (function(module, exports) {

module.exports = require("redux-promise-middleware");

/***/ }),
/* 21 */
/***/ (function(module, exports) {

module.exports = require("lodash/throttle");

/***/ }),
/* 22 */
/***/ (function(module, exports) {

module.exports = require("redux-devtools-extension");

/***/ }),
/* 23 */
/***/ (function(module, exports) {

module.exports = require("lodash/keyBy");

/***/ }),
/* 24 */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),
/* 25 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "react-redux"
var external__react_redux_ = __webpack_require__(4);
var external__react_redux__default = /*#__PURE__*/__webpack_require__.n(external__react_redux_);

// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(5);
var router__default = /*#__PURE__*/__webpack_require__.n(router_);

// EXTERNAL MODULE: external "next/link"
var link_ = __webpack_require__(3);
var link__default = /*#__PURE__*/__webpack_require__.n(link_);

// CONCATENATED MODULE: ./containers/footer.js


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




var footer__default =
/*#__PURE__*/
function (_Component) {
  _inherits(_default, _Component);

  function _default() {
    _classCallCheck(this, _default);

    return _possibleConstructorReturn(this, (_default.__proto__ || Object.getPrototypeOf(_default)).apply(this, arguments));
  }

  _createClass(_default, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", {
        className: "footer links flex justify-between items-center h3"
      }, external__react__default.a.createElement("ul", {
        className: "list pa0 ma0 w-100 pl3"
      }, external__react__default.a.createElement("li", {
        className: "dib"
      }, external__react__default.a.createElement(link__default.a, {
        href: "/about"
      }, external__react__default.a.createElement("a", {
        className: "ph3-ns ph2 pv2"
      }, "About"))), external__react__default.a.createElement("li", {
        className: "dib"
      }, external__react__default.a.createElement(link__default.a, {
        href: "/terms"
      }, external__react__default.a.createElement("a", {
        className: "ph3-ns ph2 pv2"
      }, "Terms"))), external__react__default.a.createElement("li", {
        className: "dib"
      }, external__react__default.a.createElement(link__default.a, {
        href: "/privacy"
      }, external__react__default.a.createElement("a", {
        className: "ph3-ns ph2 pv2"
      }, "Privacy")))), external__react__default.a.createElement("div", {
        className: "w5 tc"
      }, external__react__default.a.createElement("span", {
        className: "ml3"
      }, "2018 Yervice")));
    }
  }]);

  return _default;
}(external__react_["Component"]);


// EXTERNAL MODULE: external "prop-types"
var external__prop_types_ = __webpack_require__(24);
var external__prop_types__default = /*#__PURE__*/__webpack_require__.n(external__prop_types_);

// EXTERNAL MODULE: external "@material-ui/core/styles"
var styles_ = __webpack_require__(15);
var styles__default = /*#__PURE__*/__webpack_require__.n(styles_);

// EXTERNAL MODULE: external "@material-ui/core/SwipeableDrawer"
var SwipeableDrawer_ = __webpack_require__(26);
var SwipeableDrawer__default = /*#__PURE__*/__webpack_require__.n(SwipeableDrawer_);

// EXTERNAL MODULE: external "@material-ui/core/Button"
var Button_ = __webpack_require__(10);
var Button__default = /*#__PURE__*/__webpack_require__.n(Button_);

// EXTERNAL MODULE: external "@material-ui/core/List"
var List_ = __webpack_require__(27);
var List__default = /*#__PURE__*/__webpack_require__.n(List_);

// EXTERNAL MODULE: external "@material-ui/core/Divider"
var Divider_ = __webpack_require__(28);
var Divider__default = /*#__PURE__*/__webpack_require__.n(Divider_);

// EXTERNAL MODULE: external "@material-ui/core/ListItem"
var ListItem_ = __webpack_require__(29);
var ListItem__default = /*#__PURE__*/__webpack_require__.n(ListItem_);

// EXTERNAL MODULE: external "@material-ui/core/ListItemIcon"
var ListItemIcon_ = __webpack_require__(30);
var ListItemIcon__default = /*#__PURE__*/__webpack_require__.n(ListItemIcon_);

// EXTERNAL MODULE: external "@material-ui/core/ListItemText"
var ListItemText_ = __webpack_require__(31);
var ListItemText__default = /*#__PURE__*/__webpack_require__.n(ListItemText_);

// EXTERNAL MODULE: external "@material-ui/icons/PersonAddRounded"
var PersonAddRounded_ = __webpack_require__(32);
var PersonAddRounded__default = /*#__PURE__*/__webpack_require__.n(PersonAddRounded_);

// EXTERNAL MODULE: external "@material-ui/icons/LocalLibraryRounded"
var LocalLibraryRounded_ = __webpack_require__(33);
var LocalLibraryRounded__default = /*#__PURE__*/__webpack_require__.n(LocalLibraryRounded_);

// EXTERNAL MODULE: external "@material-ui/icons/Help"
var Help_ = __webpack_require__(34);
var Help__default = /*#__PURE__*/__webpack_require__.n(Help_);

// EXTERNAL MODULE: external "@material-ui/icons/PersonRounded"
var PersonRounded_ = __webpack_require__(35);
var PersonRounded__default = /*#__PURE__*/__webpack_require__.n(PersonRounded_);

// EXTERNAL MODULE: external "@material-ui/icons/ExitToApp"
var ExitToApp_ = __webpack_require__(36);
var ExitToApp__default = /*#__PURE__*/__webpack_require__.n(ExitToApp_);

// EXTERNAL MODULE: external "@material-ui/icons/Web"
var Web_ = __webpack_require__(37);
var Web__default = /*#__PURE__*/__webpack_require__.n(Web_);

// EXTERNAL MODULE: ./store/effects/auth.js
var auth = __webpack_require__(6);

// CONCATENATED MODULE: ./containers/sidedrawerContent.js


function sidedrawerContent__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { sidedrawerContent__typeof = function _typeof(obj) { return typeof obj; }; } else { sidedrawerContent__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return sidedrawerContent__typeof(obj); }

function sidedrawerContent__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function sidedrawerContent__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function sidedrawerContent__createClass(Constructor, protoProps, staticProps) { if (protoProps) sidedrawerContent__defineProperties(Constructor.prototype, protoProps); if (staticProps) sidedrawerContent__defineProperties(Constructor, staticProps); return Constructor; }

function sidedrawerContent__possibleConstructorReturn(self, call) { if (call && (sidedrawerContent__typeof(call) === "object" || typeof call === "function")) { return call; } return sidedrawerContent__assertThisInitialized(self); }

function sidedrawerContent__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function sidedrawerContent__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }















var sidedrawerContent__default =
/*#__PURE__*/
function (_Component) {
  sidedrawerContent__inherits(_default, _Component);

  function _default(props) {
    var _this;

    sidedrawerContent__classCallCheck(this, _default);

    _this = sidedrawerContent__possibleConstructorReturn(this, (_default.__proto__ || Object.getPrototypeOf(_default)).call(this, props));
    Object.defineProperty(sidedrawerContent__assertThisInitialized(_this), "isAllAccount", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: false
    });
    Object.defineProperty(sidedrawerContent__assertThisInitialized(_this), "logout", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(e) {
        e.preventDefault();
        Object(auth["b" /* logout */])();

        _this.props.reset();

        router__default.a.push('/');
      }
    });
    if (props.account.provider.id && props.account.client.id) _this.isAllAccount = true;
    return _this;
  }

  sidedrawerContent__createClass(_default, [{
    key: "render",
    value: function render() {
      var account = this.props.account;
      return external__react__default.a.createElement("div", null, !this.props.user.id ? external__react__default.a.createElement("div", null, external__react__default.a.createElement(link__default.a, {
        href: "/login"
      }, external__react__default.a.createElement(ListItem__default.a, {
        button: true
      }, external__react__default.a.createElement(ListItemIcon__default.a, null, external__react__default.a.createElement(PersonRounded__default.a, null)), external__react__default.a.createElement(ListItemText__default.a, {
        primary: "LOGIN"
      }))), external__react__default.a.createElement(link__default.a, {
        href: "/signup"
      }, external__react__default.a.createElement(ListItem__default.a, {
        button: true
      }, external__react__default.a.createElement(ListItemIcon__default.a, null, external__react__default.a.createElement(PersonAddRounded__default.a, null)), external__react__default.a.createElement(ListItemText__default.a, {
        primary: "SIGN UP"
      })))) : external__react__default.a.createElement("div", null, account.provider.id && external__react__default.a.createElement(link__default.a, {
        href: "/provider"
      }, external__react__default.a.createElement(ListItem__default.a, {
        button: true
      }, external__react__default.a.createElement(ListItemIcon__default.a, null, external__react__default.a.createElement(Web__default.a, null)), external__react__default.a.createElement(ListItemText__default.a, {
        primary: "".concat(this.isAllAccount ? 'PROVIDER' : 'DASHBOARD')
      }))), account.client.id && external__react__default.a.createElement(link__default.a, {
        href: "/client"
      }, external__react__default.a.createElement(ListItem__default.a, {
        button: true
      }, external__react__default.a.createElement(ListItemIcon__default.a, null, external__react__default.a.createElement(Web__default.a, null)), external__react__default.a.createElement(ListItemText__default.a, {
        primary: "".concat(this.isAllAccount ? 'CLIENT' : 'DASHBOARD')
      }))), external__react__default.a.createElement("a", {
        onClick: this.logout
      }, external__react__default.a.createElement(ListItem__default.a, {
        button: true
      }, external__react__default.a.createElement(ListItemIcon__default.a, null, external__react__default.a.createElement(ExitToApp__default.a, null)), external__react__default.a.createElement(ListItemText__default.a, {
        primary: "LOGOUT"
      })))), external__react__default.a.createElement("div", {
        className: "bt b--light-gray mt3 pt3"
      }, external__react__default.a.createElement(link__default.a, {
        href: "/about"
      }, external__react__default.a.createElement(ListItem__default.a, {
        button: true
      }, external__react__default.a.createElement(ListItemIcon__default.a, null, external__react__default.a.createElement(LocalLibraryRounded__default.a, null)), external__react__default.a.createElement(ListItemText__default.a, {
        primary: "ABOUT US"
      }))), external__react__default.a.createElement(link__default.a, {
        href: "/help"
      }, external__react__default.a.createElement(ListItem__default.a, {
        button: true
      }, external__react__default.a.createElement(ListItemIcon__default.a, null, external__react__default.a.createElement(Help__default.a, null)), external__react__default.a.createElement(ListItemText__default.a, {
        primary: "HELP"
      })))));
    }
  }]);

  return _default;
}(external__react_["Component"]);


// EXTERNAL MODULE: external "@material-ui/core/IconButton"
var IconButton_ = __webpack_require__(17);
var IconButton__default = /*#__PURE__*/__webpack_require__.n(IconButton_);

// EXTERNAL MODULE: external "@material-ui/icons/Menu"
var Menu_ = __webpack_require__(38);
var Menu__default = /*#__PURE__*/__webpack_require__.n(Menu_);

// CONCATENATED MODULE: ./containers/sidedrawer.js


function sidedrawer__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { sidedrawer__typeof = function _typeof(obj) { return typeof obj; }; } else { sidedrawer__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return sidedrawer__typeof(obj); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function sidedrawer__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function sidedrawer__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function sidedrawer__createClass(Constructor, protoProps, staticProps) { if (protoProps) sidedrawer__defineProperties(Constructor.prototype, protoProps); if (staticProps) sidedrawer__defineProperties(Constructor, staticProps); return Constructor; }

function sidedrawer__possibleConstructorReturn(self, call) { if (call && (sidedrawer__typeof(call) === "object" || typeof call === "function")) { return call; } return sidedrawer__assertThisInitialized(self); }

function sidedrawer__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function sidedrawer__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }













var styles = {
  list: {
    width: 250
  },
  fullList: {
    width: 'auto'
  },
  root: {
    flexGrow: 1
  },
  flex: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  }
};

var sidedrawer_SwipeableTemporaryDrawer =
/*#__PURE__*/
function (_Component) {
  sidedrawer__inherits(SwipeableTemporaryDrawer, _Component);

  function SwipeableTemporaryDrawer() {
    var _ref;

    var _temp, _this;

    sidedrawer__classCallCheck(this, SwipeableTemporaryDrawer);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return sidedrawer__possibleConstructorReturn(_this, (_temp = _this = sidedrawer__possibleConstructorReturn(this, (_ref = SwipeableTemporaryDrawer.__proto__ || Object.getPrototypeOf(SwipeableTemporaryDrawer)).call.apply(_ref, [this].concat(args))), Object.defineProperty(sidedrawer__assertThisInitialized(_this), "state", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: {
        top: false,
        left: false,
        bottom: false,
        right: false
      }
    }), Object.defineProperty(sidedrawer__assertThisInitialized(_this), "odsSidebar", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: {
        socialArea: {
          height: '60px',
          width: '100%',
          position: 'relative',
          bottom: '0vh'
        },
        fbIcon: {
          fontSize: "1.5em",
          color: "#555",
          position: "absolute",
          top: "1rem",
          left: "15%"
        },
        IgIcon: {
          fontSize: "1.5em",
          color: "#555",
          position: "absolute",
          top: "1rem",
          left: "45%"
        },
        TwIcon: {
          fontSize: "1.5em",
          color: "#555",
          position: "absolute",
          top: "1rem",
          left: "75%"
        }
      }
    }), Object.defineProperty(sidedrawer__assertThisInitialized(_this), "toggleDrawer", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(side, open) {
        return function () {
          _this.setState(_defineProperty({}, side, open));
        };
      }
    }), _temp));
  }

  sidedrawer__createClass(SwipeableTemporaryDrawer, [{
    key: "render",
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          user = _props.user,
          account = _props.account,
          reset = _props.reset;
      return external__react__default.a.createElement("div", {
        className: "dib"
      }, external__react__default.a.createElement(IconButton__default.a, {
        onClick: this.toggleDrawer('right', true),
        className: "z-2",
        color: "inherit",
        "aria-label": "Menu"
      }, external__react__default.a.createElement(Menu__default.a, null)), external__react__default.a.createElement(SwipeableDrawer__default.a, {
        anchor: "right",
        open: this.state.right,
        onClose: this.toggleDrawer('right', false),
        onOpen: this.toggleDrawer('right', true)
      }, external__react__default.a.createElement("div", {
        tabIndex: 0,
        role: "button",
        onClick: this.toggleDrawer('right', false),
        onKeyDown: this.toggleDrawer('right', false)
      }, external__react__default.a.createElement("div", {
        className: "".concat(classes.list, " mb3 bb b--light-gray pb2")
      }, external__react__default.a.createElement("div", {
        className: "tc links pv4 mb2 bb b--light-gray"
      }, user.id ? external__react__default.a.createElement("div", null, external__react__default.a.createElement(link__default.a, {
        href: "/user"
      }, external__react__default.a.createElement("a", {
        className: "b ph3 dib"
      }, external__react__default.a.createElement("img", {
        src: user.avatar,
        className: "br-100 pa1 ba b--black-10 dib mb3"
      }), external__react__default.a.createElement("br", null), user.firstname, " ", user.lastname))) : external__react__default.a.createElement("div", null, external__react__default.a.createElement("img", {
        className: "w4 center db",
        src: "/static/images/logos/logo-dark-color.svg"
      }))), external__react__default.a.createElement(List__default.a, null, external__react__default.a.createElement(sidedrawerContent__default, {
        user: user,
        account: account,
        reset: reset
      }))), external__react__default.a.createElement("div", {
        style: this.odsSidebar.socialArea
      }, external__react__default.a.createElement("i", {
        className: "icon-facebook",
        style: this.odsSidebar.fbIcon
      }), external__react__default.a.createElement("i", {
        className: "icon-instagram",
        style: this.odsSidebar.IgIcon
      }), external__react__default.a.createElement("i", {
        className: "icon-twitter",
        style: this.odsSidebar.TwIcon
      })))));
    }
  }]);

  return SwipeableTemporaryDrawer;
}(external__react_["Component"]);

/* harmony default export */ var sidedrawer = (Object(styles_["withStyles"])(styles)(sidedrawer_SwipeableTemporaryDrawer));
// EXTERNAL MODULE: ./store/actions/auth.js
var actions_auth = __webpack_require__(16);

// CONCATENATED MODULE: ./containers/header.js


function header__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { header__typeof = function _typeof(obj) { return typeof obj; }; } else { header__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return header__typeof(obj); }

function header__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function header__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function header__createClass(Constructor, protoProps, staticProps) { if (protoProps) header__defineProperties(Constructor.prototype, protoProps); if (staticProps) header__defineProperties(Constructor, staticProps); return Constructor; }

function header__possibleConstructorReturn(self, call) { if (call && (header__typeof(call) === "object" || typeof call === "function")) { return call; } return header__assertThisInitialized(self); }

function header__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function header__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }








var header_Header =
/*#__PURE__*/
function (_Component) {
  header__inherits(Header, _Component);

  function Header(props) {
    var _this;

    header__classCallCheck(this, Header);

    _this = header__possibleConstructorReturn(this, (Header.__proto__ || Object.getPrototypeOf(Header)).call(this, props));
    Object.defineProperty(header__assertThisInitialized(_this), "state", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: {}
    });
    Object.defineProperty(header__assertThisInitialized(_this), "links", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: {
        logo: external__react__default.a.createElement(link__default.a, {
          href: "/"
        }, external__react__default.a.createElement("a", {
          className: "dib pl3"
        }, external__react__default.a.createElement("img", {
          className: "w5 db",
          src: "/static/images/logos/logo-dark-color.svg"
        }))),
        howitworks: external__react__default.a.createElement(link__default.a, {
          href: "/whatisyervice"
        }, external__react__default.a.createElement("a", {
          className: "pv2 ph3 dib"
        }, external__react__default.a.createElement("i", {
          className: "icon-information-solid f3 mr2 v-mid"
        }), external__react__default.a.createElement("span", {
          className: "b v-mid"
        }, "WHAT IS YERVICE?"))),
        home: external__react__default.a.createElement(link__default.a, {
          href: "/"
        }, external__react__default.a.createElement("a", {
          className: "pv2 ph3 dib"
        }, external__react__default.a.createElement("i", {
          className: "icon-home3 f3 mr2 v-mid"
        }), external__react__default.a.createElement("span", {
          className: "b v-mid"
        }, "Home"))),
        dashboard: external__react__default.a.createElement(link__default.a, {
          href: "/"
        }, external__react__default.a.createElement("a", {
          className: "pv2 ph3 dib"
        }, external__react__default.a.createElement("i", {
          className: "icon-home3 f3 mr2 v-mid"
        }), external__react__default.a.createElement("span", {
          className: "b v-mid"
        }, "Dashboard")))
      }
    });
    if (global.document) _this.isLoginPage = router__default.a.route == '/login';
    return _this;
  }

  header__createClass(Header, [{
    key: "render",
    value: function render() {
      var _props = this.props,
          user = _props.user,
          account = _props.account,
          reset = _props.reset;
      return external__react__default.a.createElement("div", {
        className: "header links fixed w-100 bg-almost-white flex items-center justify-between z-999"
      }, external__react__default.a.createElement("div", {
        className: "logo w6 nowrap ml0 ml2-l"
      }, this.links[this.props.logo || 'logo']), external__react__default.a.createElement("div", {
        className: "menu w-100"
      }, this.props.children), external__react__default.a.createElement("div", {
        className: "user w5 tr"
      }, user.id ? external__react__default.a.createElement(link__default.a, {
        href: "/dashboard"
      }, external__react__default.a.createElement("a", {
        key: "user",
        className: "dib nowrap"
      }, external__react__default.a.createElement("img", {
        src: user.avatar,
        className: "br-100 pa1 ba b--black-10 dib v-mid mr2 mr3-l"
      }), external__react__default.a.createElement("span", {
        className: "v-mid b dn dib-ns"
      }, user.firstname))) : !this.isLoginPage && external__react__default.a.createElement(link__default.a, {
        href: "/login"
      }, external__react__default.a.createElement("a", {
        key: "login",
        className: "ph3 pv2 dn dib-l"
      }, external__react__default.a.createElement("span", {
        className: "fw7 v-mid"
      }, "Login")))), external__react__default.a.createElement("div", {
        className: "w4 tc"
      }, external__react__default.a.createElement(sidedrawer, {
        user: user,
        account: account,
        reset: reset
      })));
    }
  }]);

  return Header;
}(external__react_["Component"]);

var stateProps = function stateProps(_ref) {
  var user = _ref.user,
      account = _ref.account;
  return {
    user: user,
    account: account
  };
};

var header_dispatchProps = function dispatchProps(dispatch) {
  return {
    reset: function reset() {
      return dispatch(Object(actions_auth["c" /* reset */])());
    }
  };
};

/* harmony default export */ var header = (Object(external__react_redux_["connect"])(stateProps, header_dispatchProps)(header_Header));
// EXTERNAL MODULE: ./components/progress.js
var progress = __webpack_require__(18);

// CONCATENATED MODULE: ./layouts/main.js


function main__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { main__typeof = function _typeof(obj) { return typeof obj; }; } else { main__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return main__typeof(obj); }

function main__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function main__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function main__createClass(Constructor, protoProps, staticProps) { if (protoProps) main__defineProperties(Constructor.prototype, protoProps); if (staticProps) main__defineProperties(Constructor, staticProps); return Constructor; }

function main__possibleConstructorReturn(self, call) { if (call && (main__typeof(call) === "object" || typeof call === "function")) { return call; } return main__assertThisInitialized(self); }

function main__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function main__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }








var main_Layout =
/*#__PURE__*/
function (_Component) {
  main__inherits(Layout, _Component);

  function Layout(props) {
    var _this;

    main__classCallCheck(this, Layout);

    _this = main__possibleConstructorReturn(this, (Layout.__proto__ || Object.getPrototypeOf(Layout)).call(this, props));

    if (global.document) {
      if (props.auth && !props.user.id) router__default.a.push('/login');
    }

    return _this;
  }

  main__createClass(Layout, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", {
        className: "layout"
      }, external__react__default.a.createElement(header, {
        logo: this.props.logo
      }), external__react__default.a.createElement("div", {
        className: "content color-text ph3 pt3 pb4 pb5-l"
      }, !this.props.ui.ready || this.props.hide || this.props.auth && !this.props.user.id ? external__react__default.a.createElement("div", {
        className: "tc pt5"
      }, external__react__default.a.createElement(progress["a" /* default */], {
        size: 30
      })) : this.props.children), external__react__default.a.createElement(footer__default, null));
    }
  }]);

  return Layout;
}(external__react_["Component"]);

var main_stateProps = function stateProps(_ref) {
  var user = _ref.user,
      ui = _ref.ui;
  return {
    user: user,
    ui: ui
  };
};

/* harmony default export */ var main = __webpack_exports__["a"] = (Object(external__react_redux_["connect"])(main_stateProps)(main_Layout));

/***/ }),
/* 26 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/SwipeableDrawer");

/***/ }),
/* 27 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/List");

/***/ }),
/* 28 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Divider");

/***/ }),
/* 29 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/ListItem");

/***/ }),
/* 30 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/ListItemIcon");

/***/ }),
/* 31 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/ListItemText");

/***/ }),
/* 32 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/PersonAddRounded");

/***/ }),
/* 33 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/LocalLibraryRounded");

/***/ }),
/* 34 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Help");

/***/ }),
/* 35 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/PersonRounded");

/***/ }),
/* 36 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/ExitToApp");

/***/ }),
/* 37 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Web");

/***/ }),
/* 38 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Menu");

/***/ }),
/* 39 */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),
/* 40 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["b"] = register;
/* harmony export (immutable) */ __webpack_exports__["a"] = postNewAddress;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_axios__);

function register(fields) {
  return __WEBPACK_IMPORTED_MODULE_0_axios___default.a.post('/api/register', fields);
}
function postNewAddress(fields) {
  return __WEBPACK_IMPORTED_MODULE_0_axios___default.a.post('/api/user/address', fields);
}

/***/ }),
/* 41 */,
/* 42 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return setUIReady; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loadCountryList; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_axios__);


var setUIReady = function setUIReady(ready) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__types__["a" /* default */].UI_READY_SET,
    ready: ready
  };
};
var loadCountryList = function loadCountryList() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_0__types__["a" /* default */].COUNTRY_LIST_SET,
    payload: __WEBPACK_IMPORTED_MODULE_1_axios___default.a.get('/static/country-codes.min.json').then(function (response) {
      return response.data;
    })
  };
};

/***/ }),
/* 43 */,
/* 44 */,
/* 45 */,
/* 46 */,
/* 47 */,
/* 48 */,
/* 49 */,
/* 50 */,
/* 51 */,
/* 52 */,
/* 53 */,
/* 54 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loadCountryList; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__actions_ui__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1____ = __webpack_require__(13);


/**
 * Example of conditional dispatch
 */

var loadCountryList = function loadCountryList(dispatch) {
  return function () {
    if (Object(__WEBPACK_IMPORTED_MODULE_1____["b" /* getState */])('ui.country.list').length) return Promise.resolve();
    return dispatch(Object(__WEBPACK_IMPORTED_MODULE_0__actions_ui__["a" /* loadCountryList */])());
  };
};

/***/ }),
/* 55 */,
/* 56 */,
/* 57 */,
/* 58 */,
/* 59 */,
/* 60 */,
/* 61 */,
/* 62 */,
/* 63 */,
/* 64 */,
/* 65 */,
/* 66 */,
/* 67 */,
/* 68 */,
/* 69 */,
/* 70 */,
/* 71 */,
/* 72 */,
/* 73 */,
/* 74 */,
/* 75 */,
/* 76 */,
/* 77 */,
/* 78 */,
/* 79 */,
/* 80 */,
/* 81 */,
/* 82 */,
/* 83 */,
/* 84 */,
/* 85 */,
/* 86 */,
/* 87 */,
/* 88 */,
/* 89 */,
/* 90 */,
/* 91 */,
/* 92 */,
/* 93 */,
/* 94 */,
/* 95 */,
/* 96 */,
/* 97 */,
/* 98 */,
/* 99 */,
/* 100 */,
/* 101 */,
/* 102 */,
/* 103 */,
/* 104 */,
/* 105 */,
/* 106 */,
/* 107 */,
/* 108 */,
/* 109 */,
/* 110 */,
/* 111 */,
/* 112 */,
/* 113 */,
/* 114 */,
/* 115 */,
/* 116 */,
/* 117 */,
/* 118 */,
/* 119 */,
/* 120 */,
/* 121 */,
/* 122 */,
/* 123 */,
/* 124 */,
/* 125 */,
/* 126 */,
/* 127 */,
/* 128 */,
/* 129 */,
/* 130 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(131);


/***/ }),
/* 131 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: external "@babel/runtime/regenerator"
var regenerator_ = __webpack_require__(11);
var regenerator__default = /*#__PURE__*/__webpack_require__.n(regenerator_);

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "react-redux"
var external__react_redux_ = __webpack_require__(4);
var external__react_redux__default = /*#__PURE__*/__webpack_require__.n(external__react_redux_);

// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(5);
var router__default = /*#__PURE__*/__webpack_require__.n(router_);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(39);
var head__default = /*#__PURE__*/__webpack_require__.n(head_);

// EXTERNAL MODULE: external "lodash/padStart"
var padStart_ = __webpack_require__(132);
var padStart__default = /*#__PURE__*/__webpack_require__.n(padStart_);

// EXTERNAL MODULE: external "lodash/pick"
var pick_ = __webpack_require__(14);
var pick__default = /*#__PURE__*/__webpack_require__.n(pick_);

// EXTERNAL MODULE: ./layouts/main.js + 4 modules
var main = __webpack_require__(25);

// EXTERNAL MODULE: external "next/link"
var link_ = __webpack_require__(3);
var link__default = /*#__PURE__*/__webpack_require__.n(link_);

// EXTERNAL MODULE: ./components/progress.js
var progress = __webpack_require__(18);

// EXTERNAL MODULE: ./store/effects/user.js
var user = __webpack_require__(40);

// CONCATENATED MODULE: ./utils/dateList.js
var monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
var monthList = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
/**
 * Zero base index
 */

function getDaysOfMonth(indexMonth) {
  return monthDays[indexMonth];
}
function getMonthList() {
  return monthList.concat();
}
function getMonthWord(indexMonth) {
  return monthList[indexMonth];
}
// EXTERNAL MODULE: ./store/dispatchers/ui.js
var ui = __webpack_require__(54);

// CONCATENATED MODULE: ./pages/signup/index.js



function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }














var signup_SignUp =
/*#__PURE__*/
function (_Component) {
  _inherits(SignUp, _Component);

  function SignUp(props) {
    var _this;

    _classCallCheck(this, SignUp);

    _this = _possibleConstructorReturn(this, (SignUp.__proto__ || Object.getPrototypeOf(SignUp)).call(this, props));
    Object.defineProperty(_assertThisInitialized(_this), "classStyle", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: 'gray w-100 pa2 ph4 mv2 br-pill shadow-4 b f4 bg-light-gray'
    });
    Object.defineProperty(_assertThisInitialized(_this), "state", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: {
        init: false,
        now_year: new Date().getFullYear(),
        gender: '',
        bd_month: '',
        bd_day: '',
        bd_year: '',
        addr_country: ''
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "loadData", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        if (!_this.state.init && _this.props.ui.ready) {
          _this.setState({
            init: true
          }); // May not exist, if constructor creates a redirect


          if (_this.dispatchLoadCountyList) _this.dispatchLoadCountyList();
        }
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "changeState", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(e) {
        e.preventDefault();

        _this.setState(_defineProperty({}, e.target.name, e.target.value));
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "changeStateAddress", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(e) {
        e.preventDefault();

        _this.setState({
          address: _objectSpread({}, _this.state.address, _defineProperty({}, e.target.name, e.target.value))
        });
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "submitForm", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(e) {
        e.preventDefault();
        if (_this.state.working) return;

        _this.setState({
          working: true,
          err: null
        });

        var _this$state = _this.state,
            bd_day = _this$state.bd_day,
            bd_month = _this$state.bd_month,
            bd_year = _this$state.bd_year;
        Object(user["b" /* register */])(_objectSpread({}, pick__default()(_this.state, ['email', 'password', 'firstname', 'lastname', 'gender', 'addr_line1', 'addr_line2', 'addr_country', 'addr_region', 'addr_locality']), {
          type: _this.props.query.as,
          birthdate: bd_year + '-' + padStart__default()(parseInt(bd_month, 10) + 1, 2, 0) + '-' + padStart__default()(parseInt(bd_day, 10), 2, 0)
        })).then(function (res) {
          return {
            id: res.data.payload.id,
            token: res.data.payload.token
          };
        }).then(function (_ref) {
          var id = _ref.id,
              token = _ref.token;
          return router__default.a.push("/others/activate?id=".concat(id, "&tk=").concat(token));
        }).catch(function (err) {
          _this.setState({
            working: false,
            err: err.response ? err.response.data : err
          });
        });
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "getMainErrorMessage", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        if (!_this.state.err) return '';

        switch (_this.state.err.code) {
          case 'EAIP':
            return 'Please fix invalid fields';

          case 'EAAX':
            return 'User Already Registered';

          default:
            return '';
        }
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "getError", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(name, bool) {
        if (_this.state.err && Array.isArray(_this.state.err.payload)) {
          if (_this.state.err.payload.find(function (errField) {
            return errField.param === name;
          })) return bool ? true : 'red';
        }

        return bool ? false : 'transparent';
      }
    });

    if (global.document) {
      if (props.user.id) {
        // User exist but wants to signup as either provide or client.
        // Activate account type instead of signing up a new user
        if (props.query.as === 'provider') {
          router__default.a.push('/provider' + (props.account.provider.id ? '' : '/activate'));
        } else if (props.query.as === 'client' && !props.account.client.id) {
          router__default.a.push('/client' + (props.account.provider.id ? '' : '/activate'));
        } else {
          router__default.a.push('/dashboard');
        }

        return _possibleConstructorReturn(_this);
      } // Provider the dispatch function to our dispatcher


      _this.dispatchLoadCountyList = Object(ui["a" /* loadCountryList */])(_this.props.dispatch);
    }

    return _this;
  }

  _createClass(SignUp, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.loadData();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      this.loadData();
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var getError = this.getError;
      return external__react__default.a.createElement(main["a" /* default */], null, external__react__default.a.createElement(head__default.a, null, external__react__default.a.createElement("title", null, "Sign Up")), external__react__default.a.createElement("div", {
        className: "color-text open-sans"
      }, external__react__default.a.createElement("main", {
        className: "tc"
      }, external__react__default.a.createElement("div", {
        className: "w-100 pa2 z-0"
      }, external__react__default.a.createElement("form", {
        onSubmit: this.submitForm
      }, external__react__default.a.createElement("h1", null, "SIGN UP"), external__react__default.a.createElement("h2", null, external__react__default.a.createElement("span", null, "as "), external__react__default.a.createElement("span", {
        className: "color-".concat(this.props.query.as === 'provider' ? 'a2' : 'b2')
      }, this.props.query.as === 'provider' ? 'Service Provider' : 'Client')), external__react__default.a.createElement("div", {
        className: "red"
      }, "\xA0", this.getMainErrorMessage(), "\xA0"), external__react__default.a.createElement("div", {
        className: "mt3 mw6 center"
      }, external__react__default.a.createElement("input", {
        className: "ba b--".concat(getError('firstname'), " ").concat(this.classStyle),
        type: "text",
        name: "firstname",
        onChange: this.changeState,
        placeholder: "Fist Name"
      }), external__react__default.a.createElement("br", null), external__react__default.a.createElement("input", {
        className: "ba b--".concat(getError('lastname'), " ").concat(this.classStyle),
        type: "text",
        name: "lastname",
        onChange: this.changeState,
        placeholder: "Last Name"
      }), external__react__default.a.createElement("br", null), external__react__default.a.createElement("select", {
        className: "ba b--".concat(getError('gender'), " ").concat(this.classStyle),
        value: this.state.gender,
        name: "gender",
        onChange: this.changeState
      }, ">", external__react__default.a.createElement("option", {
        value: "",
        disabled: true
      }, "Gender"), external__react__default.a.createElement("option", {
        value: "M"
      }, "Male"), external__react__default.a.createElement("option", {
        value: "F"
      }, "Female")), external__react__default.a.createElement("br", null), external__react__default.a.createElement("div", null, external__react__default.a.createElement("h3", {
        className: "".concat(getError('birthdate') === 'transparent' ? '' : 'red')
      }, "Birth Date"), external__react__default.a.createElement("select", {
        className: "".concat(this.classStyle, " bn"),
        value: this.state.bd_month,
        name: "bd_month",
        onChange: this.changeState
      }, external__react__default.a.createElement("option", {
        value: "",
        disabled: true
      }, "Month"), getMonthList().map(function (month, i) {
        return external__react__default.a.createElement("option", {
          key: i,
          value: i
        }, month);
      })), external__react__default.a.createElement("br", null), external__react__default.a.createElement("div", {
        className: "w-100 flex"
      }, external__react__default.a.createElement("select", {
        className: "".concat(this.classStyle, " fl w-50 bn mr2"),
        value: this.state.bd_day,
        name: "bd_day",
        onChange: this.changeState
      }, external__react__default.a.createElement("option", {
        value: "",
        disabled: true
      }, "Day"), Array.from(Array(getDaysOfMonth(parseInt(this.state.bd_month, 10) || 0)).keys()).map(function (item) {
        var _val = item + 1;

        return external__react__default.a.createElement("option", {
          key: _val,
          value: _val
        }, _val);
      })), external__react__default.a.createElement("select", {
        className: "".concat(this.classStyle, " fl w-50 bn ml2"),
        value: this.state.bd_year,
        name: "bd_year",
        onChange: this.changeState
      }, external__react__default.a.createElement("option", {
        value: "",
        disabled: true
      }, "Year"), Array.from(Array(100).keys()).map(function (item) {
        var _val = _this2.state.now_year - item;

        return external__react__default.a.createElement("option", {
          key: _val,
          value: _val
        }, _val);
      })))), external__react__default.a.createElement("br", null), external__react__default.a.createElement("div", null, external__react__default.a.createElement("h3", {
        className: "".concat(getError('address') === 'transparent' ? '' : 'red')
      }, "Address"), external__react__default.a.createElement("input", {
        className: "ba b--".concat(getError('addr_line1'), " ").concat(this.classStyle),
        type: "text",
        name: "addr_line1",
        onChange: this.changeState,
        placeholder: "Line 1"
      }), external__react__default.a.createElement("br", null), external__react__default.a.createElement("input", {
        className: "ba b--".concat(getError('addr_line2'), " ").concat(this.classStyle),
        type: "text",
        name: "addr_line2",
        onChange: this.changeState,
        placeholder: "Line 2"
      }), external__react__default.a.createElement("br", null), external__react__default.a.createElement("select", {
        className: "ba b--".concat(getError('addr_country'), " ").concat(this.classStyle),
        value: this.state.addr_country,
        name: "addr_country",
        onChange: this.changeState
      }, external__react__default.a.createElement("option", {
        value: "",
        disabled: true
      }, "Country"), this.props.ui.country.list.map(function (country) {
        return external__react__default.a.createElement("option", {
          key: country['ISO3166-1-Alpha-2'],
          value: country['ISO3166-1-Alpha-2']
        }, country.name);
      })), external__react__default.a.createElement("br", null), external__react__default.a.createElement("input", {
        className: "ba b--".concat(getError('addr_region'), " ").concat(this.classStyle),
        type: "text",
        name: "addr_region",
        onChange: this.changeState,
        placeholder: "Region"
      }), external__react__default.a.createElement("br", null), external__react__default.a.createElement("input", {
        className: "ba b--".concat(getError('addr_locality'), " ").concat(this.classStyle),
        type: "text",
        name: "addr_locality",
        onChange: this.changeState,
        placeholder: "Area"
      })), external__react__default.a.createElement("br", null), external__react__default.a.createElement("div", null, external__react__default.a.createElement("h3", null, "Login Credentials"), external__react__default.a.createElement("input", {
        className: "".concat(this.classStyle, " ba b--").concat(getError('email')),
        type: "email",
        name: "email",
        onChange: this.changeState,
        placeholder: "Email"
      }), external__react__default.a.createElement("br", null), external__react__default.a.createElement("input", {
        className: "".concat(this.classStyle, " ba b--").concat(getError('password')),
        type: "password",
        name: "password",
        onChange: this.changeState,
        placeholder: "Password"
      }), external__react__default.a.createElement("p", {
        className: "red mt2 mb0 f6"
      }, "\xA0", getError('password', true) && 'Must be 8+ characters long', "\xA0"))), external__react__default.a.createElement("button", {
        className: "pv2 ph5 relative ma2 br-pill bn shadow-4 b f3 bg-color-text white mt4 pointer",
        type: "submit"
      }, this.state.working && external__react__default.a.createElement("span", {
        className: "absolute left-1"
      }, external__react__default.a.createElement(progress["a" /* default */], {
        color: "#ffffff"
      })), external__react__default.a.createElement("span", null, "Sign Up")), external__react__default.a.createElement("div", {
        className: "mt3 pointer ph3 ph5-ns lh-copy f6 mw6 tc center mid-gray links"
      }, external__react__default.a.createElement("span", null, "By clicking Sign Up, you agree to our\xA0", external__react__default.a.createElement(link__default.a, {
        href: "/terms"
      }, external__react__default.a.createElement("a", {
        className: "fw6"
      }, "Terms & Conditions")), "\xA0and\xA0", external__react__default.a.createElement(link__default.a, {
        href: "/privacy"
      }, external__react__default.a.createElement("a", {
        className: "fw6"
      }, "Privacry Policy")))))))));
    }
  }], [{
    key: "getInitialProps",
    value: function () {
      var _getInitialProps = _asyncToGenerator(
      /*#__PURE__*/
      regenerator__default.a.mark(function _callee(_ref2) {
        var query, res;
        return regenerator__default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                query = _ref2.query, res = _ref2.res;

                if (!query.as) {
                  if (res) res.redirect('/signup/select');else router__default.a.push('/signup/select');
                }

                return _context.abrupt("return", {
                  query: query
                });

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function getInitialProps(_x) {
        return _getInitialProps.apply(this, arguments);
      };
    }()
  }]);

  return SignUp;
}(external__react_["Component"]);

var stateProps = function stateProps(_ref3) {
  var user = _ref3.user,
      account = _ref3.account,
      ui = _ref3.ui;
  return {
    user: user,
    account: account,
    ui: ui
  };
};

/* harmony default export */ var signup = __webpack_exports__["default"] = (Object(external__react_redux_["connect"])(stateProps)(signup_SignUp));

/***/ }),
/* 132 */
/***/ (function(module, exports) {

module.exports = require("lodash/padStart");

/***/ })
/******/ ]);