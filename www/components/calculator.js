import { Component } from 'react'
import numbro from 'numbro'
import _capitalize from 'lodash/capitalize'
import CircularProgress from '@material-ui/core/CircularProgress'

export default class extends Component {

  isPerHour = () => this.props.service.pricing == 'HOUR'
  isPerItem = () => this.props.service.pricing == 'ITEM'
  isPerJob = () => this.props.service.pricing == 'JOB'

  getTextPricing = () => {
    return _capitalize(this.props.service.pricing)
  }

  getPricingCount = () => {
    return (
      (this.isPerHour() && this.props.order.numHours) ||
      (this.isPerItem() && this.props.order.numItems) ||
      1
    )
  }

  render() {
    let baseTotal, grandTotal, { service, order } = this.props
    // Calculate base total (without additional attributes or options)
    baseTotal = service.price * this.getPricingCount()
    // Calculate grand total
    grandTotal = baseTotal * order.numProviders
    return (
      <div className="ba b--silver ph2 pv3 br3 bg-white">
        <div className="db mt0 mb3 pb2 bb b--silver bw1 f5">
          <h4 className="mt0 mb2">Calculator</h4>
          <span className="f7 gray">This is an approximate calculation based on base price of the service</span>
        </div>
        <ul className="list pa0 ma0 f6">
          <li className="flex justify-between items-center mb2 pb2 bb b--silver">
            <span>Base Price per {this.getTextPricing()}</span>
            <span className="nowrap">{numbro(service.price).format()} AED</span>
          </li>
          <li className="flex justify-between mb2 pb2 bb b--silver">
            <span>No. of {this.getTextPricing()}(s)</span>
            <span className="nowrap">{this.getPricingCount()}</span>
          </li>
          <li className="flex justify-between mb2 pb2 bb b--silver">
            <span>No. of Providers</span>
            <span className="nowrap">{order.numProviders}</span>
          </li>
          <li className="flex justify-between fw6 f5">
            <span>Total Approx:</span>
            <span className="nowrap">
              {numbro(grandTotal).format()} AED
            </span>
          </li>
        </ul>
      </div>
    )
  }

}