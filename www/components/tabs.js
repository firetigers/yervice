import { Component } from 'react'
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

/**
 * This component is a wrapper for Material Tabs.
 * 
 * It automatically create tabs based on children.
 * Children must have `name` prop
 * 
 * eg.
 * <Tabs
 *    <div name="Tab A">Content A</div>
 *    <div name="Tab B">Content B</div>
 * </Tabs>
 */

export default class extends Component {

  state = {
    value: 0
  }

  constructor(props) {
    super(props)
    this.state.value = parseInt(props.default || 0, 10)
  }

  handleChange = (e, value) => {
    this.setState({ value });
  }

  render() {
    return (
      <div>
        <div className={this.props.classNameTabs}>
          <Tabs value={this.state.value} onChange={this.handleChange}>
            {this.props.children.map((child, i) => {
              return (<Tab key={"tab" + i} label={child.props.name} />)
            })}
          </Tabs>
        </div>
        {this.props.children[this.state.value]}
      </div>
    )
  }

}