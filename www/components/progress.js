import { Component } from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'

export default class extends Component {

  render() {
    return (
      <CircularProgress
        size={this.props.size || 20}
        style={{ color: this.props.color || "#333333" }}
      />
    )
  }

}