import { Component } from 'react'
import Progess from '../components/progress'

export default (Content, initialProps) => {
  return class extends Component {

    state = {
      loading: false,
      loaded: false,
      props: null
    }

    constructor(props) {
      super(props)
      if (!global.document) return
      if (!this.state.loading && !this.state.loaded) {
        this.state.loading = true;
        initialProps(props).then(
          newProps => this.setState({
            loaded: true,
            loading: false,
            props: newProps || {}
          })
        )
      }
    }

    render() {
      return (
        this.state.loaded ?
          (<Content {...this.state.props} {...this.props} />) :
          (<div className="tc"><Progess size={30} /></div>)
      )
    }

  }
}