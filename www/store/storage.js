
export const readState = () => {
  try {
    const serializedState = localStorage.getItem('state')
    if (serializedState === undefined || serializedState === null) return
    return JSON.parse(serializedState)
  } catch (err) {
    // Do nothing, function will return undefined
  }
}

export const writeState = state => {
  try {
    localStorage.setItem('state', JSON.stringify(state))
  } catch (err) {
    // Do nothing, function will return undefined
  }
}