import _keyBy from 'lodash/keyBy'

const actionTypes = [
  /**
   * NAMES
   */

  'PROVIDER',
  'CLIENT',

  /**
   * ACTIONS
   * 
   * Define all possible actions
   * 
   * Format: <name...>_<action>
   * eg. 
   *  - USER_SET
   *  - AUTH_LOGIN (Authentication Login)
   */
  'UI_READY_SET',
  'COUNTRY_LIST_SET',

  'PROVIDER_SET',
  'PROVIDER_SERVICES_SET',
  'PROVIDER_SERVICE_ADD',
  'PROVIDER_SERVICE_REM',
  'PROVIDER_SERVICE_PRICE_SET',

  'CLIENT_SET',
  'CLIENT_ORDER_SET',
  'CLIENT_ORDER_REM',

  'CATEGORY_LIST_SET',
  'SERVICE_LIST_SET',
  
  'USER_SET',
  'USER_ADDRESS_ADD',
  'USER_ADDRESS_REM',
  
  'AUTH_LOGIN',
  'AUTH_REFRESH',
  'RESET'
]

export default _keyBy(actionTypes, action => action)