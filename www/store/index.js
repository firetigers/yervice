import { Component } from 'react'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import reduxPromise from 'redux-promise-middleware'
import _throttle from 'lodash/throttle'
import _get from 'lodash/get'
import _pick from 'lodash/pick'
import { composeWithDevTools } from 'redux-devtools-extension'

import { readState, writeState } from './storage'
import rootReducer, { persist } from './reducers'

let theStore;

function initializeStore(rootState = {}) {
  let middlewares = applyMiddleware(reduxPromise())
  if (!_get(global, '__NEXT_DATA__.props.pageProps.config.production'))
    middlewares = composeWithDevTools(middlewares)
  return createStore(rootReducer, rootState, middlewares)
}

export const getState = path => path ?
  _get(theStore.getState(), path) : theStore.getState()

export default (App) => {
  return class AppWithRedux extends Component {

    constructor(props) {
      super(props)
      // Create store
      if (global.document) {
        // Client side createStore
        theStore = initializeStore(readState())
        theStore.subscribe(_throttle(() => {
          writeState(_pick(this.reduxStore.getState(), persist))
        }, 1000))
      } else {
        // Server side createStore (no need initial state)
        theStore = createStore(rootReducer)
      }
      this.reduxStore = theStore
    }

    static async getInitialProps(appContext) {
      let appProps = {}
      if (typeof App.getInitialProps === 'function') {
        appProps = await App.getInitialProps.call(App, appContext)
      }
      return { ...appProps }
    }

    render() {
      return (
        <Provider store={this.reduxStore}>
          <App {...this.props} store={this.reduxStore} />
        </Provider>
      )
    }

  }
}
