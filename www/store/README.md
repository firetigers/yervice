#### Usage Flow

`Dispatchers` dispatches `Actions` uses `Effects` causes `Reducers` to change `State`
  
or
  
`Dispatchers` uses `Effects` and dispatches `Actions` causes `Reducers` to change `State`

#### Action Creators

Folder: `/actions`  
  
List of functions that creates action object for dispatchers.  
  
# Action Object

```javascript
// Sample action object. Created using object literal.
let action = {
  type: 'ADD_TODO',
  payload: 'Build my first Redux app'
}
```

# Action Creator

```javascript
// Function that creates acton object
function addTodo(text) {
  return {
    type: 'ADD_TODO',
    payload: text 
  }
}
// Or async action with redux promise middleware
function asyncDownloadTodo() {
  return {
    type: 'ADD_TODO',
    payload: axios.get('/gettodos') // -> Promise
  }
  // Then handle 'ADD_TODO_FULFILLED' in reducer
}
// Now, use the function to create action object
addTodo('First thing to do')
addTodo('Do something good today')
// You can use this function anywhere in your dispatchers
```

#### Effects

Folder: `/effects`  
  
List of side-effect functions (mostly http requests) that are 
used for dispatchers or actions.  


#### Dispatchers

Folder: `/dispatchers`  
Using: `redux-promise-middleware`  
  
List of dispatchers that dispatches actions in sequence asynchronously.
Also this is the best place to conditionaly dispatch an action.
E.g. If data is fetched, do not dispatch, else dispatch the action fetch.


# Applying Promise Middleware

Triggers action type: <action_name> + "_PENDING" | "_REJECTED" | "_FULFILLED"

```javascript
// Import react and redux
const { combineReducers, createStore, applyMiddleware } = require('redux')
const { default: promise } = require('redux-promise-middleware')

// Contains all reducers of very deep tree of reducers
let rootRecuder
// Initial state that reflects from server,
// or from localstorage which syncs with server
let stateFromServer

// Creating middleware object
const middleware = applyMiddleware(promise())
// Apply middleware to state
const store = createStore(rootRecuder, stateFromServer, middleware)

// Inject `store` to react root app component
```

#### Reducers

Folder: `/reducers`  
  
List of pure functions that reduces/recreates state objects.  
  
`combineReducers` helps to combine multiple files of reducers
