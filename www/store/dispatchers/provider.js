import {
  setServices,
  addService as addServiceAction,
  remService as remServiceAction
} from '../actions/provider'
import { getServices } from '../../store/effects/provider'
import { subscribeService, unsubscribeService } from '../effects/provider'

export const loadServices = dispatch => () => {
  return getServices()
    .then(data => dispatch(setServices(data)))
}

export const addService = dispatch => (id, price) => {
  return subscribeService(id, price)
    .then(provService => dispatch(addServiceAction(provService, price)))
}

export const remService = dispatch => (id) => {
  return unsubscribeService(id)
    .then(() => dispatch(remServiceAction(id)))
}