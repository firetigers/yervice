import { login as loginAction, refresh as refreshAction } from '../actions/auth'
import { setUser } from '../actions/user'
import { setProvider, setClient } from '../actions/account'

function hydrate(dispatch) {
  return ({ value: { user, provider, client } }) => {
    dispatch(setUser(user))
    if (provider) dispatch(setProvider(provider))
    if (client) dispatch(setClient(client))
  }
}

export const login = dispatch => (email, password) => {
  return dispatch(loginAction(email, password))
    .then(hydrate(dispatch))
}

export const refresh = dispatch => (checkToken = true) => {
  return dispatch(refreshAction(checkToken))
    .then(hydrate(dispatch))
}