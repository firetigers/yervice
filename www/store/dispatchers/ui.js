import { loadCountryList as loadCountryListAction } from '../actions/ui'
import { getState } from '../'

/**
 * Example of conditional dispatch
 */
export const loadCountryList = dispatch => () => {
  if (getState('ui.country.list').length) return Promise.resolve()
  return dispatch(loadCountryListAction())
}