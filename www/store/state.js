
import { getState } from './'

/**
 * Utility functions for state
 */

export function getUser() {
  return getState().user
}

export function getUserId() {
  return getUser().id
}

export function isAuth() {
  return !!getUserId()
}