import type from '../../types'

const init = []

export default (state = init, action) => {
  switch (action.type) {
    case type.SERVICE_LIST_SET:
      return [...action.list || []]
  }
  return state
}