import type from '../../types'

const init = {
  orders: []
}

export default (state = init, action) => {
  switch (action.type) {

    case type.RESET: return { ...init }
  }
  return state
} 