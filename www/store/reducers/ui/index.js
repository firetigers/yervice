import { combineReducers } from 'redux'
import type from '../../types'
import country from './country'

// Ready
const ready = (state = false, action) => {
  switch (action.type) {
    case type.UI_READY_SET:
      return action.ready || false
  }
  return state
}

export default combineReducers({
  ready,
  country
})