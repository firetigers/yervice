import type from '../../types'

const init = {
  list: []
}

export default (state = init, action) => {
  switch (action.type) {
    case type.COUNTRY_LIST_SET + '_FULFILLED':
      return {
        ...state,
        list: action.payload || []
      }
  }
  return state
}