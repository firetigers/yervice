import type from '../types'

const init = {
  refresh_token: null
}

export default (state = init, action) => {
  switch (action.type) {
    case type.AUTH_LOGIN + '_FULFILLED':
      return {
        refresh_token: action.payload.refresh_token || state.refresh_token
      }
    case type.RESET: return { ...init }
  }
  return state
}