import type from '../../types'
import _remove from 'lodash/remove'
import _find from 'lodash/find'

const init = []

export default (state = init, action) => {
  switch (action.type) {

    case type.PROVIDER_SERVICES_SET:
      return action.list || []

    case type.PROVIDER_SERVICE_ADD:
      return [...state, action.service]

    case type.PROVIDER_SERVICE_REM:
      _remove(state, item => item._id === action.provServiceId)
      return [...state]

    case type.PROVIDER_SERVICE_PRICE_SET + '_FULFILLED':
      let service = _find(state, { _id: action.payload._id })
      if (service) service.price = action.payload.price
      return [...state]

    case type.RESET: return init
  }
  return state
}