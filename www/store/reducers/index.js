import { combineReducers } from 'redux'
import auth from './auth'
import user from './user'
import account from './account'
import provider from './provider'
import client from './client'
import service from './service'
import category from './category'
import ui from './ui'

/**
 * Root reducer
 */
export default combineReducers({
  account,
  auth,
  category,
  client,
  service,
  provider,
  user,
  ui
})

/**
 * Filter states that will be saved in localstorage
 */
export const persist = [
  'account',
  'auth',
  'user'
]