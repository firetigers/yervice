import type from '../types'
import _get from 'lodash/get'
import _remove from 'lodash/remove'

const init = {
  id: null,
  avatar: null,
  firstname: null,
  lastname: null,
  addresses: []
}

export default (state = init, action) => {
  switch (action.type) {

    case type.USER_SET:
      let user = action.user
      return {
        ...state,
        addresses: user.addresses || [],
        lastname: user.lastname,
        firstname: user.firstname,
        avatar: user.avatar,
        id: user._id
      }

    case type.USER_ADDRESS_ADD + '_FULFILLED':
      return {
        ...state,
        addresses: [...state.addresses, action.payload.data.payload]
      }

    case type.USER_ADDRESS_REM + '_FULFILLED':
      _remove(state.addresses, { _id: _get(action, 'payload.data.payload._id') })
      return {
        ...state,
        addresses: [...state.addresses]
      }

    case type.RESET: return { ...init }
  }
  return state
}