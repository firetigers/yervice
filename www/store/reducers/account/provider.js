import type from '../../types'

const init = {
  id: null
}

export default (state = init, action) => {
  switch (action.type) {

    case type.PROVIDER_SET:
      return {
        ...state,
        id: action.id
      }

    case type.RESET: return { ...init }
  }
  return state
}