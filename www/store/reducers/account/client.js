import type from '../../types'

const init = {
  id: null,
  order: {}
}

export default (state = init, action) => {
  switch (action.type) {

    case type.CLIENT_ORDER_SET:
      return {
        ...state,
        order: action.order || {}
      }

    case type.CLIENT_ORDER_REM:
      return {
        ...state,
        order: {}
      }

    case type.CLIENT_SET:
      return {
        ...state,
        id: action.id
      }

    case type.RESET: return { ...init }
  }
  return state
}