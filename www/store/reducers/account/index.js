import type from '../../types'
import { combineReducers } from 'redux'
import provider from './provider'
import client from './client'

export default combineReducers({
  client,
  provider
})