import axios from 'axios'

export function activate(type) {
  return axios.post(`/api/${type.toLowerCase()}/create`)
    .then(res => res.data.payload)
}