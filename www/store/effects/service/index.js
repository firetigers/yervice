import axios from 'axios'

export function getServices() {
  return axios.get(`/api/service/list`)
    .then(res => res.data.payload)
}