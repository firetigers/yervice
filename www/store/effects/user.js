import axios from 'axios'

export function register(fields) {
  return axios.post(
    '/api/register',
    fields
  )
}

export function postNewAddress(fields) {
  return axios.post(
    '/api/user/address',
    fields
  )
}