import axios from 'axios'
import { getState } from '../'

function getScript(source, callback) {
  var script = document.createElement('script')
  var prior = document.getElementsByTagName('script')[0]
  script.async = 1
  script.onload = script.onreadystatechange = function (_, isAbort) {
    if (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {
      script.onload = script.onreadystatechange = null
      script = undefined
      if (!isAbort) { if (callback) return callback() }
    }
    if (callback) return callback(new Error('Aborted'))
  }
  script.onerror = function (e) {
    if (callback) return callback(err)
  }
  script.src = source
  prior.parentNode.insertBefore(script, prior)
}

function downloadFingerprint() {
  if (global.Fingerprint2) return
  return (new Promise(function (resolve, reject) {
    getScript('/static/libs/fingerprint2.min.js', (err) => {
      if (err) return reject(err)
      resolve()
    })
  }))
}

function setAuth(res) {
  // Save access token to every request
  axios.defaults.headers.
    common['Authorization'] = 'Bearer ' + res.data.payload.access_token
  // if (global.Fingerprint2) global.Fingerprint2 = null
  return res.data.payload
}

function unsetAuth() {
  delete axios.defaults.headers.common['Authorization']
}

function getFingerprint() {
  return Promise.resolve()
    .then(downloadFingerprint)
    .then(() => Promise.delay(1000))
    .then(() => {
      return new Promise(function (resolve, reject) {
        (new global.Fingerprint2).get(fp => {
          if (fp) resolve(fp)
          else reject(new Error('Fingerprint hash required'))
        })
      })
    })
}

export function login(email, password) {
  return Promise.resolve()
    .then(getFingerprint)
    .then(fp => {
      return axios.post(
        '/api/login',
        {
          email: email,
          password: password,
          fingerprint: fp
        })
    })
    .then(setAuth)
}

export function refreshLogin(checkToken) {
  return Promise.resolve()
    .then(() => {
      if (checkToken && axios.defaults.headers.common['Authorization'])
        throw new Error('Already has token')
    })
    .then(getFingerprint)
    .then(fp => {
      let rt = getState().auth.refresh_token
      if (!rt) throw new Error('No refresh token')
      return axios.post(
        '/api/login/refresh',
        {
          fingerprint: fp
        },
        {
          headers: {
            'Authorization': 'Bearer ' + getState().auth.refresh_token
          }
        })
    })
    .then(setAuth)
}

export function logout() {
  unsetAuth()
}

export function getUser() {
  return axios.get('/api/user')
    .then(res => res.data.payload)
}