import axios from 'axios'

export function getServices() {
  return axios.get(`/api/provider/service/list`)
    .then(res => res.data.payload)
}

export function subscribeService(sid, price) {
  return axios.post(`/api/provider/service/add`,
    {
      service: sid,
      price
    })
    .then(res => res.data.payload)
}

export function unsubscribeService(psid) {
  return axios.delete(
    `/api/provider/service/remove`,
    {
      data: {
        id: psid
      }
    })
    .then(res => res.data.payload)
}

export function setServicePrice(psid, price) {
  return axios.post(
    `/api/provider/service`,
    {
      id: psid,
      price
    })
    .then(res => res.data.payload)
}