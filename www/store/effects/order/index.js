import axios from 'axios'

export function loadClientOrders() {
  return axios.get(`/api/order/list/client`)
    .then(res => res.data.payload)
}

export function placeClientOrder(data) {
  return axios.post(
    '/api/order/create',
    data)
    .then(res => res.data.payload)
}