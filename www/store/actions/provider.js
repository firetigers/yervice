import type from '../types'
import { setServicePrice as effectSetServicePrice } from '../effects/provider'

export const setServices = (list) => ({
  type: type.PROVIDER_SERVICES_SET,
  list
})

export const addService = (service) => ({
  type: type.PROVIDER_SERVICE_ADD,
  service
})

export const remService = (provServiceId) => ({
  type: type.PROVIDER_SERVICE_REM,
  provServiceId
})

export const setServicePrice = (provServiceId, price) => ({
  type: type.PROVIDER_SERVICE_PRICE_SET,
  payload: effectSetServicePrice(provServiceId, price)
})