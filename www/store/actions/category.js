import type from '../types'

export const setCategories = (list) => ({
  type: type.CATEGORY_LIST_SET,
  list
})