import type from '../types'
import { postNewAddress } from '../effects/user'
import axios from 'axios'

export const setUser = (user) => ({
  type: type.USER_SET,
  user
})

export const addNewAddress = (fields) => ({
  type: type.USER_ADDRESS_ADD,
  payload: postNewAddress(fields)
})

export const remAddress = (id) => ({
  type: type.USER_ADDRESS_REM,
  payload: axios.delete(
    '/api/user/address',
    { data: { id } }
  )
})