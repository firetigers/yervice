import type from '../types'

export const setServices = (list) => ({
  type: type.SERVICE_LIST_SET,
  list
})