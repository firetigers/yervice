import type from '../../types'
import axios from 'axios'

export const setUIReady = (ready) => ({
  type: type.UI_READY_SET,
  ready
})

export const loadCountryList = () => ({
  type: type.COUNTRY_LIST_SET,
  payload: axios.get('/static/country-codes.min.json')
    .then(response => response.data)
})