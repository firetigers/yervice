import type from '../types'

export const setOrder = order => ({
  type: type.CLIENT_ORDER_SET,
  order
})

export const removeOrder = () => ({
  type: type.CLIENT_ORDER_REM
})