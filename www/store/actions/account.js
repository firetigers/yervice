import type from '../types'

export const setProvider = (provider) => ({
  type: type.PROVIDER_SET,
  id: provider._id
})

export const setClient = (client) => ({
  type: type.CLIENT_SET,
  id: client._id
})