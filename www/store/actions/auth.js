import type from '../types'
import { login as authLogin } from '../effects/auth'
import { refreshLogin } from '../effects/auth'

export const reset = () => ({
  type: type.RESET
})

export const login = (email, password) => ({
  type: type.AUTH_LOGIN,
  payload: authLogin(email, password)
})

export const refresh = (checkToken) => ({
  type: type.AUTH_REFRESH,
  payload: refreshLogin(checkToken)
})