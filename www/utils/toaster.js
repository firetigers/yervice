import nativeToast from 'native-toast'

const position = 'north-east'

export function toastError(message) {
  nativeToast({
    message,
    type: 'error',
    // position
  })
}