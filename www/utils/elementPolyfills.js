
export default () => {
  (function (ElementProto) {

    /**
     * matches
     */
    if (typeof ElementProto.matches !== 'function') {
      ElementProto.matches = ElementProto.msMatchesSelector || ElementProto.mozMatchesSelector || ElementProto.webkitMatchesSelector || function matches(selector) {
        var element = this;
        var elements = (element.document || element.ownerDocument).querySelectorAll(selector);
        var index = 0;

        while (elements[index] && elements[index] !== element) {
          ++index;
        }

        return Boolean(elements[index]);
      };
    }
    
    /**
     * closest
     */
    if (typeof ElementProto.closest !== 'function') {
      ElementProto.closest = function closest(selector) {
        var element = this;

        while (element && element.nodeType === 1) {
          if (element.matches(selector)) {
            return element;
          }

          element = element.parentNode;
        }

        return null;
      };
    }

    /**
     * find & findAll (querySelector & querySelectorAll)
     */

    if (typeof ElementProto.find !== 'function') {
      ElementProto.find = ElementProto.querySelector;
    }
    if (typeof ElementProto.findAll !== 'function') {
      ElementProto.findAll = ElementProto.querySelectorAll;
    }

  })(global.Element.prototype);
}