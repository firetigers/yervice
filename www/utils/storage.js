
/**
 * Local storage memory-only polyfill
 */

let data = {}

function lsTest() {
  let test = '__test__';
  try {
    localStorage.setItem(test, test);
    localStorage.removeItem(test);
    return true;
  } catch (e) {
    return false;
  }
}

const storage = lsTest() ? global['localStorage'] : {
  setItem(a, b) { data[a] = b },
  getItem(a) { return data[a] },
  removeItem(a) { delete data[a] },
  clear() { data = {} }
}

export default storage