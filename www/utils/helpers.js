
export const isMobile = (function () {
  return /iPhone|iPad|iPod|Android/i.test((global.navigator || {}).userAgent);
})()