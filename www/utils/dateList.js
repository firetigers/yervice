
const monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

const monthList = ['January', 'February', 'March', 'April', 'May',
  'June', 'July', 'August', 'September', 'October', 'November', 'December']

/**
 * Zero base index
 */

export function getDaysOfMonth(indexMonth) {
  return monthDays[indexMonth]
}

export function getMonthList() {
  return [...monthList]
}

export function getMonthWord(indexMonth) {
  return monthList[indexMonth]
}